<?php

use Illuminate\Database\Seeder;

class ResearchFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Bangla Text to Speech',
                'slug' => 'bangla-tts',
                'description' => 'Improving an existing Bangla TTS system ‘Subachan’ (developed in SUST). Diaphone-based output in Subachan was not smooth. New system is defined with less concatenation point. Identified most frequent Bangla words that are directly pronounced to obtain a more natural sounding output. Implementing a durational model using PSOLA algorithm.',
                'preview_image' => 'f55794931a0413062cb3e4c768450a26.png',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'name' => 'Bangla Optical Character Recognition',
                'slug' => 'bangla-ocr',
                'description' => 'Preprocessing is done,Character Segmentation is done,Working with recognition phase using ANN (Artificial Neural Network) Preparing training data',
                'preview_image' => '194d2638a10af1f6d0733ad3f445b7b5.jpeg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'name' => 'English to Bangla Translation',
                'slug' => 'e2b-translation',
                'description' => 'Resource collection: English-Bengali parallel corpus(Supara, Indic-Parallel, Indian Statistical Institutes Corpus), Bengali Monolingual Coupus (SUMono, Indian Statistical Institutes Corpus, EMILLE Corpus), Open Subtitle 2016 Corpus
Implementation of Baseline System: Baseline Statistical Machine Translation System(using Moses, Giza++, KenLM)',
                'preview_image' => '4e420c8045b4e9290753001b48bbc900.png',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 4,
                'name' => 'Bangla Speech to Text',
                'slug' => 'bangla-stt',
                'description' => 'A demo for ANN-based Speaker Independent Isolated Speech Recognizer for Bangla Word is already developed. Now, exploring the different Speech Feature extraction methods to identify the speech in robust way We are also exploring the techniques to develop a Standard Bangla Speech Corpora for Speaker Independent Continuous SR as well as Isolated SR Now we are working with Bangla Phonemes recognition techniques, which will be a part of a Standard Speech Corpora',
                'preview_image' => '2913b3a1549c6fddfeb8a9e849d98674.png',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('research_fields')->insert($data);
    }
}
