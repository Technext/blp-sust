<?php

use Illuminate\Database\Seeder;
// use DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'id' => 1,
                'homepage_header_title_1' => 'Research & Development of',
                'homepage_about_title_1' => 'University Grants Commission of Bangladesh',
                'homepage_about_title_2' => 'Window 4 Innovation Fund',
                'homepage_about_paragraph' => '<p>University-Industry Research Collaboration. Development of Multi-Platform Speech and Language Processing Software for Bangla ( CP-3888), HEQEP, UGC. Implementation Period: 3 Years (July 01, 2015- June 30, 2018)<br></p>',
                'custom_field1' => 'Language Processing Software for Bangla',
                'facebook' => 'https://www.facebook.com/',
                'twitter' => 'https://twitter.com/',
                'youtube' => 'http://youtube.com/'
            ]
        ];

        DB::table('settings')->insert($data);
    }
}
