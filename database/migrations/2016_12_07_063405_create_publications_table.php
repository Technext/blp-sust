<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->string('type_of_publication');
            $table->string('book_title')->nullable();
            $table->longText('abstract')->nullable();
            $table->dateTime('published_at')->nullable();
            $table->string('address')->nullable();
            $table->string('editor')->nullable();
            $table->string('publisher')->nullable();
            $table->string('pages')->nullable();
            $table->string('series')->nullable();
            $table->longText('note')->nullable();
            $table->string('journal')->nullable();
            $table->string('volume')->nullable();
            $table->string('institution')->nullable();
            $table->string('file_url')->nullable();
            $table->string('reference_url')->nullable();
            $table->boolean('is_public')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
