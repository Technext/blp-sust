<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_types', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->longText('description')->nullable();
            $table->timestamps();
        });

        \App\ResourceType::updateOrCreate(['name' => 'Publication Archive', 'slug' => 'publication-archive']);
        \App\ResourceType::updateOrCreate(['name' => 'Products', 'slug' => 'products']);
        \App\ResourceType::updateOrCreate(['name' => 'User Guide', 'slug' => 'user-guide']);
        \App\ResourceType::updateOrCreate(['name' => 'Presentations', 'slug' => 'presentations']);
        \App\ResourceType::updateOrCreate(['name' => 'Books', 'slug' => 'books']);
        \App\ResourceType::updateOrCreate(['name' => 'Corpus', 'slug' => 'corpus']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_types');
    }
}
