<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('homepage_header_title_1',191)->default("Research & Development of")->nullable();
            $table->string('homepage_about_title_1',191)->default("University Grants Commission of Bangladesh")->nullable();
            $table->string('homepage_about_title_2',191)->default("Window 4 Innovation Fund")->nullable();
            $table->longText('homepage_about_paragraph',2000)->nullable();
            $table->string('custom_field1',191)->nullable();
            $table->string('custom_field2',191)->nullable();
            $table->string('custom_field3',191)->nullable();
            $table->string('facebook',191)->nullable();
            $table->string('twitter',191)->nullable();
            $table->string('youtube',191)->nullable();
            $table->string('github',191)->nullable();
            $table->string('bitbucket',191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
