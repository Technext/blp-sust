                <div class="col-md-3 sidebar">
                    <ul>
                        <li>
                            <a href="{{ url('profile') }}" class="{{ Request::segment(1)=='profile' ? 'active' : '' }}">Profile</a>
                        </li>
                        <li>
                            <a href="{{ url('dashboard/publication') }}" class="{{ Request::segment(2)=='publication' ? 'active' : '' }}">Publications</a>
                        </li>
                        <li>
                            <a href="{{ url('dashboard/resources') }}" class="{{ Request::segment(2)=='resources' ? 'active' : '' }}">Resources</a>
                        </li>
                        <li>
                            <a href="{{ url('dashboard/resource_types') }}" class="{{ Request::segment(2)=='resource_types' ? 'active' : '' }}">Resources Types</a>
                        </li>
                        @if(Auth::user()->is_admin)
                        <li>
                            <a href="{{ url('dashboard/setting/homepage') }}" class="{{ Request::segment(3)=='homepage' ? 'active' : '' }}">Homepage Settings</a>
                        </li>
                        <li>
                            <a href="{{ url('dashboard/setting/footer') }}" class="{{ Request::segment(3)=='footer' ? 'active' : '' }}">Footer Settings</a>
                        </li>
                        <li>
                            <a href="{{ url('dashboard/lab') }}" class="{{ Request::segment(2)=='lab' ? 'active' : '' }}">Lab Facilities</a>
                        </li>
                        <li>
                            <a href="{{ url('dashboard/members') }}" class="{{ Request::segment(2)=='members' ? 'active' : '' }}">Members</a>
                        </li>
                        <li>
                            <a href="{{ url('dashboard/peoples') }}" class="{{ Request::segment(2)=='peoples' ? 'active' : '' }}">People</a>
                        </li>
                        <!-- <li>
                            <a href="{{ url('dashboard/designations') }}" class="{{ Request::segment(2)=='designations' ? 'active' : '' }}">Designations</a>
                        </li>
                        <li>
                            <a href="{{ url('dashboard/research_fields') }}" class="{{ Request::segment(2)=='research_fields' ? 'active' : '' }}">Research Fields</a>
                        </li> -->
                        @endif

                    </ul>
                </div>