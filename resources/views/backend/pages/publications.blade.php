@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Publications</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    <a href="{{ url('/dashboard/publication') }}" class="btn btn-new btn_new_active">Manage publications</a>
                                    <a href="{{ url('/dashboard/publication/book/create') }}" class="btn btn-new">Add Book</a>
                                    <a href="{{ url('/dashboard/publication/conference/create') }}" class="btn btn-new">Add Conference Paper</a>
                                    <a href="{{ url('/dashboard/publication/journal/create') }}" class="btn btn-new">Add Journal</a>
                                </p>


                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Type</th>
                                                <th>Attachment</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($publications))
                                            @foreach($publications as $publication)
                                            <tr>
                                                <td>{{$publication->title}}</td>
                                                <td>{{\App\Publication::$types[$publication->type_of_publication]}}</td>
                                                <td>
                                                    @if(isset($publication->file_url))
                                                        <a  target="_blank" href="{{ url('/assets/publications/'. $publication->file_url) }}">Download</a>
                                                    @endif

                                                </td>
                                                <td>
                                                    <a class="btn btn-xs btn-primary" href="{{ url('dashboard/publication/show/'. $publication->id) }}" target="_blank"><i class="fa fa-eye"></i></a>

                                                    <a href="{{ url('/dashboard/publication/'.$publication->id.'/edit') }}" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>

                                                    <a onclick="return confirm('Are you sure ?')" href="{{ url('/dashboard/publication/'.$publication->id.'/delete') }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



@endsection