@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Lab facilities</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="{{ url('/dashboard/lab') }}" class="btn btn-new btn_new_active">Manage Lab facilities</a>
                                    <a href="{{ url('/dashboard/lab/create') }}" class="btn btn-new">Add Lab facilities</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Attatchment</th>
                                                <th>Caption</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($labFacilities))
                                            @foreach($labFacilities as $facility)
                                            <tr>
                                                <td>{{$facility->id}}</td>
                                                <td>{{$facility->caption}}</td>
                                                <td><a target="_blank" href="{{asset('assets/lab_facilities/'.$facility->image)}}">Download</a></td>
                                                <td>

                                                    <a href="{{ url('/dashboard/lab/' . $facility->id . '/edit') }}"class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>

                                                    <a onclick="return confirm('Are you sure ?')"  href="{{ url('/dashboard/lab/' . $facility->id . '/delete') }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



@endsection