@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                        <h3  class="head_title">Edit People</h3>

                        <p>
                            <a href="{{ url('/dashboard/peoples') }}" class="btn btn-new btn_new_active">Manage People</a>
                            <a href="{{ url('/dashboard/peoples/create') }}" class="btn btn-new">Add New People</a>
                        </p>


                            {!! Form::open(['url' => action('Backend\PeopleController@update'), 'method' => 'POST', 'id' => 'resStore','class' => 'row', 'role'=>'form','enctype' => 'multipart/form-data']) !!}

                                {{ csrf_field() }}

                                <div class="form-group  col-sm-6 col-md-offset-3">

                                    @if($person->image)
                                        <img src="{{ url('assets/profile/'.$person->image) }}" alt="Profile Pic" class="profile-pic-img">
                                    @else
                                        <img class="profile-pic-img" src="{{ url('images/profile-placeholder.jpg') }}" alt="Profile Picture">
                                    @endif

                                    <label for="image"></label>
                                    <input type="file" name="image" class="form-control">
                                    <p class="text-center" style="font-size: 14px;margin-top: 7px;"><span class="text-danger">Picture must be : 200px x 200px</span></p>

                                </div>

                                <input hidden="hidden" type="hidden" name="id" value="{{$person->id}}">



                                <div class="form-group col-sm-12">
                                  {!! Form::label('name', 'Name *', array('class' => 'control-label')) !!}
                                  {!! Form::text('name', $person->name , ['class' => 'form-control','required' => 'required']); !!}
                                  @if ($errors->has('name'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </p>
                                  @endif
                                </div>


                                <div class="form-group col-sm-12">
                                  {!! Form::label('email', 'Email *', array('class' => 'control-label')) !!}
                                  {!! Form::email('email', $person->email , ['class' => 'form-control','required' => 'required']); !!}
                                  @if ($errors->has('email'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('phone_no', 'Phone Number', array('class' => 'control-label')) !!}
                                  {!! Form::text('phone_no', $person->phone_no , ['class' => 'form-control','required' => 'required']); !!}
                                  @if ($errors->has('phone_no'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('phone_no') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-6">
                                  {!! Form::label('type', 'Type', array('class' => 'control-label')) !!}
                                  {{ Form::select('type', [ 'academic' => 'Academic', 'admin' => 'Administrative' ], $person->type , ['id' => 'type','class' => 'form-control selectChosen']) }}
                                </div>


                                <div class="form-group col-sm-6">
                                  {!! Form::label('designation', 'Designation', array('class' => 'control-label')) !!}
                                  {{ Form::select('designation', $designations, $person->designation , ['id' => 'designation','class' => 'form-control selectChosen']) }}
                                </div>


                                <div class="form-group col-sm-12">
                                  {!! Form::label('bio', 'Bio', array('class' => 'control-label')) !!}
                                  {!! Form::textarea('bio', $person->bio , ['class' => 'form-control textArea','placeholder'=>'Enter Something About Member...','cols'=>15]); !!}
                                  @if ($errors->has('bio'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('bio') }}</strong>
                                      </p>
                                  @endif
                                </div>


                                <div class="form-group col-sm-12 text-right">
                                    <button type="submit" class="btn btn-new">Update</button>
                                </div>
                            {!! Form::close() !!}

                        </div>
                    </div>
                    
                </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>


@endsection