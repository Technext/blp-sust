@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3  class="head_title">Add New lab facilities</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="{{ url('/dashboard/lab') }}" class="btn btn-new">Manage lab lacilities</a>
                                    <a href="{{ url('/dashboard/lab/create') }}" class="btn btn-new btn_new_active">Create lab lacilities</a>
                                </p>

                                {!! Form::open(['url' => action('Backend\LabFacilitiesController@store'), 'method' => 'POST', 'id' => 'labStore','class' => 'row', 'role'=>'form','enctype' => 'multipart/form-data']) !!}

                                    {{ csrf_field() }}

                                    <div class="form-group col-sm-12">
                                        {!! Form::label('caption', 'Caption', array('class' => 'control-label')) !!}
                                        {!! Form::text('caption','',['placeholder' => 'Caption','class' => 'form-control']); !!}
                                        @if ($errors->has('caption'))
                                          <p class="help-block error_login">
                                              <strong>{{ $errors->first('caption') }}</strong>
                                          </p>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <label for="image">File</label>
                                        <input type="file" name="image" id="image" class="form-control" required="required">
                                    </div>

                                    <p class="text-right col-sm-12">
                                        <button type="submit" class="btn btn-new">Add</button>
                                    </p>

                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->

@endsection