@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3  class="head_title">Edit lab facilities</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="{{ url('/dashboard/lab') }}" class="btn btn-new btn_new_active">Manage lab lacilities</a>
                                    <a href="{{ url('/dashboard/lab/create') }}" class="btn btn-new">Create lab lacilities</a>
                                </p>

                                {!! Form::open(['url' => action('Backend\LabFacilitiesController@update'), 'method' => 'POST', 'id' => 'labStore','class' => 'row', 'role'=>'form','enctype' => 'multipart/form-data']) !!}

                                    {{ csrf_field() }}

                                    <input type="text" name="id" value="{{$labFacility->id}}" hidden="hidden">

                                    <div class="form-group col-sm-12">
                                        {!! Form::label('caption', 'Caption', array('class' => 'control-label')) !!}
                                        {!! Form::text('caption',$labFacility->caption,['placeholder' => 'Caption','class' => 'form-control']); !!}
                                        @if ($errors->has('caption'))
                                          <p class="help-block error_login">
                                              <strong>{{ $errors->first('caption') }}</strong>
                                          </p>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <label for="image">File</label>
                                        <input type="file" name="image" id="image" class="form-control">
                                        <p style="margin-top: 15px;"><a target="_blank" href="{{asset('assets/lab_facilities/'.$labFacility->image)}}">Download</a></p>
                                    </div>

                                    <p class="text-right col-sm-12">
                                        <button type="submit" class="btn btn-new">Update</button>
                                    </p>

                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->

@endsection