@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Add Resource Types</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="{{ url('/dashboard/resource_types') }}" class="btn btn-new btn_new_active">Manage Resource Types</a>
                                    <a href="{{ url('/dashboard/resource_types/create') }}" class="btn btn-new">Create Resource Type</a>
                                </p>

                                {!! Form::open(['url' => action('Backend\ResourceTypeController@update'), 'method' => 'POST', 'id' => 'typeUpdate','class' => 'row', 'role'=>'form']) !!}

                                    {{ csrf_field() }}

                                    <input type="text" name="id" value="{{$resource_type['id']}}" hidden="hidden">

                                    <div class="form-group col-sm-12">
                                        {!! Form::label('name', 'Name *', array('class' => 'control-label')) !!}
                                        {!! Form::text('name',$resource_type['name'],['placeholder' => 'Name','class' => 'form-control','required' => 'required']); !!}
                                        @if ($errors->has('name'))
                                          <p class="help-block error_login">
                                              <strong>{{ $errors->first('name') }}</strong>
                                          </p>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-12">
                                        {!! Form::label('slug', 'Slug *', array('class' => 'control-label')) !!}
                                        {!! Form::text('slug',$resource_type['slug'],['placeholder' => 'Slug','class' => 'form-control','required' => 'required']); !!}
                                        @if ($errors->has('slug'))
                                          <p class="help-block error_login">
                                              <strong>{{ $errors->first('slug') }}</strong>
                                          </p>
                                        @endif
                                    </div>



                                    <div class="form-group col-sm-12">
                                        {!! Form::label('description', 'Description', array('class' => 'control-label')) !!}
                                        {!! Form::textarea('description',$resource_type['description'],['placeholder' => 'Write something about the category...','class' => 'form-control textArea','rows' => '10']); !!}
                                        @if ($errors->has('description'))
                                          <p class="help-block error_login">
                                              <strong>{{ $errors->first('description') }}</strong>
                                          </p>
                                        @endif
                                    </div>


                                    <p class="text-right col-sm-12">
                                        <button type="submit" class="btn btn-new">Update</button>
                                    </p>

                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>


@endsection