@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Members</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="{{ url('/dashboard/members') }}" class="btn btn-new btn_new_active">Manage Member</a>
                                    <a href="{{ url('/dashboard/members/create') }}" class="btn btn-new">Add New Member</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Designation</th>
                                                <th>Phone Number</th>
                                                <th>User status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($users))
                                            @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->name}}</td>
                                                <td>
                                                @if($designations)
                                                @foreach($designations as $designation)
                                                    @if($designation->id == $user->designation)
                                                        {{ $designation->name }}
                                                    @endif
                                                @endforeach
                                                @endif
                                                </td>
                                                <td>{{ isset($user->phone_no) ? $user->phone_no : ''}}</td>
                                                <td>
                                                    @if($user->is_active)
                                                        <a onclick="return confirm('Are you sure. Want to de-active ?')" href="{{ url('/dashboard/members/'.$user->id.'/toggle-active') }}" class="btn">Deactivate</a>
                                                    @else
                                                        <a onclick="return confirm('Are you sure. Want to active ?')" href="{{ url('/dashboard/members/'.$user->id.'/toggle-active') }}" class="btn">Activate</a>
                                                    @endif
                                                </td>
                                                <td>

                                                    <a class="btn btn-xs btn-primary"  href="{{ url('/dashboard/members/show/'. $user->id) }}" class="btn" target="_blank"><i class="fa fa-eye"></i></a>
                                                    <a href="{{ url('/dashboard/members/' . $user->id . '/edit') }}"class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a onclick="return confirm('Are you sure ?')"  href="{{ url('/dashboard/members/' . $user->id . '/delete') }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



@endsection