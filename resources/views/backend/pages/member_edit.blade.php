@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                        <h3  class="head_title">Edit Member</h3>

                            <p>
                                <a href="{{ url('/dashboard/members') }}" class="btn btn-new btn_new_active">Manage Member</a>
                                <a href="{{ url('/dashboard/members/create') }}" class="btn btn-new">Add New Member</a>
                            </p>

                            {!! Form::open(['url' => action('Backend\MembersController@update'), 'method' => 'POST', 'id' => 'resStore','class' => 'row', 'role'=>'form','enctype' => 'multipart/form-data']) !!}

                                {{ csrf_field() }}


                                <div class="form-group  col-sm-6 col-md-offset-3">

                                    @if($user->image)
                                        <img src="{{ url('assets/profile/'.$user->image) }}" alt="Profile Pic" class="profile-pic-img">
                                    @else
                                        <img class="profile-pic-img" src="{{ url('images/profile-placeholder.jpg') }}" alt="Profile Picture">
                                    @endif

                                    <label for="image"></label>
                                    <input type="file" name="image" class="form-control">
                                    <p class="text-center" style="font-size: 14px;margin-top: 7px;"><span class="text-danger">Picture must be : 200px x 200px</span></p>

                                </div>

                                <input hidden="hidden" type="hidden" name="id" value="{{$user->id}}">


                                <div class="form-group col-sm-12">
                                  {!! Form::label('name', 'Name *', array('class' => 'control-label')) !!}
                                  {!! Form::text('name', $user->name, ['class' => 'form-control','required' => 'required']); !!}
                                  @if ($errors->has('name'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </p>
                                  @endif
                                </div>


                                <div class="form-group col-sm-12">
                                  {!! Form::label('email', 'Email *', array('class' => 'control-label')) !!}
                                  {!! Form::email('email', $user->email, ['class' => 'form-control','required' => 'required']); !!}
                                  @if ($errors->has('email'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('phone_no', 'Phone Number', array('class' => 'control-label')) !!}
                                  {!! Form::text('phone_no', $user->phone_no, ['class' => 'form-control','required' => 'required']); !!}
                                  @if ($errors->has('phone_no'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('phone_no') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('password', 'Old Password ( If you want to change password then enter otherwise keep it null. )', array('class' => 'control-label')) !!}
                                  <input type="password" name="password" class="form-control" placeholder="Old Password">
                                  @if ($errors->has('password'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('new_password', 'New Password ( If you want to change password then enter otherwise keep it null. )', array('class' => 'control-label')) !!}
                                  <input type="password" name="new_password" class="form-control" placeholder="New Password">
                                  @if ($errors->has('new_pasSSsword'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('new_password') }}</strong>
                                      </p>
                                  @endif
                                </div>


                                <div class="form-group col-sm-12">
                                  {!! Form::label('dept_name', 'Department', array('class' => 'control-label')) !!}
                                  {!! Form::text('dept_name',$user->dept_name, ['class' => 'form-control']); !!}
                                  @if ($errors->has('dept_name'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('dept_name') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('institute', 'Institute', array('class' => 'control-label')) !!}
                                  {!! Form::text('institute', $user->institute_name, ['class' => 'form-control']); !!}
                                  @if ($errors->has('institute'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('institute') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-6">
                                  {!! Form::label('designation', 'Designation', array('class' => 'control-label')) !!}
                                  {{ Form::select('designation', $designations, $user->designation, ['id' => 'designation','class' => 'form-control selectChosen']) }}
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="supervisor">Supervisor</label>
                                    <select name="supervisor" id="supervisor" class="form-control selectChosen">
                                        <option value="">Select supervisor</option>
                                        @foreach($faculties as $faculty)
                                            <option value="{{ $faculty->id }}" {{ ($user->supervisor == $faculty->id) ? 'selected' : '' }}>{{ $faculty->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('research_fields', 'Research Fields', array('class' => 'control-label')) !!}
                                  {{ Form::select('research_fields[]', $research_fields, $user->researchFields, ['id' => 'research_fields','class' => 'form-control selectChosen','multiple' => 'multiple']) }}
                                </div>


                                <div class="form-group col-sm-12">
                                  {!! Form::label('bio', 'Bio', array('class' => 'control-label')) !!}
                                  {!! Form::textarea('bio', $user->bio, ['class' => 'form-control textArea','placeholder'=>'Enter Something About Member...','cols'=>15]); !!}
                                  @if ($errors->has('bio'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('bio') }}</strong>
                                      </p>
                                  @endif
                                </div>


                                <div class="form-group col-sm-12 text-right">
                                    <button type="submit" class="btn btn-new">Update</button>
                                </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                    
                </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>


@endsection