@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3  class="head_title">View Resource</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="{{ url('/dashboard/resources') }}" class="btn btn-new btn_new_active">Manage Resources</a>
                                    <a href="{{ url('/dashboard/resources/create') }}" class="btn btn-new">Create Resource</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <tbody>

                                            <tr>
                                                <td class="text-center" width="25%"><strong>Title</strong></td>
                                                <td width="75%">{{$resources['title']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Keywords</strong></td>
                                                <td width="75%">{{$resources['keywords']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Description</strong></td>
                                                <td width="75%">{!!$resources['description']!!}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Resource type</strong></td>
                                                <td width="75%">
                                                    @foreach($resource_types as $resource)
                                                    @if($resource->id == $resources['resource_type'])
                                                        {{$resource->name}}
                                                    @endif
                                                    @endforeach
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Reference Url</strong></td>
                                                <td width="75%"><a href="{{$resources['reference_url']}}">{{$resources['reference_url']}}</a></td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Is public ?</strong></td>
                                                <td width="75%">{{$resources['is_public']==1 ? 'Yes' : 'NO'}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Version</strong></td>
                                                <td width="75%">{{$resources['version']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Files</strong></td>
                                                <td width="75%">
                                                    @if($resourceFiles)
                                                    @foreach($resourceFiles as $file)
                                                    <a class="btn btn-xs btn-primary" href="{{asset('assets/resources/'.$file->file_name)}}" target="_blank">Download</a>
                                                    @endforeach
                                                    @endif
                                                </td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();
        // $('.image').filer({
        //     showThumbs: true
        // });

    });
</script>


@endsection