@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Resources</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="{{ url('/dashboard/resources') }}" class="btn btn-new btn_new_active">Manage Resource</a>
                                    <a href="{{ url('/dashboard/resources/create') }}" class="btn btn-new">Add New Resource</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Keywords</th>
                                                <th>Type</th>
                                                <th>Version</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($resources))
                                            @foreach($resources as $resource)
                                            <tr>
                                                <td>{{$resource->title}}</td>
                                                <td>{{(isset($resource->keywords) ? $resource->keywords : '')}}</td>
                                                <td>{{(isset($resource->resourceType) ? $resource->resourceType->name : '')}}</td>
                                                <td>{{(isset($resource->version) ? $resource->version : '')}}</td>
                                                <td>

                                                    <a class="btn btn-xs btn-primary"  href="{{ url('/dashboard/resources/show/'. $resource->id) }}" class="btn" target="_blank"><i class="fa fa-eye"></i></a>
                                                    <a href="{{ url('/dashboard/resources/' . $resource->id . '/edit') }}"class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a onclick="return confirm('Are you sure ?')"  href="{{ url('/dashboard/resources/' . $resource->id . '/delete') }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



@endsection