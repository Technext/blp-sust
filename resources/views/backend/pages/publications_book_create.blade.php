@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <h3 class="head_title">Publications journal</h3>

                    <p>
                        <a href="{{ url('/dashboard/publication') }}" class="btn btn-new">Manage publications</a>
                        <a href="{{ url('/dashboard/publication/book/create') }}" class="btn btn-new {{ (Request::segment(3)=='book' ? 'btn_new_active' : '') }}">Add Book</a>
                        <a href="{{ url('/dashboard/publication/conference/create') }}" class="btn btn-new {{ (Request::segment(3)=='conference' ? 'btn_new_active' : '') }}">Add Conference Paper</a>
                        <a href="{{ url('/dashboard/publication/journal/create') }}" class="btn btn-new {{ (Request::segment(3)=='journal' ? 'btn_new_active' : '') }}">Add Journal</a>
                    </p>

                    {!! Form::open(['url' => action('Backend\PublicationBookController@store'), 'method' => 'POST', 'id' => 'bookStore','class' => '', 'role'=>'form', 'enctype'=>'multipart/form-data']) !!}

                            {{ csrf_field() }}


                            <div class="form-group col-sm-12">
                              {!! Form::label('title', 'Title *', array('class' => 'control-label')) !!}
                              {!! Form::text('title','',['placeholder' => 'title','class' => 'form-control','required' => 'required']); !!}
                              @if ($errors->has('title'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('title') }}</strong>
                                  </p>
                              @endif
                            </div>



                            <div class="form-group col-sm-12">
                              {!! Form::label('booktitle', 'Book Title', array('class' => 'control-label')) !!}
                              {!! Form::text('booktitle','',['placeholder' => 'Book Title','class' => 'form-control','required' => 'required']); !!}
                              @if ($errors->has('booktitle'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('booktitle') }}</strong>
                                  </p>
                              @endif
                            </div>


                            <div class="form-group col-sm-12">
                              {!! Form::label('notes', 'Notes', array('class' => 'control-label')) !!}
                              {!! Form::textarea('notes','',[ 'cols'=>'30', 'rows'=>'10','class' => 'form-control textArea']); !!}
                              @if ($errors->has('notes'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('notes') }}</strong>
                                  </p>
                              @endif
                            </div>

                            <div class="form-group  col-sm-12">
                                <input type="checkbox" name="is_public" id="show-publicly" checked>
                                <label for="show-publicly">Show Publicly?</label>
                            </div>
                            <div class="form-group  col-sm-12">
                                <input type="checkbox" name="is_profile_only" id="show-profile-only">
                                <label for="show-profile-only">Show Profile Only?</label>
                            </div>



                            <div class="form-group col-sm-12">
                              {!! Form::label('author', 'Author', array('class' => 'control-label')) !!}
                              {!! Form::text('author','',['placeholder' => 'Author','class' => 'form-control','required' => 'required']); !!}
                              @if ($errors->has('author'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('author') }}</strong>
                                  </p>
                              @endif
                            </div>


                            <div class="form-group col-sm-12">
                              {!! Form::label('editor', 'Editor', array('class' => 'control-label')) !!}
                              {!! Form::text('editor','',['placeholder' => 'Editor','class' => 'form-control']); !!}
                              @if ($errors->has('editor'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('editor') }}</strong>
                                  </p>
                              @endif
                            </div>

                            <div class="form-group col-sm-12">
                              {!! Form::label('publisher', 'Publisher', array('class' => 'control-label')) !!}
                              {!! Form::text('publisher','',['placeholder' => 'Publisher','class' => 'form-control']); !!}
                              @if ($errors->has('publisher'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('publisher') }}</strong>
                                  </p>
                              @endif
                            </div>

                            <div class="form-group col-sm-12">
                              {!! Form::label('year', 'Year', array('class' => 'control-label')) !!}
                              {!! Form::text('year','',['placeholder' => 'Year Published At','class' => 'form-control']); !!}
                              @if ($errors->has('year'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('year') }}</strong>
                                  </p>
                              @endif
                            </div>

                            <div class="form-group col-sm-12">
                              {!! Form::label('address', 'Address', array('class' => 'control-label')) !!}
                              {!! Form::text('address','',['placeholder' => 'Address','class' => 'form-control']); !!}
                              @if ($errors->has('address'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('address') }}</strong>
                                  </p>
                              @endif
                            </div>



                            <div class="form-group col-sm-12">
                              {!! Form::label('series', 'Series', array('class' => 'control-label')) !!}
                              {!! Form::text('series','',['placeholder' => 'Series','class' => 'form-control']); !!}
                              @if ($errors->has('series'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('series') }}</strong>
                                  </p>
                              @endif
                            </div>


                            <div class="form-group col-sm-12">
                              {!! Form::label('pages', 'Pages', array('class' => 'control-label')) !!}
                              {!! Form::text('pages','',['placeholder' => 'Pages','class' => 'form-control']); !!}
                              @if ($errors->has('pages'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('pages') }}</strong>
                                  </p>
                              @endif
                            </div>


                            <div class="form-group col-sm-12">
                              {!! Form::label('reference_url', 'Reference', array('class' => 'control-label')) !!}
                              {!! Form::url('reference_url','',['placeholder' => 'Reference Url','class' => 'form-control']); !!}
                              @if ($errors->has('reference_url'))
                                  <p class="help-block error_login">
                                      <strong>{{ $errors->first('reference_url') }}</strong>
                                  </p>
                              @endif
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="attachment">Attachment</label>
                                <input type="file" class="form-control" name="attachment" placeholder="Attachment File" id="attachment">
                            </div>
                            <p class="text-right col-sm-12">
                                <button type="submit" class="btn btn-new">Add</button>
                            </p>
                      {!! Form::close() !!}

                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>
@endsection
