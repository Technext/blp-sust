@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Resource Types</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="{{ url('/dashboard/resource_types') }}" class="btn btn-new btn_new_active">Manage Resource Types</a>
                                    <a href="{{ url('/dashboard/resource_types/create') }}" class="btn btn-new">Create Resource Type</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Slug</th>
                                                <th>Description</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($resource_types))
                                            @foreach($resource_types as $resource_type)
                                            <tr>
                                                <td>{{ $resource_type->name }}</td>
                                                <td>{{ $resource_type->slug }}</td>
                                                <td>{{ $resource_type->description }}</td>
                                                <td>
                                                    <a class="btn btn-xs btn-primary" href="{{ url('/dashboard/resource_types/' . $resource_type->id . '/edit') }}"><i class="fa fa-pencil"></i></a>

                                                    <a onclick="return confirm('Are you sure ?')"  href="{{ url('/dashboard/resource_types/' . $resource_type->id . '/delete') }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



@endsection