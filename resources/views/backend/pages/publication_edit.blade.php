@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <h3 class="head_title">Edit publication</h3>

                    <p>
                        <a href="{{ url('/dashboard/publication') }}" class="btn btn-new">Manage publications</a>
                        <a href="{{ url('/dashboard/publication/book/create') }}" class="btn btn-new {{ (Request::segment(3)=='book' ? 'btn_new_active' : '') }}">Add Book</a>
                        <a href="{{ url('/dashboard/publication/conference/create') }}" class="btn btn-new {{ (Request::segment(3)=='conference' ? 'btn_new_active' : '') }}">Add Conference Paper</a>
                        <a href="{{ url('/dashboard/publication/journal/create') }}" class="btn btn-new {{ (Request::segment(3)=='journal' ? 'btn_new_active' : '') }}">Add Journal</a>
                    </p>


<?php
// echo '<pre>';
// print_r($datas);
// exit;

?>

{!! Form::open(['url' => action('Backend\PublicationController@update'), 'method' => 'POST', 'id' => 'updateForm','class' => 'row', 'role'=>'form', 'enctype'=>'multipart/form-data']) !!}

{{ csrf_field() }}


<!-- Common Stsrt -->

<input hidden="hidden" type="hidden" name="type_of_publication" value="{{$datas['type_of_publication']}}">
<input hidden="hidden" type="hidden" name="publication_id" value="{{$datas['id']}}">

<div class="form-group col-sm-12">
  {!! Form::label('title', 'Title *', array('class' => 'control-label')) !!}
  {!! Form::text('title',$datas['title'],['placeholder' => 'title','class' => 'form-control','required' => 'required']); !!}
  @if ($errors->has('title'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('title') }}</strong>
      </p>
  @endif
</div>

@if($datas['type_of_publication']=="books")
<div class="form-group col-sm-12">
  {!! Form::label('booktitle', 'Book Title', array('class' => 'control-label')) !!}
  {!! Form::text('booktitle',$datas['book_title'],['placeholder' => 'Book Title','class' => 'form-control','required' => 'required']); !!}
  @if ($errors->has('booktitle'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('booktitle') }}</strong>
      </p>
  @endif
</div>
@endif


<div class="form-group col-sm-12">
  {!! Form::label('notes', 'Notes', array('class' => 'control-label')) !!}
  {!! Form::textarea('notes',$datas['note'],[ 'cols'=>'30', 'rows'=>'10','class' => 'form-control textArea']); !!}
  <!--<textarea name="notes" cols="30" rows="10" class="form-control textArea">{{ $datas['note'] }}</textarea>-->
  @if ($errors->has('notes'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('notes') }}</strong>
      </p>
  @endif
</div>


<div class="form-group col-sm-12">
    <input type="checkbox" name="is_public" id="show-publicly" {{ $datas['is_public']==1 ? 'checked' : ''}}>
    <label for="show-publicly">Show Publicly?</label>
</div>

<div class="form-group col-sm-12">
    <input type="checkbox" name="is_profile_only" id="show-profile-only" {{ $datas['is_profile_only']==1 ? 'checked' : ''}}>
    <label for="show-profile-only">Show Profile Only?</label>
</div>


<div class="form-group col-sm-12">
  {!! Form::label('author', 'Author', array('class' => 'control-label')) !!}
  {!! Form::text('author',$datas['author'],['placeholder' => 'Author','class' => 'form-control','required' => 'required']); !!}
  @if ($errors->has('author'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('author') }}</strong>
      </p>
  @endif
</div>


<div class="form-group col-sm-12">
  {!! Form::label('year', 'Year', array('class' => 'control-label')) !!}
  {!! Form::text('year',date('Y-m-d',strtotime($datas['published_at'])),['placeholder' => 'Year Published At','class' => 'form-control']); !!}
  @if ($errors->has('year'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('year') }}</strong>
      </p>
  @endif
</div>


<div class="form-group col-sm-12">
  {!! Form::label('address', 'Address', array('class' => 'control-label')) !!}
  {!! Form::text('address',$datas['address'],['placeholder' => 'Address','class' => 'form-control']); !!}
  @if ($errors->has('address'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('address') }}</strong>
      </p>
  @endif
</div>

<!-- Common End -->


@if(($datas['type_of_publication']=="books") OR ($datas['type_of_publication']=="journal"))

<div class="form-group col-sm-12">
  {!! Form::label('editor', 'Editor', array('class' => 'control-label')) !!}
  {!! Form::text('editor',$datas['editor'],['placeholder' => 'Editor','class' => 'form-control']); !!}
  @if ($errors->has('editor'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('editor') }}</strong>
      </p>
  @endif
</div>

<div class="form-group col-sm-12">
  {!! Form::label('publisher', 'Publisher', array('class' => 'control-label')) !!}
  {!! Form::text('publisher',$datas['publisher'],['placeholder' => 'Publisher','class' => 'form-control']); !!}
  @if ($errors->has('publisher'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('publisher') }}</strong>
      </p>
  @endif
</div>


<div class="form-group col-sm-12">
  {!! Form::label('pages', 'Pages', array('class' => 'control-label')) !!}
  {!! Form::text('pages',$datas['pages'],['placeholder' => 'Pages','class' => 'form-control']); !!}
  @if ($errors->has('pages'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('pages') }}</strong>
      </p>
  @endif
</div>

@endif


@if($datas['type_of_publication']=="books")

<div class="form-group col-sm-12">
  {!! Form::label('series', 'Series', array('class' => 'control-label')) !!}
  {!! Form::text('series',$datas['series'],['placeholder' => 'Series','class' => 'form-control']); !!}
  @if ($errors->has('series'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('series') }}</strong>
      </p>
  @endif
</div>

@endif


@if($datas['type_of_publication']=="journal")


<div class="form-group col-sm-12">
  {!! Form::label('abstract', 'Abstract', array('class' => 'control-label')) !!}
  {!! Form::textarea('abstract',$datas['abstract'],[ 'cols'=>'30', 'rows'=>'10','class' => 'form-control textArea']); !!}
  @if ($errors->has('abstract'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('abstract') }}</strong>
      </p>
  @endif
</div>



<div class="form-group col-sm-12">
  {!! Form::label('journal', 'Journal', array('class' => 'control-label')) !!}
  {!! Form::text('journal',$datas['journal'],['placeholder' => 'Journal','class' => 'form-control']); !!}
  @if ($errors->has('journal'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('journal') }}</strong>
      </p>
  @endif
</div>

<div class="form-group col-sm-12">
  {!! Form::label('volume', 'Volume', array('class' => 'control-label')) !!}
  {!! Form::text('volume',$datas['volume'],['placeholder' => 'Volume','class' => 'form-control']); !!}
  @if ($errors->has('volume'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('volume') }}</strong>
      </p>
  @endif
</div>

@endif



@if(($datas['type_of_publication']=="conference") OR ($datas['type_of_publication']=="journal"))

<div class="form-group col-sm-12">
  {!! Form::label('institution', 'Institution', array('class' => 'control-label')) !!}
  {!! Form::text('institution',$datas['institution'],['placeholder' => 'Institution name','class' => 'form-control']); !!}
  @if ($errors->has('institution'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('institution') }}</strong>
      </p>
  @endif
</div>

@endif


<div class="form-group col-sm-12">
  {!! Form::label('reference_url', 'Reference', array('class' => 'control-label')) !!}
  {!! Form::url('reference_url',$datas['reference_url'],['placeholder' => 'Reference Url','class' => 'form-control']); !!}
  @if ($errors->has('reference_url'))
      <p class="help-block error_login">
          <strong>{{ $errors->first('reference_url') }}</strong>
      </p>
  @endif
</div>


<div class="form-group col-sm-12">
    <label for="attachment">Attachment </label>
    <input type="file" class="form-control" name="attachment" placeholder="Attachment File" id="attachment">
    @if($datas['file_url'])
    <p><strong><a href="{{asset('assets/publications/'.$datas['file_url'])}}" target="_blank">Download</a></strong></p>
    @endif

    
</div>

<p class="text-right col-sm-12">
    <button type="submit" class="btn btn-new">Update</button>
</p>

{!! Form::close() !!}
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>
@endsection
