@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <h3 class="head_title">Publications journal</h3>

                    <p>
                        <a href="{{ url('/dashboard/publication') }}" class="btn btn-new btn_new_active">Manage publications</a>
                        <a href="{{ url('/dashboard/publication/book/create') }}" class="btn btn-new {{ (Request::segment(3)=='book' ? 'btn_new_active' : '') }}">Add Book</a>
                        <a href="{{ url('/dashboard/publication/conference/create') }}" class="btn btn-new {{ (Request::segment(3)=='conference' ? 'btn_new_active' : '') }}">Add Conference Paper</a>
                        <a href="{{ url('/dashboard/publication/journal/create') }}" class="btn btn-new {{ (Request::segment(3)=='journal' ? 'btn_new_active' : '') }}">Add Journal</a>
                    </p>

                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <tbody>

                                @if($datas['title'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Title</strong></td>
                                    <td width="75%">{{$datas['title']}}</td>
                                </tr>
                                @endif

                                @if($datas['type_of_publication'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Publication type</strong></td>
                                    <td width="75%">{{$datas['type_of_publication']}}</td>
                                </tr>
                                @endif

                                @if($datas['published_at'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Publication date</strong></td>
                                    <td width="75%">{{date('F j, Y',strtotime($datas['published_at']))}}</td>
                                </tr>
                                @endif

                                @if($datas['book_title'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Book title</strong></td>
                                    <td width="75%">{{$datas['book_title']}}</td>
                                </tr>
                                @endif

                                @if($datas['author'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Author</strong></td>
                                    <td width="75%">{{$datas['author']}}</td>
                                </tr>
                                @endif

                                @if($datas['editor'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Editor</strong></td>
                                    <td width="75%">{{$datas['editor']}}</td>
                                </tr>
                                @endif

                                @if($datas['publisher'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Publisher</strong></td>
                                    <td width="75%">{{$datas['publisher']}}</td>
                                </tr>
                                @endif

                                @if($datas['abstract'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Abstract</strong></td>
                                    <td width="75%">{!!$datas['abstract']!!}</td>
                                </tr>
                                @endif

                                @if($datas['address'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Address</strong></td>
                                    <td width="75%">{{$datas['address']}}</td>
                                </tr>
                                @endif

                                @if($datas['pages'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Pages</strong></td>
                                    <td width="75%">{{$datas['pages']}}</td>
                                </tr>
                                @endif

                                @if($datas['series'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Series</strong></td>
                                    <td width="75%">{{$datas['series']}}</td>
                                </tr>
                                @endif

                                @if($datas['note'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Note</strong></td>
                                    <td width="75%">{!!$datas['note']!!}</td>
                                </tr>
                                @endif

                                @if($datas['journal'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Journal</strong></td>
                                    <td width="75%">{{$datas['journal']}}</td>
                                </tr>
                                @endif

                                @if($datas['volume'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Volume</strong></td>
                                    <td width="75%">{{$datas['volume']}}</td>
                                </tr>
                                @endif

                                @if($datas['institution'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Institution</strong></td>
                                    <td width="75%">{{$datas['institution']}}</td>
                                </tr>
                                @endif

                                @if($datas['file_url'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>File</strong></td>
                                    <td width="75%"><a target="_blank" href="{{asset('assets/publications/'.$datas['file_url'])}}">Download</a></td>
                                </tr>
                                @endif

                                @if($datas['reference_url'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Reference URL</strong></td>
                                    <td width="75%"><a href="{{$datas['reference_url']}}">{{$datas['reference_url']}}</a></td>
                                </tr>
                                @endif

                                @if($datas['is_public'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Is public ?</strong></td>
                                    <td width="75%">{{ $datas['is_public'] ? 'Yes' : 'No' }}</td>
                                </tr>
                                @endif

                                @if($datas['is_profile_only'])
                                <tr>
                                    <td class="text-center" width="25%"><strong>Is Profile ?</strong></td>
                                    <td width="75%">{{ $datas['is_profile_only'] ? 'Yes' : 'No' }}</td>
                                </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>
@endsection
