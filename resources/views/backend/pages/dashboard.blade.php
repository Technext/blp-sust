@extends('frontend.layout.master')
@section('css')

    <style type="text/css">

        /*Only for index page*/

        @media only screen and (max-width: 760px) {
          header{min-height: 70vh;}
        }

        @media only screen and (max-width: 480px) {
          header{min-height: 50vh !important;}
        }

        .ui-autocomplete-input, .ui-menu, .ui-menu-item {  z-index: 2006; }
          .img-box {width: 130px;height: 130px;}

    </style>

@endsection
@section('mainContent')

    <div class="main-wrapper"> 
        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">
                    <div class="profile">
                        <h3 class="head_title">My Profile</h3>
                        <div class="row">




                                <div class="col-md-4">

                                    @if(Auth::user()->image)
                                        <div class="img-box">
                                            <img class="profile-pic-img" src="{{ url('assets/profile/'.Auth::user()->image) }}" alt="Profile Picture">
                                        </div>
                                    @else
                                        <div class="img-box">
                                          <img class="profile-pic-img" src="{{ url('images/profile-placeholder.png') }}" alt="Profile Picture">
                                        </div>
                                    @endif

                                    <p class="text-center pro_name">{{ Auth::user()->name }}</p>

                                    @if($designation)
                                        <p class="text-center pro_designation">{{ $designation->name }}</p>
                                    @endif


                                    
                                </div>

                                <div class="col-md-8">

                                    @if(!empty(Auth::user()->phone_no))
                                        <p class="pro_designation"><strong>Contact number : </strong>{{ Auth::user()->phone_no }}</p>
                                    @endif

                                    <p class="pro_designation"><strong>Email : </strong>{{ Auth::user()->email }}</p>

                                    @if(isset(Auth::user()->researchFields))

                                        <p class="pro_designation"><strong>Research Field : </strong>{{ Auth::user()->researchFields->implode('name', ', ') }}</p>

                                    @endif
                                    @if(isset(Auth::user()->supervisorDetails))

                                        <p class="pro_designation"><strong>Supervisor : </strong>{{ Auth::user()->supervisorDetails->name }}</p>

                                    @endif

                                    <p class="pro_designation"><strong>Bio : </strong>
                                        @if(Auth::user()->bio)
                                            {!! Auth::user()->bio !!}
                                        @else
                                            Please enter your bio.
                                        @endif
                                    </p>
                                    <br>
                                    <p class="text-left">
                                        <a href="{{ url('/profile/edit') }}" class="btn btn-new">Edit</a>
                                    </p>

                                </div>

                        </div>
                    </div>            
                </div>

            </div>
        </div>
    </div>

@endsection
