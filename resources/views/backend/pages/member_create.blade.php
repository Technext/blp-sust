@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                        <h3  class="head_title">Add New Member</h3>

                            <p>
                                <a href="{{ url('/dashboard/members') }}" class="btn btn-new">Manage Member</a>
                                <a href="{{ url('/dashboard/members/create') }}" class="btn btn-new btn_new_active">Add New Member</a>
                            </p>

                            {!! Form::open(['url' => action('Backend\MembersController@store'), 'method' => 'POST', 'id' => 'resStore','class' => 'row', 'role'=>'form','enctype' => 'multipart/form-data']) !!}

                                {{ csrf_field() }}

                                <div class="form-group col-sm-12">
                                  {!! Form::label('name', 'Name *', array('class' => 'control-label')) !!}
                                  {!! Form::text('name', '', ['class' => 'form-control','required' => 'required']); !!}
                                  @if ($errors->has('name'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </p>
                                  @endif
                                </div>


                                <div class="form-group col-sm-12">
                                  {!! Form::label('email', 'Email *', array('class' => 'control-label')) !!}
                                  {!! Form::email('email', '', ['class' => 'form-control','required' => 'required']); !!}
                                  @if ($errors->has('email'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('phone_no', 'Phone Number', array('class' => 'control-label')) !!}
                                  {!! Form::text('phone_no', '', ['class' => 'form-control','required' => 'required']); !!}
                                  @if ($errors->has('phone_no'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('phone_no') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('password', 'Password *', array('class' => 'control-label')) !!}
                                  <input type="password" name="password" class="form-control" placeholder="Password" required>
                                  @if ($errors->has('password'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('designation', 'Designation', array('class' => 'control-label')) !!}
                                  {{ Form::select('designation', $designations, '', ['id' => 'designation','class' => 'form-control selectChosen']) }}
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('dept_name', 'Department', array('class' => 'control-label')) !!}
                                  {!! Form::text('dept_name','', ['class' => 'form-control']); !!}
                                  @if ($errors->has('dept_name'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('dept_name') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('institute', 'Institute', array('class' => 'control-label')) !!}
                                  {!! Form::text('institute', '', ['class' => 'form-control']); !!}
                                  @if ($errors->has('institute'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('institute') }}</strong>
                                      </p>
                                  @endif
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('research_fields', 'Research Fields', array('class' => 'control-label')) !!}
                                  {{ Form::select('research_fields[]', $research_fields, '', ['id' => 'research_fields','class' => 'form-control selectChosen','multiple' => 'multiple']) }}
                                </div>


                                <div class="form-group col-sm-12">
                                    <label for="supervisor">Supervisor</label>
                                    <select name="supervisor" id="supervisor" class="form-control selectChosen">
                                        <option value="">Select supervisor</option>
                                        @foreach($faculties as $faculty)
                                            <option value="{{ $faculty->id }}">{{ $faculty->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-sm-12">
                                  {!! Form::label('bio', 'Bio', array('class' => 'control-label')) !!}
                                  {!! Form::textarea('bio', '', ['class' => 'form-control textArea','placeholder'=>'Enter Something About Member...','cols'=>15]); !!}
                                  @if ($errors->has('bio'))
                                      <p class="help-block error_login">
                                          <strong>{{ $errors->first('bio') }}</strong>
                                      </p>
                                  @endif
                                </div>


                                <div class="form-group col-sm-12">
                                    <label for="image">Profile Picture &nbsp; &nbsp;<span class="text-danger">(Picture must be : 200px x 200px)</span></label>
                                    <input type="file" name="image" class="form-control">
                                </div>


                                <div class="form-group col-sm-12 text-right">
                                    <button type="submit" class="btn btn-new">Create</button>
                                </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                    
                </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>


@endsection