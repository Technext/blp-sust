@extends('frontend.layout.master')
@section('mainContent')

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                @include('backend.sidebar')

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3  class="head_title">View People</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="{{ url('/dashboard/peoples') }}" class="btn btn-new btn_new_active">Manage People</a>
                                    <a href="{{ url('/dashboard/peoples/create') }}" class="btn btn-new">Add New People</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <tbody>
                                        @if($person)

                                            <tr>
                                                <td class="text-center" width="25%"><strong>Name</strong></td>
                                                <td width="75%">{{ $person->name }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Email</strong></td>
                                                <td width="75%">{{ $person->email }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Phone number</strong></td>
                                                <td width="75%">{{ $person->phone_no }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Designation</strong></td>
                                                <td width="75%">

                                                    @if($designations)
                                                    @foreach($designations as $designation)
                                                        @if($designation->id==$person->designation)
                                                            {{ $designation->name }}
                                                        @endif
                                                    @endforeach
                                                    @endif

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Image</strong></td>
                                                <td width="75%">
                                                    @if($person->image)
                                                        <img height="80px" src="{{ url('assets/profile/'.$person->image) }}" alt="Profile Pic" class="profile-pic">
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>BIO</strong></td>
                                                <td width="75%">{!!  $person->bio  !!}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Status</strong></td>
                                                <td width="75%">{{ $person->type }}</td>
                                            </tr>


                                        @else
                                            <tr>
                                                <td><h3>Sorry, this user doesn't exists</h3></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();
        // $('.image').filer({
        //     showThumbs: true
        // });

    });
</script>


@endsection