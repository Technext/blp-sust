@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">Members</h2>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div style="padding: 30px;" class="hidden-xs hidden-sm"></div>
                @if(isset($members) && count($members) > 0)
                    @foreach($members as $member)
                    <div class="col-md-4">
                        <div class="board-card">
                            <div class="img-box">
                                @if(isset($member->image))
                                    <img src="{{ url('/assets/profile/' . $member->image) }}" alt="Member Profile Image">
                                @else
                                    <img src="{{ url('/images/profile-placeholder.jpg') }}" alt="Member Profile Image">
                                @endif
                            </div>
                            <p class="text-center name_p">{{ $member->name }}</p>
                            <p class="text-center designation_p">{{ $member->designationDetails ? $member->designationDetails->name : '' }}</p>
                            <p class="text-center designation_p">{{ $member->dept_name ? $member->dept_name : 'Department Of Computer Science and Engineering' }}</p>
                            <p class="text-center designation_p">{{ $member->institute_name ? $member->institute_name : 'Shahjalal University of Science and Technology, Sylhet' }}</p>
                            <p class="text-center designation_p"><span><a href="{{ url('members/'.$member->id) }}">View Profile</a></span></p>

                        </div>
                    </div>
                @endforeach
            @endif
        </div>

    </div>

    <div class="m-b-120"></div>


@endsection