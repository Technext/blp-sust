@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">View member</h2>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="faculty-member">
            <div class="row">
                <div class="col-md-3">
                    @if(isset($member->image))
                        <img src="{{ url('/assets/profile/' . $member->image) }}" alt="Member Profile Image">
                    @else
                        <img src="{{ url('/images/profile-placeholder.jpg') }}" alt="Member Profile Image">
                    @endif
                </div>
                <div class="col-md-5">
                    <h4 style="margin: 0;">{{$member->name}}</h4>
                    <h5>{{ $member->designationDetails->name }}</h5>
                    <p class="designation_p_2">{{ $member->dept_name }}</p>
                    <p class="designation_p_2">{{ $member->institute_name }}</p>
                    <p class="designation_p_2"><span class="bold">Email:</span> {{ $member->email }}</p>
                    <p class="designation_p_2"><span class="bold">Phone No:</span> {{ $member->phone_no }}</p>
                </div>
                <div class="col-md-4">
                    <p class="designation_p_2"><span class="bold">Research Fields:</span> {{ $member->researchFields->implode('name', ', ') }}</p>
                    @if(isset($member->supervisorDetails))
                        <p class="designation_p_2"><span class="bold">Supervisor:</span> {{ $member->supervisorDetails->name }}</p>
                    @endif
                    <p class="bio designation_p_2"><strong>BIO : </strong> {!! $member->bio!="" ? $member->bio : 'N/A' !!}</p>
                </div>
                <div class="clear-fix"></div>
            </div>

            @if(isset($member->publications) && count($member->publications))
            <br>
            <br>
            <div class="publication">
                <div class="col-md-12">
                    
                    <div class="row">
                        <h4>Publications :</h4>
                    </div>
                    <div class="row box-dsgn-thead">
                        <div class="col-md-4">
                            <p class="">Title</p>
                        </div>
                        <div class="col-md-3">
                            <p class="">Author</p>
                        </div>
                        <div class="col-md-2">
                            <p class="">File</p>
                        </div>
                        <div class="col-md-2">
                            <p class="">Publication date</p>
                        </div>
                        <div class="col-md-1">
                            <p class="">Action</p>
                        </div>
                    </div>
                    <hr class="row">
                    @foreach($member->publications as $publication)
                        <div class="row box-dsgn">
                            <div class="col-md-4">
                                <h5>
                                    <a class="publication_title" href="{{ url('/'.$publication->type_of_publication.'/'.$publication->id) }}">{{ $publication->title }}</a>
                                </h5>
                            </div>
                            <div class="col-md-3">
                                <p class="publication_author">{{ $publication->author }}</p>
                            </div>
                            <div class="col-md-2">
                                <p>
                                    @if(isset($publication->file_url))
                                        <a  class="publication_image" href="{{ url('assets/publications/'.$publication->file_url) }}" target="_blank">Download</a>
                                    @else
                                        <p>N/A</p>
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-2">
                                <p class="text-left">{{ \Carbon\Carbon::parse($publication->published_at)->format('d M, Y') }}</p>
                            </div>
                            <div class="col-md-1">
                                <a class="publication_view" href="{{ url('/'.$publication->type_of_publication.'/'.$publication->id) }}">View</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>

    <div class="m-b-120"></div>


@endsection