@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">{{ $pubType }} Publications</h2>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-12">
            <div style="padding: 15px;" class="hidden-xs hidden-sm"></div>
            <div class="row box-dsgn-thead">
                <div class="col-md-4">
                    <p class="">Title</p>
                </div>
                <div class="col-md-3">
                    <p class="">Author</p>
                </div>
                <div class="col-md-2">
                    <p class="">File</p>
                </div>
                <div class="col-md-2">
                    <p class="">Publication date</p>
                </div>
                <div class="col-md-1">
                    <p class="">Action</p>
                </div>
            </div>
            <hr class="row">
            @if(isset($publication))
            <div class="row publication">
                <div class="col-md-12">
                    @foreach($publication as $singlePublication)
                        <div class="row box-dsgn">
                            <div class="col-md-4">
                                <h5>
                                    <a class="publication_title" href="{{ url('/'.$singlePublication->type_of_publication.'/'.$singlePublication->id) }}">{{ $singlePublication->title }}</a>
                                </h5>
                            </div>
                            <div class="col-md-3">
                                <p class="publication_author">{{ $singlePublication->author }}</p>
                            </div>
                            <div class="col-md-2">
                                <p>
                                    @if(isset($singlePublication->file_url))
                                        <a  class="publication_image" href="{{ url('assets/publications/'.$singlePublication->file_url) }}" target="_blank">Download</a>
                                    @else
                                        <p>N/A</p>
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-2">
                                <p class="text-left">{{ \Carbon\Carbon::parse($singlePublication->published_at)->format('d M, Y') }}</p>
                            </div>
                            <div class="col-md-1">
                                <a class="publication_view" href="{{ url('/'.$singlePublication->type_of_publication.'/'.$singlePublication->id) }}">View</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @endif

            </div>
        </div>

    </div>
    <div class="m-b-120"></div>


@endsection