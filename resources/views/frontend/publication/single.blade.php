@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">{{ $pubType }} Publications</h2>
            </div>
        </div>
    </div>
    <br>
    <div class="container">

        <div class="row">
            <div class="col-md-12">
            @if(isset($publication))
            <div class="row publication">
                <div class="col-md-12">

                    <div class="journal">
                        <h4>{{ $publication->title }}</h4>

                        <p class="pub_p">

                            @if(isset($publication->author))
                                <span class="pub_span">{{ $publication->author }} | </span>
                            @endif
                            <span class="pub_span">{{ \Carbon\Carbon::parse($publication->published_at)->format('d M, Y') }}</span>

                        </p>

                        <div class="row journal_details">

                        @if(isset($publication->abstract) || isset($publication->note))

                            <div class="col-md-8">

                                @if(isset($publication->abstract))
                                <div class="abstract">
                                    <p class="pub_p"><span class="bold">Abstract : </span>
                                        {!! $publication->abstract !!}</p>
                                </div>
                                @endif

                                @if($publication->note)
                                <div class="notes">
                                    <br>
                                    <p><span class="bold">Note : </span>
                                        {!! $publication->note !!}
                                    </p>
                                </div>
                                @endif

                            </div>

                        @endif

                        @if(isset($publication->abstract) || isset($publication->note))
                            <div class="col-md-4">
                        @else
                            <div class="col-md-12">
                        @endif
                                @if(isset($publication->editor))
                                <div class="editor">
                                    <p>
                                        <span class="bold">Editor : </span> {{ $publication->editor }}
                                    </p>
                                </div>
                                @endif
                                @if(isset($publication->publisher))
                                    <div class="publisher">
                                        <p>
                                            <span class="bold">Publisher : </span> {{ $publication->publisher }}
                                        </p>
                                    </div>
                                @endif
                                @if(isset($publication->institution))
                                    <div class="institution">
                                        <p>
                                            <span class="bold">Institute : </span> {{ $publication->institution }}
                                        </p>
                                    </div>
                                @endif
                                @if(isset($publication->address))
                                    <div class="address">
                                        <p>
                                            <span class="bold">Address : </span> {{ $publication->address }}
                                        </p>
                                    </div>
                                @endif

                                @if(isset($publication->journal))
                                    <div class="journal">
                                        <p>
                                            <span class="bold">Journal : </span> {{ $publication->journal }}
                                        </p>
                                    </div>
                                @endif
                                @if(isset($publication->volume))
                                    <div class="publisher">
                                        <p>
                                            <span class="bold">Volume : </span> {{ $publication->volume }}
                                        </p>
                                    </div>
                                @endif
                                @if(isset($publication->pages))
                                    <div class="pages">
                                        <p>
                                            <span class="bold">Pages : </span> {{ $publication->pages }}
                                        </p>
                                    </div>
                                @endif

                                @if(isset($publication->file_url))
                                    <div class="attachment">
                                        <p>
                                            <span class="bold">Attachment : </span> <a target="_blank" href="{{ url('/assets/publications/'. $publication->file_url) }}">Download</a>
                                        </p>
                                    </div>
                                @endif


                                @if(isset($publication->reference_url))
                                <div class="reference-url">
                                    <p>
                                        <span class="bold">Reference URL: </span><a href="{{ $publication->reference_url }}">{{ $publication->reference_url }}</a>
                                    </p>
                                </div>
                                @endif
                            </div>



                        </div>



                    </div>


                </div>
            </div>
            @endif

            </div>
        </div>

    </div>
    <div class="m-b-120"></div>


@endsection