@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">{{$researchField->name}}</h2>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
    <div class="research-single-wrapper">
        @if(isset($researchField) && isset($researchField->description))
            <p>{{ $researchField->description }}</p>
        @endif
        @if(isset($researchField) && isset($researchField->members) && count($researchField->members) > 0)
            <div class="members-list-wrapper">
                <h4>Researchers :</h4>
                <hr>
                <div class="row">
                @foreach($researchField->members as $member)

                    <div class="col-md-4">
                        <div class="board-card">
                            <div class="img-box">
                                @if(isset($member->image))
                                    <img src="{{ url('/assets/profile/' . $member->image) }}" alt="Member Profile Image">
                                @else
                                    <img src="{{ url('/images/profile-placeholder.jpg') }}" alt="Member Profile Image">
                                @endif
                            </div>
                            <p class="text-center name_p">{{ $member->name }}</p>
                            <p class="text-center designation_p">{{ $member->designationDetails->name }}</p>
                            <p class="text-center designation_p">{{ $member->dept_name ? $member->dept_name : 'Department Of Computer Science and Engineering' }}</p>
                            <p class="text-center designation_p">{{ $member->institute_name ? $member->institute_name : 'Shahjalal University of Science and Technology, Sylhet' }}</p>
                            <p class="text-center designation_p"><span><a href="{{ url('members/'.$member->id) }}">View Profile</a></span></p>

                        </div>
                    </div>

                @endforeach
                </div>

            </div>
        @endif

        <div style="float: left;width: 100%;"></div>
        <br>
        <br>

        @if(isset($supervisors) && count($supervisors) > 0)
            <div class="supervisor-list-wrapper">

                <div class="m-b-40"></div>
                <h4>Supervisors :</h4>
                <hr>
                <div class="row">
                @foreach($supervisors as $member)



                    <div class="col-md-4">
                        <div class="board-card">
                            <div class="img-box">
                                @if(isset($member->image))
                                    <img src="{{ url('/assets/profile/' . $member->image) }}" alt="Member Profile Image">
                                @else
                                    <img src="{{ url('/images/profile-placeholder.jpg') }}" alt="Member Profile Image">
                                @endif
                            </div>
                            <p class="text-center name_p">{{ $member->name }}</p>
                            <p class="text-center designation_p">{{ $member->designationDetails->name }}</p>
                            <p class="text-center designation_p">{{ $member->dept_name ? $member->dept_name : 'Department Of Computer Science and Engineering' }}</p>
                            <p class="text-center designation_p">{{ $member->institute_name ? $member->institute_name : 'Shahjalal University of Science and Technology, Sylhet' }}</p>
                            <p class="text-center designation_p"><span><a href="{{ url('faculty/'.$member->id) }}">View Profile</a></span></p>

                        </div>
                    </div>


                @endforeach
                </div>
            </div>
        @endif
    </div>
    </div>

    <div class="m-b-120"></div>


@endsection