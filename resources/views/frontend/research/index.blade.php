@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">Research fields</h2>
            </div>
        </div>
    </div>
        @if(isset($researchFields) && count($researchFields) > 0)
            <?php $flag = 0; ?>
            @foreach($researchFields as $researchField)
                <div class="container">
                    <div class="row research-block">
                        @if($flag == 0)
                            <div class="col-md-6 preview">
                                <img class="img_style" src="{{ url('/assets/research_fields/'.$researchField->preview_image) }}" alt="Research Field Preview Image">
                            </div>
                            <div class="col-md-6 text text-center">
                                <h3 class="front_title_2">{{ $researchField->name }}</h3>
                                <p class="text-center">{{ substr($researchField->description, 0, 150) }}</p>
                                <a href="{{ url('/research/' . $researchField->slug) }}" class="btn btn-view-profile">Read More</a>
                            </div>
                            <?php $flag = 1; ?>

                        @else
                            <div class="col-md-6 text text-center">
                                <h3 class="front_title_2">{{ $researchField->name }}</h3>
                                <p class="text-center">{{ substr($researchField->description, 0, 150) }}</p>
                                <a href="{{ url('/research/' . $researchField->slug) }}" class="btn btn-view-profile">Read More</a>

                            </div>
                            <div class="col-md-6 preview">
                                <img class="img_style" src="{{ url('/assets/research_fields/'.$researchField->preview_image) }}" alt="Research Field Preview Image">
                            </div>
                            <?php $flag = 0; ?>
                        @endif

                    </div>
                </div>
                <div class="m-b-80"></div>
            @endforeach
        @endif
    </div>
    <div class="m-b-120"></div>


@endsection