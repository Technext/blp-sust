@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">About Technext</h2>
            </div>
        </div>
    </div>
    <div class="container section">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8 col-md-offset-2 text-center col-sm-12 col-xs-12">
                    <img class="margin-bottom-20" src="{{ asset('images/tn-logo.png') }}" alt="Technext Logo">
                    <p class="h4 text-center margin-bottom-50">
                        Technext Limited is a software company based in Sylhet, Bangladesh. It started it's journey
                        in 29th April,2012 with only 2 members. Now it has more than 20 people working on various projects.
                        It has a large portfolio for international projects along with some outstanding national
                        projects as well that solved the real life problems of many.
                    </p>
                    <a target="_blank" href="http://technext.it" class="btn btn-new">Visit website</a>
                </div>
            </div>
        </div>
    </div>
    <div class="m-b-120"></div>


@endsection