
    <div class="footer-widget-area">
        <div class="container">
            <div class="col-md-4">
                <h5>Useful Links</h5>
                <hr>
                <p class="bold"><a href="http://www.heqep-ugc.gov.bd/">Higher Education Quality Enhancement Project (HEQEP)</a></p>
                <p class="bold"><a href="http://www.sust.edu/departments/cse">Dept. of Computer Science and Engineering, SUST, Sylhet</a></p>
                <p class="bold"><a href="http://www.sust.edu/">Shahjalal University Of Science and Technology, Sylhet</a></p>
            </div>
            <div class="col-md-5">
                <h5>Lab Contact</h5>
                <hr>
                <p class="bold">SUST CSE Bangla Language Processing (BLP) Group</p>
                <p class="bold">IICT Building</p>
                <p class="bold">Kumargaon, Sylhet-3114 Bangladesh</p>
                <p class="bold">TEL: PABX : 880-821-713491, 714479, 713850, 717850, 716123, 71539</p>
                <p class="bold">FAX: 880-821-715257, 725050</p>
            </div>
            <div class="col-md-3">
                <h5>Stay Connected</h5>
                <hr>
                <ul class="social-icons">


                  @if($themeDatas->facebook)
                    <li><a href="{{ $themeDatas->facebook }}" target="_blank"><i class="fa fa-facebook-official"></i></a></li>
                  @endif

                  @if($themeDatas->twitter)
                    <li><a href="{{ $themeDatas->twitter }}" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                  @endif

                  @if($themeDatas->youtube)
                    <li><a href="{{ $themeDatas->youtube }}" target="_blank"><i class="fa fa-youtube-square"></i></a></li>
                  @endif

                  @if($themeDatas->github)
                    <li><a href="{{ $themeDatas->github }}" target="_blank"><i class="fa fa-github-square"></i></a></li>
                  @endif

                  @if($themeDatas->bitbucket)
                    <li><a href="{{ $themeDatas->bitbucket }}" target="_blank"><i class="fa fa-bitbucket-square"></i></a></li>
                  @endif
                    
                </ul>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="col-md-6">
                <p>© {{ \Carbon\Carbon::now()->year }} SUST CSE Bangla Language Processing (BLP) Group.</p>
            </div>
            <div class="col-md-6">
                <p class="text-right">Developed by <a class="bold" href="{{ url('/about-technext') }}">Technext</a></p>
            </div>
        </div>
    </div>
    
    <script src="{{ asset('common/toast/iziToast.min.js') }}"></script>
    <script src="{{ asset('frontend/js/custom.js') }}"></script>
    @yield('js')

    <script>

        $(".selectChosen").chosen({
          no_results_text: "No data found !"
        });

        $("#year").datepicker();

        @if (Session::has('sess_alert'))
            @php
                $alertData = session::get('sess_alert');
            @endphp
            iziToast.success({
                title: '{!!$alertData['status']!!}',
                message: '{!!$alertData['message']!!}',
                position: 'topRight',
                timeout: 3000,
                transitionIn: 'fadeInDown',
                transitionOut: 'fadeOut',
                transitionInMobile: 'fadeInUp',
                transitionOutMobile: 'fadeOutDown',
            });
        @endif

        function toast(response){

          if(response.status=='success'){
            iziToast.success({
              title: response.status,
              message: response.message,
              position: 'topRight',
              timeout: 3000,
              transitionIn: 'fadeInDown',
              transitionOut: 'fadeOut',
              transitionInMobile: 'fadeInUp',
              transitionOutMobile: 'fadeOutDown',
            });
          }

          if(response.status=='error'){
            iziToast.error({
              title: response.status,
              message: response.message,
              position: 'topRight',
              timeout: 3000,
              transitionIn: 'fadeInDown',
              transitionOut: 'fadeOut',
              transitionInMobile: 'fadeInUp',
              transitionOutMobile: 'fadeOutDown',
            });
          }

        }

    </script>

</body>
</html>
