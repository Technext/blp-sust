@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">View administrator</h2>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
    <div class="faculty-member">
        <div class="row">
            <div class="col-md-3">

            @if(isset($faculty->image))
                <img src="{{ url('/assets/profile/' . $faculty->image) }}" alt="Member Profile Image">
            @else
                <img src="{{ url('/images/profile-placeholder.jpg') }}" alt="Member Profile Image">
            @endif

                                
            </div>
            <div class="col-md-9">
                <h4 style="margin: 0;">{{$faculty->name}}</h4>
                <h5>{{ $faculty->designationDetails->name }}</h5>
                <p class="designation_p_2">Department of Computer Science and Engineering</p>
                <p class="designation_p_2">Shahjalal University of Science and Technology</p>
                <p class="designation_p_2"><span class="bold">Email:</span> {{ $faculty->email }}</p>
                <p class="designation_p_2"><span class="bold">Phone No:</span> {{ $faculty->phone_no }}</p>
            </div>
            <div class="clear-fix"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <hr>
                <p class="bio designation_p_2"><strong>BIO : </strong> {!! $faculty->bio!="" ? $faculty->bio : 'N/A' !!}</p>
            </div>
        </div>
    </div>
    </div>

    <div class="m-b-120"></div>


@endsection