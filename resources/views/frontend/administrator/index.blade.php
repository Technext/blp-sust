@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">Administrator</h2>
            </div>
        </div>
    </div>
    <div class="container">


        <div class="row">
            <div style="padding: 30px;" class="hidden-xs hidden-sm"></div>
                @if(isset($faculties) && count($faculties) > 0)
                    @foreach($faculties as $faculty)
                    <div class="col-md-4">
                        <div class="board-card">
                            <div class="img-box">
                                @if(isset($faculty->image))
                                    <img src="{{ url('/assets/profile/' . $faculty->image) }}" alt="Member Profile Image">
                                @else
                                    <img src="{{ url('/images/profile-placeholder.jpg') }}" alt="Member Profile Image">
                                @endif
                            </div>
                            <p class="text-center name_p">{{ $faculty->name }}</p>
                            <p class="text-center designation_p">{{ $faculty->designationDetails->name }}</p>
                            <p class="text-center designation_p">{{ $faculty->dept_name ? $faculty->dept_name : 'Department Of Computer Science and Engineering' }}</p>
                            <p class="text-center designation_p">{{ $faculty->institute_name ? $faculty->institute_name : 'Shahjalal University of Science and Technology, Sylhet' }}</p>
                            <p class="text-center designation_p"><span><a href="{{ url('administrator/'.$faculty->id) }}">View Profile</a></span></p>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

    </div>
    <div class="m-b-120"></div>


@endsection