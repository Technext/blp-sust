@extends('frontend.layout.master')
@section('css')
    <style>
        .null_attach{
            width: 100%;
            padding: 20px;
            padding-top: 100px;
            height: 300px;
            font-size: 24px;
            color: white;
            text-align: center;
            background: #002147b8;
            margin: 0;
        }
    </style>
@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">Resource type details</h2>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-12">
            <div class="row publication">
                <div class="col-md-12">

                    <div class="journal">

                        <div class="row journal_details">

                            <div class="col-md-12">
                                <h4>{{ $resource->title }}</h4>
                                
                                <p class="pub_p"><span class="pub_span">{{ \Carbon\Carbon::parse($resource->updated_at)->format('d M, Y') }}</span></p>
                                
                                <br>

                                @if(isset($resource->version))
                                    <p class="pub_p"><span class="pub_span">Version : </span>{{ $resource->version }}</p>
                                @endif

                                @if(isset($resource->keywords))
                                    <p class="pub_p"><span class="pub_span">Keywords : </span>{{ $resource->keywords }}</p>
                                @endif
                                
                                @if(isset($resource->description))
                                    <p class="pub_p"><span class="pub_span">Description : </span>{!! $resource->description !!}</p>
                                @endif

                                @if(isset($resource->reference_url))
                                    <p class="pub_p"><span class="pub_span">Reference : </span>{{ $resource->reference_url }}</p>
                                @endif

                                @if(isset($resource->files) && count($resource->files) > 0)
                                <div class="attachments">
                                <p class="pub_p pub_span">Attachments :</p>
                                <div class="attachment-list">
                                <div class="row">
                                @foreach($resource->files as $file)
                                    <div class="col-md-4 attachment">
                                        <h5>{{ $file->file_name }}</h5>
                                        @if($file->file_extension=='doc' || $file->file_extension=='docx' || $file->file_extension=='pptx' || $file->file_extension=='pdf' || $file->file_extension=='xls' || $file->file_extension=='xlsx')
                                        <iframe src="https://docs.google.com/gview?url={{ url('/assets/resources/'.$file->file_name) }}&embedded=true" frameborder="0" width="100%" height="300px"></iframe>
                                        @elseif($file->file_extension=='png' || $file->file_extension=='PNG' || $file->file_extension=='jpg' || $file->file_extension=='JPG' || $file->file_extension=='jpeg' || $file->file_extension=='JPEG' || $file->file_extension=='gif' || $file->file_extension=='GIF')
                                        <img src="{{ url('/assets/resources/'.$file->file_name) }}" height="300px" style="margin: 0 auto;display:block;"/>
                                        @else
                                        <p class="null_attach">You have to download to see this attachment.</p>
                                        @endif
                                        <a target="_blank" href="{{ url('/assets/resources/'.$file->file_name) }}" class="btn btn-download btn-sm">Download <i class="fa fa-download" aria-hidden="true"></i></a>
                                    </div>
                                            {{--<div class="col-md-9"><a href="{{ url('/assets/'.$file->fileLocation().'/'.$file->file_storage_name.'.'.$file->file_extension) }}">{{ $file->file_name }}</a></div>--}}
                                @endforeach
                                </div>
                                </div>
                                </div>
                                @endif

                            </div>



                        </div>



                    </div>


                </div>
            </div>
            </div>
        </div>

    </div>
    <div class="m-b-120"></div>


@endsection