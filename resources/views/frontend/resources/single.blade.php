@extends('frontend.layout.master')
@section('css')

@endsection
@section('mainContent')

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">Resources : {{ $typeName }}</h2>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-12">

            <div style="padding: 15px;" class="hidden-xs hidden-sm"></div>
            <div class="row box-dsgn-thead">
                <div class="col-md-5">
                    <p class="">Title</p>
                </div>
                <div class="col-md-3">
                    <p class="">Type</p>
                </div>
                <div class="col-md-3">
                    <p class="">Publication date</p>
                </div>
                <div class="col-md-1">
                    <p class="">Action</p>
                </div>
            </div>
            <hr class="row">

            <div class="row publication">
                <div class="col-md-12">

                    <div class="row publication">
                        <div class="col-md-12">
                        @if(isset($resources) && count($resources) > 0)
                            @foreach($resources as $resource)
                                <div class="row box-dsgn">
                                    <div class="col-md-5">
                                        <h5>
                                            <a class="publication_title" href="{{url('/resource/types/'.$resource->id)}}">{{ $resource->title }}</a>
                                        </h5>
                                    </div>
                                    <div class="col-md-3">
                                        <p class="publication_author">{{ $resource->type->name }}</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p class="text-left">{{ \Carbon\Carbon::parse($resource->updated_at)->format('M-d-Y') }}</p>
                                    </div>
                                    <div class="col-md-1">
                                        <a class="publication_view" href="{{url('/resource/types/'.$resource->id)}}">View</a>
                                    </div>
                                </div>
                                @endforeach
                            @else
                                <h5 class="text-center">Sorry, there is no resource available right now!!</h5>
                            @endif
                        </div>
                    </div>

                </div>
            </div>

            </div>
        </div>

    </div>
    <div class="m-b-120"></div>


@endsection