<div class="form-group">
    <label for="research_fields">Research Fields</label>
    <select name="research_fields[]" id="research_fields" class="form-control" multiple="multiple">
        @foreach($research_fields as $research_field)
            <option value="{{ $research_field->id }}" {{ isset($selected) && in_array($research_field->id, $selected->pluck('id')->all()) ? 'selected' : '' }}>{{ $research_field->name }}</option>
        @endforeach
    </select>
</div>