<div class="form-group">
    <label for="">Designation</label>
    <input type="hidden" name="selected_designation" value="{{ $selected }}">
    <select name="designation" id="designation" class="form-control">
        @foreach($designations as $designation)
            <option id="{{ $designation->id }}" class="{{$designation->type}}" value="{{ $designation->id }}" {{ (isset($selected) && ($selected == $designation->id)) ? 'selected' : '' }}>{{ $designation->name }}</option>
        @endforeach
    </select>
</div>