/**
 * Created by aqib on 10/26/16.
 */

jQuery(document).ready(function ($) {

    /** =================================== Navigation Menu =========================================== **/

    var navbar      = jQuery('.main-navigation'),
        width       = Math.max(jQuery(window).width(), window.innerWidth),
        mobileTest;

    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        mobileTest = true;
    }

    var navTop = $(".main-navigation").offset().top;
    // if(navTop > 0){
    //     $(".main-navigation").addClass("navbar-dark");
    // }
    // else {
    //     $(".main-navigation").removeClass("navbar-dark");
    // }

    /* ---------------------------------------------- /*
     * Navbar submenu
     /* ---------------------------------------------- */

    function navbarSubmenu(width) {
        if (width > 767) {
            jQuery('.main-navigation .navbar-nav > li.dropdown').hover(function() {
                var MenuLeftOffset  = jQuery('.dropdown-menu', jQuery(this)).offset().left;
                var Menu1LevelWidth = jQuery('.dropdown-menu', jQuery(this)).width();
                if (width - MenuLeftOffset < Menu1LevelWidth * 2) {
                    jQuery(this).children('.dropdown-menu').addClass('leftauto');
                } else {
                    jQuery(this).children('.dropdown-menu').removeClass('leftauto');
                }
                if (jQuery('.dropdown', jQuery(this)).length > 0) {
                    var Menu2LevelWidth = jQuery('.dropdown-menu', jQuery(this)).width();
                    if (width - MenuLeftOffset - Menu1LevelWidth < Menu2LevelWidth) {
                        jQuery(this).children('.dropdown-menu').addClass('left-side');
                    } else {
                        jQuery(this).children('.dropdown-menu').removeClass('left-side');
                    }
                }
            });
        }
    }

    /* ---------------------------------------------- /*
     * Navbar hover dropdown on desctop
     /* ---------------------------------------------- */

    function hoverDropdown(width, mobileTest) {
        if ((width > 767) && (mobileTest !== true)) {
            jQuery('.main-navigation .navbar-nav > li.dropdown, .main-navigation li.dropdown > ul > li.dropdown').removeClass('open');
            var delay = 0;
            var setTimeoutConst;
            jQuery('.main-navigation .navbar-nav > li.dropdown, .main-navigation li.dropdown > ul > li.dropdown').hover(function() {
                    var $this = jQuery(this);
                    setTimeoutConst = setTimeout(function() {
                        $this.addClass('open');
                        $this.find('.dropdown-toggle').addClass('disabled');
                    }, delay);
                },
                function() {
                    clearTimeout(setTimeoutConst);
                    jQuery(this).removeClass('open');
                    jQuery(this).find('.dropdown-toggle').removeClass('disabled');
                });
        } else {
            jQuery('.main-navigation .navbar-nav > li.dropdown, .main-navigation li.dropdown > ul > li.dropdown').unbind('mouseenter mouseleave');
            jQuery('.main-navigation [data-toggle=dropdown]').not('.binded').addClass('binded').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
                jQuery(this).parent().siblings().removeClass('open');
                jQuery(this).parent().siblings().find('[data-toggle=dropdown]').parent().removeClass('open');
                jQuery(this).parent().toggleClass('open');
            });
        }
    }

    /* ---------------------------------------------- /*
     * Navbar collapse on click
     /* ---------------------------------------------- */

    jQuery(document).on('click','.navbar-collapse.in',function(e) {
        if( jQuery(e.target).is('a') && jQuery(e.target).attr('class') != 'dropdown-toggle' ) {
            jQuery(this).collapse('hide');
        }
    });


    navbarSubmenu(width);
    hoverDropdown(width, mobileTest);

    jQuery(window).resize(function() {
        var width = Math.max(jQuery(window).width(), window.innerWidth);
        hoverDropdown(width, mobileTest);
    });

    jQuery(document).on('scroll', function (e) {
        // console.log($(".main-navigation").offset().top);
        var top = $(".main-navigation").offset().top;
        // if(top > 0){
        //     $(".main-navigation").addClass("navbar-dark");
        // }
        // else {
        //     $(".main-navigation").removeClass("navbar-dark");
        // }
    });

    /** ========================================= Navigation Menu Ends ======================================== **/

    /** ----------------------------------------- *
     *  Home page carousel
     *  ----------------------------------------- */
    var homepageSlider = $(".frontend.home .header-slider");
    if(homepageSlider){
        console.log(homepageSlider);
        homepageSlider.owlCarousel({
            items: 1,
            loop: true,
            mouseDrag: true,
            touchDrag: true,
            lazyLoad: true,
            autoplay: true,
            autoplayTimeout: 10000,
            autoplaySpeed: 1000,
            autoplayHoverPause: false,
            nav: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
        });
    }

    var downArrow = $(".frontend.home .down-arrow a");
    if(downArrow){
        downArrow.on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: $("#content").offset().top - 100
            }, 'slow');
        });
    }
});