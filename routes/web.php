<?php

Route::get('/clear', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:clear');
    return 'DONE';
});

// Frontend panel

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about-technext', 'HomeController@aboutTechnext')->name('about-technext');
Route::get('/research', 'HomeController@research')->name('research');
Route::get('/research/{slug}', 'HomeController@researchSingle');

Route::get('/members', 'HomeController@allMembers');
Route::get('/members/{id}', 'HomeController@singleMember');

Route::get('/faculty', 'HomeController@allFaculty');
Route::get('/faculty/{id}', 'HomeController@singleFaculty');

Route::get('/administrator', 'HomeController@allAdministrator');
Route::get('/administrator/{id}', 'HomeController@singleAdministrator');

Route::get('/journals', 'HomeController@allJournals');
Route::get('/journal/{id}', 'HomeController@singleJournals');

Route::get('/conference', 'HomeController@allConference');
Route::get('/conference/{id}', 'HomeController@singleConference');

Route::get('/books', 'HomeController@allBooks');
Route::get('/books/{id}', 'HomeController@singleBooks');

Route::get('/resource/{slug}', 'HomeController@singleReesourceTypes');
Route::get('/resource/types/{id}', 'HomeController@singleReesourceTypeDetails');

// Dashoard Panel
Auth::routes();
Route::group(['middleware'=>['auth']],function(){

	Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

	// Profile
	Route::get('/profile', 'DashboardController@index')->name('dashboard.profile');
	Route::get('/profile/edit', 'DashboardController@profile')->name('profile.edit');
	Route::post('/update', 'DashboardController@update');

	// Publication
	Route::get('/dashboard/publication', 'Backend\PublicationController@index')->name('dashboard.publication');
	Route::get('/dashboard/publication/show/{id}', 'Backend\PublicationController@show');
    Route::get('/dashboard/publication/{id}/delete', 'Backend\PublicationController@delete');

	Route::get('/dashboard/publication/{id}/edit', 'Backend\PublicationController@edit');
	Route::post('/dashboard/publication/update', 'Backend\PublicationController@update')->name('publication.update');

    // Publication journal
	Route::get('/dashboard/publication/journal/create', 'Backend\PublicationJournalController@index')->name('publication.create');
	Route::post('/dashboard/publication/journal/store', 'Backend\PublicationJournalController@store')->name('publication.store');


    // Publication conference
	Route::get('/dashboard/publication/conference/create', 'Backend\PublicationConferenceController@index')->name('publication.create');
	Route::post('/dashboard/publication/conference/store', 'Backend\PublicationConferenceController@store')->name('publication.store');


    // Publication book
	Route::get('/dashboard/publication/book/create', 'Backend\PublicationBookController@index')->name('publication.create');
	Route::post('/dashboard/publication/book/store', 'Backend\PublicationBookController@store')->name('publication.store');


	// Resources
	Route::get('/dashboard/resources', 'Backend\ResourceController@index')->name('dashboard.resources');
	Route::get('/dashboard/resources/create', 'Backend\ResourceController@create');
	Route::post('/dashboard/resources/store', 'Backend\ResourceController@store');
	Route::get('/dashboard/resources/show/{id}', 'Backend\ResourceController@show');
    Route::get('/dashboard/resources/{id}/edit', 'Backend\ResourceController@edit');
    Route::post('/dashboard/resources/update', 'Backend\ResourceController@update');
    Route::get('/dashboard/resources/{id}/delete', 'Backend\ResourceController@delete');
    Route::get('/dashboard/resources/file/delete/{id}', 'Backend\ResourceController@deleteResFile');

	// Resource Types
	Route::get('/dashboard/resource_types', 'Backend\ResourceTypeController@index')->name('dashboard.resource_types');
	Route::get('/dashboard/resource_types/create', 'Backend\ResourceTypeController@create');
	Route::post('/dashboard/resource_types/store', 'Backend\ResourceTypeController@store');

	Route::get('/dashboard/resource_types/{id}/edit', 'Backend\ResourceTypeController@edit');
	Route::post('/dashboard/resource_types/update', 'Backend\ResourceTypeController@update');

    Route::get('/dashboard/resource_types/{id}/delete', 'Backend\ResourceTypeController@delete');


	// Lab
	Route::get('/dashboard/lab', 'Backend\LabFacilitiesController@index')->name('dashboard.lab');
	Route::get('/dashboard/lab/create', 'Backend\LabFacilitiesController@create');
	Route::post('/dashboard/lab/store', 'Backend\LabFacilitiesController@store');
    Route::get('/dashboard/lab/{id}/edit', 'Backend\LabFacilitiesController@edit');
    Route::post('/dashboard/lab/update', 'Backend\LabFacilitiesController@update');
    Route::get('/dashboard/lab/{id}/delete', 'Backend\LabFacilitiesController@delete');

	// Members
	Route::get('/dashboard/members', 'Backend\MembersController@index')->name('dashboard.members');
	Route::get('/dashboard/members/show/{id}', 'Backend\MembersController@show');
	Route::get('/dashboard/members/create', 'Backend\MembersController@create');
	Route::post('/dashboard/members/store', 'Backend\MembersController@store');
    Route::get('/dashboard/members/{id}/edit', 'Backend\MembersController@edit');
    Route::post('/dashboard/members/update', 'Backend\MembersController@update');
    Route::get('/dashboard/members/{id}/delete', 'Backend\MembersController@delete');
    Route::get('/dashboard/members/{id}/toggle-active', 'Backend\MembersController@changeActive');


	// Peoples
	Route::get('/dashboard/peoples', 'Backend\PeopleController@index')->name('dashboard.peoples');
	Route::get('/dashboard/peoples/create', 'Backend\PeopleController@create');
	Route::get('/dashboard/peoples/show/{id}', 'Backend\PeopleController@show');
	Route::post('/dashboard/peoples/store', 'Backend\PeopleController@store');
    Route::get('/dashboard/peoples/{id}/edit', 'Backend\PeopleController@edit');
    Route::post('/dashboard/peoples/update', 'Backend\PeopleController@update');
    Route::get('/dashboard/peoples/{id}/delete', 'Backend\PeopleController@delete');
    Route::get('/dashboard/peoples/{id}/toggle-active', 'Backend\PeopleController@changeActive');


    // Homepage Editor
    Route::get('/dashboard/setting/homepage','Backend\SettingController@index');
    Route::post('/dashboard/setting/homepage-update','Backend\SettingController@update');

    // Footer Editor
    Route::get('/dashboard/setting/footer','Backend\SettingController@footer');
    Route::post('/dashboard/setting/footer-update','Backend\SettingController@updateFooter');

});