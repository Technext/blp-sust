<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3  class="head_title">Add New Resource</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="<?php echo e(url('/dashboard/resources')); ?>" class="btn btn-new">Manage Resources</a>
                                    <a href="<?php echo e(url('/dashboard/resources/create')); ?>" class="btn btn-new btn_new_active">Create Resource</a>
                                </p>

                                <?php echo Form::open(['url' => action('Backend\ResourceController@store'), 'method' => 'POST', 'id' => 'resStore','class' => 'row', 'role'=>'form','enctype' => 'multipart/form-data']); ?>


                                    <?php echo e(csrf_field()); ?>


                                    <div class="form-group col-sm-12">
                                        <?php echo Form::label('title', 'Title *', array('class' => 'control-label')); ?>

                                        <?php echo Form::text('title','',['placeholder' => 'Title','class' => 'form-control','required' => 'required']);; ?>

                                        <?php if($errors->has('title')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('title')); ?></strong>
                                          </p>
                                        <?php endif; ?>
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <?php echo Form::label('keywords', 'Keywords', array('class' => 'control-label')); ?>

                                        <?php echo Form::text('keywords','',['placeholder' => 'key1,key2','class' => 'form-control']);; ?>

                                        <p>Enter all the keywords here seperated by comma.</p>
                                        <?php if($errors->has('keywords')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('keywords')); ?></strong>
                                          </p>
                                        <?php endif; ?>
                                    </div>


                                    <div class="form-group col-sm-12">
                                        <?php echo Form::label('description', 'Description', array('class' => 'control-label')); ?>

                                        <?php echo Form::textarea('description','',['placeholder' => 'Description','class' => 'form-control textArea']);; ?>

                                        <?php if($errors->has('description')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('description')); ?></strong>
                                          </p>
                                        <?php endif; ?>
                                    </div>


                                    <div class="form-group col-sm-12">
                                        <label for="resource-types-select">Type</label>
                                        <select name="resource_type" id="resource-types-select" class="form-control">
                                            <?php $__currentLoopData = $resource_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($type->id); ?>" <?php echo e(isset($selected) && ($type->id == $selected->id) ? 'selected' : ''); ?>><?php echo e($type->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>


                                    <div class="form-group col-sm-12">
                                        <label for="image">Files</label>
                                        <input type="file" name="image[]" id="image" class="form-control" multiple="multiple">
                                    </div>


                                    <div class="form-group col-sm-12">
                                        <?php echo Form::label('reference_url', 'Reference', array('class' => 'control-label')); ?>

                                        <?php echo Form::text('reference_url','',['placeholder' => 'Reference URL','class' => 'form-control']);; ?>

                                        <p>Enter the reference url of your resource in this field.</p>
                                        <?php if($errors->has('reference_url')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('reference_url')); ?></strong>
                                          </p>
                                        <?php endif; ?>
                                    </div>


                                    <div class="form-group col-sm-12">
                                        <input type="checkbox" name="is_public" id="show-publicly" checked>
                                        <label for="show-publicly">Show Publicly?</label>
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <?php echo Form::label('version', 'Version', array('class' => 'control-label')); ?>

                                        <?php echo Form::text('version','',['placeholder' => 'Version','class' => 'form-control']);; ?>

                                        <?php if($errors->has('version')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('version')); ?></strong>
                                          </p>
                                        <?php endif; ?>
                                    </div>

                                    <p class="text-right col-sm-12">
                                        <button type="submit" class="btn btn-new">Add Resource</button>
                                    </p>

                                <?php echo Form::close(); ?>


                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/plugins/table/ui/trumbowyg.table.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/plugins/colors/ui/trumbowyg.colors.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/plugins/table/trumbowyg.table.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.23.0/plugins/colors/trumbowyg.colors.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        // $('.image').filer({
        //     showThumbs: true
        // });



        $('.textArea')
        .trumbowyg({

            btns: [
                ['table'],
                ['foreColor', 'backColor'],
                ['viewHTML'],
                ['undo', 'redo'], // Only supported in Blink browsers
                ['formatting'],
                ['strong', 'em', 'del'],
                ['superscript', 'subscript'],
                ['link'],
                ['insertImage'],
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['unorderedList', 'orderedList'],
                ['horizontalRule'],
                ['removeformat'],
                ['fullscreen']
            ]

        });

    });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>