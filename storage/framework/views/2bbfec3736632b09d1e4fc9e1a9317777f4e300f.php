
<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                        <h3  class="head_title">Add new People</h3>

                        <p>
                            <a href="<?php echo e(url('/dashboard/peoples')); ?>" class="btn btn-new">Manage People</a>
                            <a href="<?php echo e(url('/dashboard/peoples/create')); ?>" class="btn btn-new btn_new_active">Add New People</a>
                        </p>


                            <?php echo Form::open(['url' => action('Backend\PeopleController@store'), 'method' => 'POST', 'id' => 'resStore','class' => 'row', 'role'=>'form','enctype' => 'multipart/form-data']); ?>


                                <?php echo e(csrf_field()); ?>


                                <div class="form-group col-sm-12">
                                  <?php echo Form::label('name', 'Name *', array('class' => 'control-label')); ?>

                                  <?php echo Form::text('name', '', ['class' => 'form-control','required' => 'required']);; ?>

                                  <?php if($errors->has('name')): ?>
                                      <p class="help-block error_login">
                                          <strong><?php echo e($errors->first('name')); ?></strong>
                                      </p>
                                  <?php endif; ?>
                                </div>


                                <div class="form-group col-sm-12">
                                  <?php echo Form::label('email', 'Email *', array('class' => 'control-label')); ?>

                                  <?php echo Form::email('email', '', ['class' => 'form-control','required' => 'required']);; ?>

                                  <?php if($errors->has('email')): ?>
                                      <p class="help-block error_login">
                                          <strong><?php echo e($errors->first('email')); ?></strong>
                                      </p>
                                  <?php endif; ?>
                                </div>

                                <div class="form-group col-sm-12">
                                  <?php echo Form::label('phone_no', 'Phone Number', array('class' => 'control-label')); ?>

                                  <?php echo Form::text('phone_no', '', ['class' => 'form-control','required' => 'required']);; ?>

                                  <?php if($errors->has('phone_no')): ?>
                                      <p class="help-block error_login">
                                          <strong><?php echo e($errors->first('phone_no')); ?></strong>
                                      </p>
                                  <?php endif; ?>
                                </div>

                                <div class="form-group col-sm-6">
                                  <?php echo Form::label('type', 'Type', array('class' => 'control-label')); ?>

                                  <?php echo e(Form::select('type', [ 'academic' => 'Academic', 'admin' => 'Administrative' ], '', ['id' => 'type','class' => 'form-control selectChosen'])); ?>

                                </div>


                                <div class="form-group col-sm-6">
                                  <?php echo Form::label('designation', 'Designation', array('class' => 'control-label')); ?>

                                  <?php echo e(Form::select('designation', $designations, '', ['id' => 'designation','class' => 'form-control selectChosen'])); ?>

                                </div>


                                <div class="form-group col-sm-12">
                                  <?php echo Form::label('bio', 'Bio', array('class' => 'control-label')); ?>

                                  <?php echo Form::textarea('bio', '', ['class' => 'form-control textArea','placeholder'=>'Enter Something About Member...','cols'=>15]);; ?>

                                  <?php if($errors->has('bio')): ?>
                                      <p class="help-block error_login">
                                          <strong><?php echo e($errors->first('bio')); ?></strong>
                                      </p>
                                  <?php endif; ?>
                                </div>


                                <div class="form-group col-sm-12">
                                    <label for="image">Profile Picture &nbsp; &nbsp;<span class="text-danger">(Recomended size : 200px x 200px)</span></label>
                                    <input type="file" name="image" class="form-control">
                                </div>


                                <div class="form-group col-sm-12 text-right">
                                    <button type="submit" class="btn btn-new">Create</button>
                                </div>
                            <?php echo Form::close(); ?>


                        </div>
                    </div>
                    
                </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>