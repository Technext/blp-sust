
<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <h3 class="head_title">Publications journal</h3>

                    <p>
                        <a href="<?php echo e(url('/dashboard/publication')); ?>" class="btn btn-new btn_new_active">Manage publications</a>
                        <a href="<?php echo e(url('/dashboard/publication/book/create')); ?>" class="btn btn-new <?php echo e((Request::segment(3)=='book' ? 'btn_new_active' : '')); ?>">Add Book</a>
                        <a href="<?php echo e(url('/dashboard/publication/conference/create')); ?>" class="btn btn-new <?php echo e((Request::segment(3)=='conference' ? 'btn_new_active' : '')); ?>">Add Conference Paper</a>
                        <a href="<?php echo e(url('/dashboard/publication/journal/create')); ?>" class="btn btn-new <?php echo e((Request::segment(3)=='journal' ? 'btn_new_active' : '')); ?>">Add Journal</a>
                    </p>

                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <tbody>

                                <?php if($datas['title']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Title</strong></td>
                                    <td width="75%"><?php echo e($datas['title']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['type_of_publication']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Publication type</strong></td>
                                    <td width="75%"><?php echo e($datas['type_of_publication']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['published_at']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Publication date</strong></td>
                                    <td width="75%"><?php echo e(date('F j, Y',strtotime($datas['published_at']))); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['book_title']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Book title</strong></td>
                                    <td width="75%"><?php echo e($datas['book_title']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['author']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Author</strong></td>
                                    <td width="75%"><?php echo e($datas['author']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['editor']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Editor</strong></td>
                                    <td width="75%"><?php echo e($datas['editor']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['publisher']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Publisher</strong></td>
                                    <td width="75%"><?php echo e($datas['publisher']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['abstract']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Abstract</strong></td>
                                    <td width="75%"><?php echo $datas['abstract']; ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['address']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Address</strong></td>
                                    <td width="75%"><?php echo e($datas['address']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['pages']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Pages</strong></td>
                                    <td width="75%"><?php echo e($datas['pages']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['series']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Series</strong></td>
                                    <td width="75%"><?php echo e($datas['series']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['note']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Note</strong></td>
                                    <td width="75%"><?php echo $datas['note']; ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['journal']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Journal</strong></td>
                                    <td width="75%"><?php echo e($datas['journal']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['volume']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Volume</strong></td>
                                    <td width="75%"><?php echo e($datas['volume']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['institution']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Institution</strong></td>
                                    <td width="75%"><?php echo e($datas['institution']); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['file_url']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>File</strong></td>
                                    <td width="75%"><a target="_blank" href="<?php echo e(asset('assets/publications/'.$datas['file_url'])); ?>">Download</a></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['reference_url']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Reference URL</strong></td>
                                    <td width="75%"><a href="<?php echo e($datas['reference_url']); ?>"><?php echo e($datas['reference_url']); ?></a></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['is_public']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Is public ?</strong></td>
                                    <td width="75%"><?php echo e($datas['is_public'] ? 'Yes' : 'No'); ?></td>
                                </tr>
                                <?php endif; ?>

                                <?php if($datas['is_profile_only']): ?>
                                <tr>
                                    <td class="text-center" width="25%"><strong>Is Profile ?</strong></td>
                                    <td width="75%"><?php echo e($datas['is_profile_only'] ? 'Yes' : 'No'); ?></td>
                                </tr>
                                <?php endif; ?>

                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>