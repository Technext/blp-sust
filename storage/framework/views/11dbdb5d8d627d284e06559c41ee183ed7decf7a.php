
    <div class="footer-widget-area">
        <div class="container">
            <div class="col-md-4">
                <h5>Useful Links</h5>
                <hr>
                <p class="bold"><a href="http://www.heqep-ugc.gov.bd/">Higher Education Quality Enhancement Project (HEQEP)</a></p>
                <p class="bold"><a href="http://www.sust.edu/departments/cse">Dept. of Computer Science and Engineering, SUST, Sylhet</a></p>
                <p class="bold"><a href="http://www.sust.edu/">Shahjalal University Of Science and Technology, Sylhet</a></p>
            </div>
            <div class="col-md-5">
                <h5>Lab Contact</h5>
                <hr>
                <p class="bold">SUST CSE Bangla Language Processing (BLP) Group</p>
                <p class="bold">IICT Building</p>
                <p class="bold">Kumargaon, Sylhet-3114 Bangladesh</p>
                <p class="bold">TEL: PABX : 880-821-713491, 714479, 713850, 717850, 716123, 71539</p>
                <p class="bold">FAX: 880-821-715257, 725050</p>
            </div>
            <div class="col-md-3">
                <h5>Stay Connected</h5>
                <hr>
                <ul class="social-icons">


                  <?php if($themeDatas->facebook): ?>
                    <li><a href="<?php echo e($themeDatas->facebook); ?>" target="_blank"><i class="fa fa-facebook-official"></i></a></li>
                  <?php endif; ?>

                  <?php if($themeDatas->twitter): ?>
                    <li><a href="<?php echo e($themeDatas->twitter); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                  <?php endif; ?>

                  <?php if($themeDatas->youtube): ?>
                    <li><a href="<?php echo e($themeDatas->youtube); ?>" target="_blank"><i class="fa fa-youtube-square"></i></a></li>
                  <?php endif; ?>

                  <?php if($themeDatas->github): ?>
                    <li><a href="<?php echo e($themeDatas->github); ?>" target="_blank"><i class="fa fa-github-square"></i></a></li>
                  <?php endif; ?>

                  <?php if($themeDatas->bitbucket): ?>
                    <li><a href="<?php echo e($themeDatas->bitbucket); ?>" target="_blank"><i class="fa fa-bitbucket-square"></i></a></li>
                  <?php endif; ?>
                    
                </ul>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="col-md-6">
                <p>© <?php echo e(\Carbon\Carbon::now()->year); ?> SUST CSE Bangla Language Processing (BLP) Group.</p>
            </div>
            <div class="col-md-6">
                <p class="text-right">Developed by <a class="bold" href="<?php echo e(url('/about-technext')); ?>">Technext</a></p>
            </div>
        </div>
    </div>
    
    <script src="<?php echo e(asset('common/toast/iziToast.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/custom.js')); ?>"></script>
    <?php echo $__env->yieldContent('js'); ?>

    <script>

        $(".selectChosen").chosen({
          no_results_text: "No data found !"
        });

        $("#year").datepicker();

        <?php if(Session::has('sess_alert')): ?>
            <?php
                $alertData = session::get('sess_alert');
            ?>
            iziToast.success({
                title: '<?php echo $alertData['status']; ?>',
                message: '<?php echo $alertData['message']; ?>',
                position: 'topRight',
                timeout: 3000,
                transitionIn: 'fadeInDown',
                transitionOut: 'fadeOut',
                transitionInMobile: 'fadeInUp',
                transitionOutMobile: 'fadeOutDown',
            });
        <?php endif; ?>

        function toast(response){

          if(response.status=='success'){
            iziToast.success({
              title: response.status,
              message: response.message,
              position: 'topRight',
              timeout: 3000,
              transitionIn: 'fadeInDown',
              transitionOut: 'fadeOut',
              transitionInMobile: 'fadeInUp',
              transitionOutMobile: 'fadeOutDown',
            });
          }

          if(response.status=='error'){
            iziToast.error({
              title: response.status,
              message: response.message,
              position: 'topRight',
              timeout: 3000,
              transitionIn: 'fadeInDown',
              transitionOut: 'fadeOut',
              transitionInMobile: 'fadeInUp',
              transitionOutMobile: 'fadeOutDown',
            });
          }

        }

    </script>

</body>
</html>
