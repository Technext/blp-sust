
<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">

                    <h3>Footer settings</h3>
                    <h4>Social Links</h4>
                    <?php echo Form::open(['url' => action('Backend\SettingController@updateFooter'), 'method' => 'POST', 'id' => 'footerUpdate','class' => 'row', 'role'=>'form']); ?>


                        <?php echo e(csrf_field()); ?>


                        <div class="form-group col-sm-12">
                            <?php echo Form::label('facebook_url', 'Facebook Url', array('class' => 'control-label')); ?>

                            <?php echo Form::text('facebook_url',$footerSocialLinks->facebook,['class' => 'form-control']);; ?>

                            <?php if($errors->has('facebook_url')): ?>
                              <p class="help-block error_login">
                                  <strong><?php echo e($errors->first('facebook_url')); ?></strong>
                              </p>
                            <?php endif; ?>
                        </div>

                        <div class="form-group col-sm-12">
                            <?php echo Form::label('twitter_url', 'Twitter Url', array('class' => 'control-label')); ?>

                            <?php echo Form::text('twitter_url',$footerSocialLinks->twitter,['class' => 'form-control']);; ?>

                            <?php if($errors->has('twitter_url')): ?>
                              <p class="help-block error_login">
                                  <strong><?php echo e($errors->first('twitter_url')); ?></strong>
                              </p>
                            <?php endif; ?>
                        </div>

                        <div class="form-group col-sm-12">
                            <?php echo Form::label('youtube_url', 'Youtube Url', array('class' => 'control-label')); ?>

                            <?php echo Form::text('youtube_url',$footerSocialLinks->youtube,['class' => 'form-control']);; ?>

                            <?php if($errors->has('youtube_url')): ?>
                              <p class="help-block error_login">
                                  <strong><?php echo e($errors->first('youtube_url')); ?></strong>
                              </p>
                            <?php endif; ?>
                        </div>

                        <div class="form-group col-sm-12">
                            <?php echo Form::label('github_url', 'Github Url', array('class' => 'control-label')); ?>

                            <?php echo Form::text('github_url',$footerSocialLinks->github,['class' => 'form-control']);; ?>

                            <?php if($errors->has('github_url')): ?>
                              <p class="help-block error_login">
                                  <strong><?php echo e($errors->first('github_url')); ?></strong>
                              </p>
                            <?php endif; ?>
                        </div>

                        <div class="form-group col-sm-12">
                            <?php echo Form::label('bitbucket_url', 'Bitbucket Url', array('class' => 'control-label')); ?>

                            <?php echo Form::text('bitbucket_url',$footerSocialLinks->bitbucket,['class' => 'form-control']);; ?>

                            <?php if($errors->has('bitbucket_url')): ?>
                              <p class="help-block error_login">
                                  <strong><?php echo e($errors->first('bitbucket_url')); ?></strong>
                              </p>
                            <?php endif; ?>
                        </div>

                        <p class="text-right col-sm-12">
                            <button class="btn btn-new" type="submit">Update</button>
                        </p>

                    <?php echo Form::close(); ?>

                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>