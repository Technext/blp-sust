<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">Research fields</h2>
            </div>
        </div>
    </div>
        <?php if(isset($researchFields) && count($researchFields) > 0): ?>
            <?php $flag = 0; ?>
            <?php $__currentLoopData = $researchFields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $researchField): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="container">
                    <div class="row research-block">
                        <?php if($flag == 0): ?>
                            <div class="col-md-6 preview">
                                <img class="img_style" src="<?php echo e(url('/assets/research_fields/'.$researchField->preview_image)); ?>" alt="Research Field Preview Image">
                            </div>
                            <div class="col-md-6 text text-center">
                                <h3 class="front_title_2"><?php echo e($researchField->name); ?></h3>
                                <p class="text-center"><?php echo e(substr($researchField->description, 0, 150)); ?></p>
                                <a href="<?php echo e(url('/research/' . $researchField->slug)); ?>" class="btn btn-view-profile">Read More</a>
                            </div>
                            <?php $flag = 1; ?>

                        <?php else: ?>
                            <div class="col-md-6 text text-center">
                                <h3 class="front_title_2"><?php echo e($researchField->name); ?></h3>
                                <p class="text-center"><?php echo e(substr($researchField->description, 0, 150)); ?></p>
                                <a href="<?php echo e(url('/research/' . $researchField->slug)); ?>" class="btn btn-view-profile">Read More</a>

                            </div>
                            <div class="col-md-6 preview">
                                <img class="img_style" src="<?php echo e(url('/assets/research_fields/'.$researchField->preview_image)); ?>" alt="Research Field Preview Image">
                            </div>
                            <?php $flag = 0; ?>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="m-b-80"></div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </div>
    <div class="m-b-120"></div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>