<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3  class="head_title">View Resource</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="<?php echo e(url('/dashboard/resources')); ?>" class="btn btn-new btn_new_active">Manage Resources</a>
                                    <a href="<?php echo e(url('/dashboard/resources/create')); ?>" class="btn btn-new">Create Resource</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <tbody>

                                            <tr>
                                                <td class="text-center" width="25%"><strong>Title</strong></td>
                                                <td width="75%"><?php echo e($resources['title']); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Keywords</strong></td>
                                                <td width="75%"><?php echo e($resources['keywords']); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Description</strong></td>
                                                <td width="75%"><?php echo $resources['description']; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Resource type</strong></td>
                                                <td width="75%">
                                                    <?php $__currentLoopData = $resource_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resource): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($resource->id == $resources['resource_type']): ?>
                                                        <?php echo e($resource->name); ?>

                                                    <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Reference Url</strong></td>
                                                <td width="75%"><a href="<?php echo e($resources['reference_url']); ?>"><?php echo e($resources['reference_url']); ?></a></td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Is public ?</strong></td>
                                                <td width="75%"><?php echo e($resources['is_public']==1 ? 'Yes' : 'NO'); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Version</strong></td>
                                                <td width="75%"><?php echo e($resources['version']); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-center" width="25%"><strong>Files</strong></td>
                                                <td width="75%">
                                                    <?php if($resourceFiles): ?>
                                                    <?php $__currentLoopData = $resourceFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <a class="btn btn-xs btn-primary" href="<?php echo e(asset('assets/resources/'.$file->file_name)); ?>" target="_blank">Download</a>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();
        // $('.image').filer({
        //     showThumbs: true
        // });

    });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>