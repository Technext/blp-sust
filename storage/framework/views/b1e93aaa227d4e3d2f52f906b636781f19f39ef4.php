<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">Administrator</h2>
            </div>
        </div>
    </div>
    <div class="container">


        <div class="row">
            <div style="padding: 30px;" class="hidden-xs hidden-sm"></div>
                <?php if(isset($faculties) && count($faculties) > 0): ?>
                    <?php $__currentLoopData = $faculties; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $faculty): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class="board-card">
                            <div class="img-box">
                                <?php if(isset($faculty->image)): ?>
                                    <img src="<?php echo e(url('/assets/profile/' . $faculty->image)); ?>" alt="Member Profile Image">
                                <?php else: ?>
                                    <img src="<?php echo e(url('/images/profile-placeholder.jpg')); ?>" alt="Member Profile Image">
                                <?php endif; ?>
                            </div>
                            <p class="text-center name_p"><?php echo e($faculty->name); ?></p>
                            <p class="text-center designation_p"><?php echo e($faculty->designationDetails->name); ?></p>
                            <p class="text-center designation_p"><?php echo e($faculty->dept_name ? $faculty->dept_name : 'Department Of Computer Science and Engineering'); ?></p>
                            <p class="text-center designation_p"><?php echo e($faculty->institute_name ? $faculty->institute_name : 'Shahjalal University of Science and Technology, Sylhet'); ?></p>
                            <p class="text-center designation_p"><span><a href="<?php echo e(url('administrator/'.$faculty->id)); ?>">View Profile</a></span></p>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </div>

    </div>
    <div class="m-b-120"></div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>