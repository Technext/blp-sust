
<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3  class="head_title">Add New lab facilities</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="<?php echo e(url('/dashboard/lab')); ?>" class="btn btn-new">Manage lab lacilities</a>
                                    <a href="<?php echo e(url('/dashboard/lab/create')); ?>" class="btn btn-new btn_new_active">Create lab lacilities</a>
                                </p>

                                <?php echo Form::open(['url' => action('Backend\LabFacilitiesController@store'), 'method' => 'POST', 'id' => 'labStore','class' => 'row', 'role'=>'form','enctype' => 'multipart/form-data']); ?>


                                    <?php echo e(csrf_field()); ?>


                                    <div class="form-group col-sm-12">
                                        <?php echo Form::label('caption', 'Caption', array('class' => 'control-label')); ?>

                                        <?php echo Form::text('caption','',['placeholder' => 'Caption','class' => 'form-control']);; ?>

                                        <?php if($errors->has('caption')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('caption')); ?></strong>
                                          </p>
                                        <?php endif; ?>
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <label for="image">File</label>
                                        <input type="file" name="image" id="image" class="form-control" required="required">
                                    </div>

                                    <p class="text-right col-sm-12">
                                        <button type="submit" class="btn btn-new">Add</button>
                                    </p>

                                <?php echo Form::close(); ?>


                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>