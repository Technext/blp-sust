<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title"><?php echo e($pubType); ?> Publications</h2>
            </div>
        </div>
    </div>
    <br>
    <div class="container">

        <div class="row">
            <div class="col-md-12">
            <?php if(isset($publication)): ?>
            <div class="row publication">
                <div class="col-md-12">

                    <div class="journal">
                        <h4><?php echo e($publication->title); ?></h4>

                        <p class="pub_p">

                            <?php if(isset($publication->author)): ?>
                                <span class="pub_span"><?php echo e($publication->author); ?> | </span>
                            <?php endif; ?>
                            <span class="pub_span"><?php echo e(\Carbon\Carbon::parse($publication->published_at)->format('d M, Y')); ?></span>

                        </p>

                        <div class="row journal_details">

                        <?php if(isset($publication->abstract) || isset($publication->note)): ?>

                            <div class="col-md-8">

                                <?php if(isset($publication->abstract)): ?>
                                <div class="abstract">
                                    <p class="pub_p"><span class="bold">Abstract : </span>
                                        <?php echo $publication->abstract; ?></p>
                                </div>
                                <?php endif; ?>

                                <?php if($publication->note): ?>
                                <div class="notes">
                                    <br>
                                    <p><span class="bold">Note : </span>
                                        <?php echo $publication->note; ?>

                                    </p>
                                </div>
                                <?php endif; ?>

                            </div>

                        <?php endif; ?>

                        <?php if(isset($publication->abstract) || isset($publication->note)): ?>
                            <div class="col-md-4">
                        <?php else: ?>
                            <div class="col-md-12">
                        <?php endif; ?>
                                <?php if(isset($publication->editor)): ?>
                                <div class="editor">
                                    <p>
                                        <span class="bold">Editor : </span> <?php echo e($publication->editor); ?>

                                    </p>
                                </div>
                                <?php endif; ?>
                                <?php if(isset($publication->publisher)): ?>
                                    <div class="publisher">
                                        <p>
                                            <span class="bold">Publisher : </span> <?php echo e($publication->publisher); ?>

                                        </p>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($publication->institution)): ?>
                                    <div class="institution">
                                        <p>
                                            <span class="bold">Institute : </span> <?php echo e($publication->institution); ?>

                                        </p>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($publication->address)): ?>
                                    <div class="address">
                                        <p>
                                            <span class="bold">Address : </span> <?php echo e($publication->address); ?>

                                        </p>
                                    </div>
                                <?php endif; ?>

                                <?php if(isset($publication->journal)): ?>
                                    <div class="journal">
                                        <p>
                                            <span class="bold">Journal : </span> <?php echo e($publication->journal); ?>

                                        </p>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($publication->volume)): ?>
                                    <div class="publisher">
                                        <p>
                                            <span class="bold">Volume : </span> <?php echo e($publication->volume); ?>

                                        </p>
                                    </div>
                                <?php endif; ?>
                                <?php if(isset($publication->pages)): ?>
                                    <div class="pages">
                                        <p>
                                            <span class="bold">Pages : </span> <?php echo e($publication->pages); ?>

                                        </p>
                                    </div>
                                <?php endif; ?>

                                <?php if(isset($publication->file_url)): ?>
                                    <div class="attachment">
                                        <p>
                                            <span class="bold">Attachment : </span> <a target="_blank" href="<?php echo e(url('/assets/publications/'. $publication->file_url)); ?>">Download</a>
                                        </p>
                                    </div>
                                <?php endif; ?>


                                <?php if(isset($publication->reference_url)): ?>
                                <div class="reference-url">
                                    <p>
                                        <span class="bold">Reference URL: </span><a href="<?php echo e($publication->reference_url); ?>"><?php echo e($publication->reference_url); ?></a>
                                    </p>
                                </div>
                                <?php endif; ?>
                            </div>



                        </div>



                    </div>


                </div>
            </div>
            <?php endif; ?>

            </div>
        </div>

    </div>
    <div class="m-b-120"></div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>