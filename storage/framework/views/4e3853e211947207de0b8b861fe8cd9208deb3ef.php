<?php $__env->startSection('css'); ?>

    <style type="text/css">

        /*Only for index page*/

        @media  only screen and (max-width: 760px) {
          header{min-height: 70vh;}
        }

        @media  only screen and (max-width: 480px) {
          header{min-height: 50vh !important;}
        }

        .ui-autocomplete-input, .ui-menu, .ui-menu-item {  z-index: 2006; }
          .img-box {width: 130px;height: 130px;}

    </style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> 
        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">
                    <div class="profile">
                        <h3 class="head_title">My Profile</h3>
                        <div class="row">




                                <div class="col-md-4">

                                    <?php if(Auth::user()->image): ?>
                                        <div class="img-box">
                                            <img class="profile-pic-img" src="<?php echo e(url('assets/profile/'.Auth::user()->image)); ?>" alt="Profile Picture">
                                        </div>
                                    <?php else: ?>
                                        <div class="img-box">
                                          <img class="profile-pic-img" src="<?php echo e(url('images/profile-placeholder.png')); ?>" alt="Profile Picture">
                                        </div>
                                    <?php endif; ?>

                                    <p class="text-center pro_name"><?php echo e(Auth::user()->name); ?></p>

                                    <?php if($designation): ?>
                                        <p class="text-center pro_designation"><?php echo e($designation->name); ?></p>
                                    <?php endif; ?>


                                    
                                </div>

                                <div class="col-md-8">

                                    <?php if(!empty(Auth::user()->phone_no)): ?>
                                        <p class="pro_designation"><strong>Contact number : </strong><?php echo e(Auth::user()->phone_no); ?></p>
                                    <?php endif; ?>

                                    <p class="pro_designation"><strong>Email : </strong><?php echo e(Auth::user()->email); ?></p>

                                    <?php if(isset(Auth::user()->researchFields)): ?>

                                        <p class="pro_designation"><strong>Research Field : </strong><?php echo e(Auth::user()->researchFields->implode('name', ', ')); ?></p>

                                    <?php endif; ?>
                                    <?php if(isset(Auth::user()->supervisorDetails)): ?>

                                        <p class="pro_designation"><strong>Supervisor : </strong><?php echo e(Auth::user()->supervisorDetails->name); ?></p>

                                    <?php endif; ?>

                                    <p class="pro_designation"><strong>Bio : </strong>
                                        <?php if(Auth::user()->bio): ?>
                                            <?php echo Auth::user()->bio; ?>

                                        <?php else: ?>
                                            Please enter your bio.
                                        <?php endif; ?>
                                    </p>
                                    <br>
                                    <p class="text-left">
                                        <a href="<?php echo e(url('/profile/edit')); ?>" class="btn btn-new">Edit</a>
                                    </p>

                                </div>

                        </div>
                    </div>            
                </div>

            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>