
<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Lab facilities</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="<?php echo e(url('/dashboard/lab')); ?>" class="btn btn-new btn_new_active">Manage Lab facilities</a>
                                    <a href="<?php echo e(url('/dashboard/lab/create')); ?>" class="btn btn-new">Add Lab facilities</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Attatchment</th>
                                                <th>Caption</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($labFacilities)): ?>
                                            <?php $__currentLoopData = $labFacilities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $facility): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($facility->id); ?></td>
                                                <td><?php echo e($facility->caption); ?></td>
                                                <td><a target="_blank" href="<?php echo e(asset('assets/lab_facilities/'.$facility->image)); ?>">Download</a></td>
                                                <td>

                                                    <a href="<?php echo e(url('/dashboard/lab/' . $facility->id . '/edit')); ?>"class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>

                                                    <a onclick="return confirm('Are you sure ?')"  href="<?php echo e(url('/dashboard/lab/' . $facility->id . '/delete')); ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                                                </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>