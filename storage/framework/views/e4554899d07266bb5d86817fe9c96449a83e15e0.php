<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Publications</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    <a href="<?php echo e(url('/dashboard/publication')); ?>" class="btn btn-new btn_new_active">Manage publications</a>
                                    <a href="<?php echo e(url('/dashboard/publication/book/create')); ?>" class="btn btn-new">Add Book</a>
                                    <a href="<?php echo e(url('/dashboard/publication/conference/create')); ?>" class="btn btn-new">Add Conference Paper</a>
                                    <a href="<?php echo e(url('/dashboard/publication/journal/create')); ?>" class="btn btn-new">Add Journal</a>
                                </p>


                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Type</th>
                                                <th>Attachment</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($publications)): ?>
                                            <?php $__currentLoopData = $publications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $publication): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($publication->title); ?></td>
                                                <td><?php echo e(\App\Publication::$types[$publication->type_of_publication]); ?></td>
                                                <td>
                                                    <?php if(isset($publication->file_url)): ?>
                                                        <a  target="_blank" href="<?php echo e(url('/assets/publications/'. $publication->file_url)); ?>">Download</a>
                                                    <?php endif; ?>

                                                </td>
                                                <td>
                                                    <a class="btn btn-xs btn-primary" href="<?php echo e(url('dashboard/publication/show/'. $publication->id)); ?>" target="_blank"><i class="fa fa-eye"></i></a>

                                                    <a href="<?php echo e(url('/dashboard/publication/'.$publication->id.'/edit')); ?>" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>

                                                    <a onclick="return confirm('Are you sure ?')" href="<?php echo e(url('/dashboard/publication/'.$publication->id.'/delete')); ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                                                </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>