
<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3  class="head_title">Homepage settings</h3>

<?php echo Form::open(['url' => action('Backend\SettingController@update'), 'method' => 'POST', 'id' => 'homepageUpdate','class' => 'row', 'role'=>'form', 'enctype'=>'multipart/form-data']); ?>


    <?php echo e(csrf_field()); ?>


    <div class="section">
        <h4 style="padding-left: 15px;">Header Text</h4>

        <div class="form-group col-sm-12">
            <?php echo Form::label('homepage_header_title_1', 'Header Title 1', array('class' => 'control-label')); ?>

            <?php echo Form::text('homepage_header_title_1',$datas->homepage_header_title_1,['class' => 'form-control']);; ?>

            <?php if($errors->has('homepage_header_title_1')): ?>
              <p class="help-block error_login">
                  <strong><?php echo e($errors->first('homepage_header_title_1')); ?></strong>
              </p>
            <?php endif; ?>
        </div>

        <div class="form-group col-sm-12">
            <?php echo Form::label('custom_field1', 'Header Title 2', array('class' => 'control-label')); ?>

            <?php echo Form::text('custom_field1',$datas->custom_field1,['class' => 'form-control']);; ?>

            <?php if($errors->has('custom_field1')): ?>
              <p class="help-block error_login">
                  <strong><?php echo e($errors->first('custom_field1')); ?></strong>
              </p>
            <?php endif; ?>
        </div>

    </div>

    <div class="section">
        <h4 style="padding-left: 15px;">About Us Section</h4>


        <div class="form-group col-sm-12">
            <?php echo Form::label('homepage_about_title_1', 'About Section Title 1', array('class' => 'control-label')); ?>

            <?php echo Form::text('homepage_about_title_1',$datas->custom_field1,['class' => 'form-control']);; ?>

            <?php if($errors->has('homepage_about_title_1')): ?>
              <p class="help-block error_login">
                  <strong><?php echo e($errors->first('homepage_about_title_1')); ?></strong>
              </p>
            <?php endif; ?>
        </div>

        <div class="form-group col-sm-12">
            <?php echo Form::label('homepage_about_title_2', 'About Section Title 2', array('class' => 'control-label')); ?>

            <?php echo Form::text('homepage_about_title_2',$datas->homepage_about_title_2,['class' => 'form-control']);; ?>

            <?php if($errors->has('homepage_about_title_2')): ?>
              <p class="help-block error_login">
                  <strong><?php echo e($errors->first('homepage_about_title_2')); ?></strong>
              </p>
            <?php endif; ?>
        </div>

        <div class="form-group col-sm-12">
            <?php echo Form::label('homepage_about_paragraph', 'Paragraph', array('class' => 'control-label')); ?>

            <?php echo Form::textarea('homepage_about_paragraph',$datas->homepage_about_paragraph,['class' => 'form-control textArea']);; ?>

            <?php if($errors->has('homepage_about_paragraph')): ?>
              <p class="help-block error_login">
                  <strong><?php echo e($errors->first('homepage_about_paragraph')); ?></strong>
              </p>
            <?php endif; ?>
        </div>

        <div class="form-group col-sm-12">
            <?php echo Form::label('custom_field2', 'Custom field', array('class' => 'control-label')); ?>

            <?php echo Form::text('custom_field2',$datas->custom_field2,['class' => 'form-control']);; ?>

            <?php if($errors->has('custom_field2')): ?>
              <p class="help-block error_login">
                  <strong><?php echo e($errors->first('custom_field2')); ?></strong>
              </p>
            <?php endif; ?>
        </div>

        <div class="form-group col-sm-12">
            <?php echo Form::label('custom_field3', 'Another Custom field', array('class' => 'control-label')); ?>

            <?php echo Form::text('custom_field3',$datas->custom_field3,['class' => 'form-control']);; ?>

            <?php if($errors->has('custom_field3')): ?>
              <p class="help-block error_login">
                  <strong><?php echo e($errors->first('custom_field3')); ?></strong>
              </p>
            <?php endif; ?>
        </div>

    </div>
    <p class="text-right col-sm-12">
        <button type="submit" class="btn btn-new">Update Page</button>
    </p>

<?php echo Form::close(); ?>



                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>