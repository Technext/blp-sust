
<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">Resources : <?php echo e($typeName); ?></h2>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-12">

            <div style="padding: 15px;" class="hidden-xs hidden-sm"></div>
            <div class="row box-dsgn-thead">
                <div class="col-md-5">
                    <p class="">Title</p>
                </div>
                <div class="col-md-3">
                    <p class="">Type</p>
                </div>
                <div class="col-md-3">
                    <p class="">Publication date</p>
                </div>
                <div class="col-md-1">
                    <p class="">Action</p>
                </div>
            </div>
            <hr class="row">

            <div class="row publication">
                <div class="col-md-12">

                    <div class="row publication">
                        <div class="col-md-12">
                        <?php if(isset($resources) && count($resources) > 0): ?>
                            <?php $__currentLoopData = $resources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resource): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="row box-dsgn">
                                    <div class="col-md-5">
                                        <h5>
                                            <a class="publication_title" href="<?php echo e(url('/resource/types/'.$resource->id)); ?>"><?php echo e($resource->title); ?></a>
                                        </h5>
                                    </div>
                                    <div class="col-md-3">
                                        <p class="publication_author"><?php echo e($resource->type->name); ?></p>
                                    </div>
                                    <div class="col-md-3">
                                        <p class="text-left"><?php echo e(\Carbon\Carbon::parse($resource->updated_at)->format('M-d-Y')); ?></p>
                                    </div>
                                    <div class="col-md-1">
                                        <a class="publication_view" href="<?php echo e(url('/resource/types/'.$resource->id)); ?>">View</a>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <h5 class="text-center">Sorry, there is no resource available right now!!</h5>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
            </div>

            </div>
        </div>

    </div>
    <div class="m-b-120"></div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>