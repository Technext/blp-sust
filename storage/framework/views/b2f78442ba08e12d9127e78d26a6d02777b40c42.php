<?php $__env->startSection('mainContent'); ?>
<style type="text/css">
  .img-box {
    width: 120px;
    height: 120px;
  }
</style>
    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="members create">
                        <h3 class="head_title">Edit Member Profile</h3>
                        <div class="row">
                            <div class="col-md-12">
                                
                                <?php echo Form::open(['url' => action('DashboardController@update'), 'method' => 'POST', 'id' => 'profileEdit','class' => '', 'role'=>'form', 'enctype'=>'multipart/form-data']); ?>


                                    <?php echo e(csrf_field()); ?>


                                    <div class="form-group  col-sm-6 col-md-offset-3">
                                        <?php if(Auth::user()->image): ?>
                                            <div class="img-box">
                                              <img src="<?php echo e(url('assets/profile/'.Auth::user()->image)); ?>" alt="Profile Pic" class="profile-pic-img">
                                            </div>
                                            
                                        <?php else: ?>
                                            <div class="img-box">
                                              <img class="profile-pic-img" src="<?php echo e(url('images/profile-placeholder.png')); ?>" alt="Profile Picture">
                                            </div>
                                        <?php endif; ?>

                                        <input type="file" name="image" class="form-control">
                                        <p class="text-center" style="font-size: 14px;margin-top: 7px;"><span class="text-danger">Picture must be (200px x 200px)</span></p>

                                    </div>

                                    <div class="form-group col-sm-12">
                                      <?php echo Form::label('name', 'Name *', array('class' => 'control-label')); ?>

                                      <?php echo Form::text('name', Auth::user()->name, ['class' => 'form-control','required' => 'required']);; ?>

                                      <?php if($errors->has('name')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('name')); ?></strong>
                                          </p>
                                      <?php endif; ?>
                                    </div>

                                    <div class="form-group col-sm-12">
                                      <?php echo Form::label('email', 'Email *', array('class' => 'control-label')); ?>

                                      <?php echo Form::email('email', Auth::user()->email, ['class' => 'form-control','required' => 'required']);; ?>

                                      <?php if($errors->has('email')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('email')); ?></strong>
                                          </p>
                                      <?php endif; ?>
                                    </div>

                                    <div class="form-group col-sm-12">
                                      <?php echo Form::label('phone_no', 'Phone Number', array('class' => 'control-label')); ?>

                                      <?php echo Form::text('phone_no', Auth::user()->phone_no, ['class' => 'form-control']);; ?>

                                      <?php if($errors->has('phone_no')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('phone_no')); ?></strong>
                                          </p>
                                      <?php endif; ?>
                                    </div>

                                    <div class="form-group col-sm-6">
                                      <?php echo Form::label('designation', 'Designation', array('class' => 'control-label')); ?>

                                      <?php echo e(Form::select('designation', $designations, Auth::user()->designation, ['id' => 'designation','class' => 'form-control selectChosen'])); ?>

                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label for="supervisor">Supervisor</label>
                                        <select name="supervisor" id="supervisor" class="form-control selectChosen">
                                            <option value="">Select supervisor</option>
                                            <?php $__currentLoopData = $faculties; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $faculty): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($faculty->id); ?>" <?php echo e((Auth::user()->supervisor == $faculty->id) ? 'selected' : ''); ?>><?php echo e($faculty->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-sm-12">
                                      <?php echo Form::label('dept_name', 'Department', array('class' => 'control-label')); ?>

                                      <?php echo Form::text('dept_name', Auth::user()->dept_name, ['class' => 'form-control']);; ?>

                                      <?php if($errors->has('dept_name')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('dept_name')); ?></strong>
                                          </p>
                                      <?php endif; ?>
                                    </div>

                                    <div class="form-group col-sm-12">
                                      <?php echo Form::label('institute', 'Institute', array('class' => 'control-label')); ?>

                                      <?php echo Form::text('institute', Auth::user()->institute_name, ['class' => 'form-control']);; ?>

                                      <?php if($errors->has('institute')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('institute')); ?></strong>
                                          </p>
                                      <?php endif; ?>
                                    </div>

                                    <div class="form-group col-sm-12">
                                      <?php echo Form::label('research_fields', 'Research Fields', array('class' => 'control-label')); ?>

                                      <?php echo e(Form::select('research_fields[]', $research_fields, Auth::user()->researchFields, ['id' => 'research_fields','class' => 'form-control selectChosen','multiple' => 'multiple'])); ?>

                                    </div>

                                    <div class="form-group col-sm-12">
                                      <?php echo Form::label('bio', 'Bio', array('class' => 'control-label')); ?>

                                      <?php echo Form::textarea('bio', Auth::user()->bio, ['class' => 'form-control textArea','placeholder'=>'Enter Something About Member...','cols'=>15]);; ?>

                                      <?php if($errors->has('bio')): ?>
                                          <p class="help-block error_login">
                                              <strong><?php echo e($errors->first('bio')); ?></strong>
                                          </p>
                                      <?php endif; ?>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="form-group  col-sm-12 text-right">
                                        <button type="submit" class="btn btn-new">Update</button>
                                    </div>

                                <?php echo Form::close(); ?>


                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>