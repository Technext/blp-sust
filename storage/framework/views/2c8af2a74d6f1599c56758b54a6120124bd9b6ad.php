<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Peoples</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="<?php echo e(url('/dashboard/peoples')); ?>" class="btn btn-new btn_new_active">Manage People</a>
                                    <a href="<?php echo e(url('/dashboard/peoples/create')); ?>" class="btn btn-new">Add New People</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Designation</th>
                                                <th>Section</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($people)): ?>
                                            <?php $__currentLoopData = $people; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $person): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr id="<?php echo e($person->id); ?>">
                                                <td><?php echo e($person->name); ?></td>
                                                <td><?php echo e($person->designationDetails ? $person->designationDetails->name : ''); ?></td>
                                                <td><?php echo e($person->type == 'academic' ? 'Academic' : 'Administrative'); ?></td>
                                                <td>

                                                    <a class="btn btn-xs btn-primary"  href="<?php echo e(url('/dashboard/peoples/show/'. $person->id)); ?>" class="btn" target="_blank"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo e(url('/dashboard/peoples/' . $person->id . '/edit')); ?>"class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a onclick="return confirm('Are you sure ?')"  href="<?php echo e(url('/dashboard/peoples/' . $person->id . '/delete')); ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                                                </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>