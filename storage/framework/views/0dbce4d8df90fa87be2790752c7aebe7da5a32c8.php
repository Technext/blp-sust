
<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Resources</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="<?php echo e(url('/dashboard/resources')); ?>" class="btn btn-new btn_new_active">Manage Resource</a>
                                    <a href="<?php echo e(url('/dashboard/resources/create')); ?>" class="btn btn-new">Add New Resource</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Keywords</th>
                                                <th>Type</th>
                                                <th>Version</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($resources)): ?>
                                            <?php $__currentLoopData = $resources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resource): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($resource->title); ?></td>
                                                <td><?php echo e((isset($resource->keywords) ? $resource->keywords : '')); ?></td>
                                                <td><?php echo e((isset($resource->resourceType) ? $resource->resourceType->name : '')); ?></td>
                                                <td><?php echo e((isset($resource->version) ? $resource->version : '')); ?></td>
                                                <td>

                                                    <a class="btn btn-xs btn-primary"  href="<?php echo e(url('/dashboard/resources/show/'. $resource->id)); ?>" class="btn" target="_blank"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo e(url('/dashboard/resources/' . $resource->id . '/edit')); ?>"class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a onclick="return confirm('Are you sure ?')"  href="<?php echo e(url('/dashboard/resources/' . $resource->id . '/delete')); ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                                                </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>