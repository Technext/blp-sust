
<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">View member</h2>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
    <div class="faculty-member">
        <div class="row">
            <div class="col-md-3">
            <?php if(isset($faculty->image)): ?>
                <img src="<?php echo e(url('/assets/profile/' . $faculty->image)); ?>" alt="Member Profile Image">
            <?php else: ?>
                <img src="<?php echo e(url('/images/profile-placeholder.jpg')); ?>" alt="Member Profile Image">
            <?php endif; ?>
            </div>
            <div class="col-md-9">
                <h4 style="margin: 0;"><?php echo e($faculty->name); ?></h4>
                <h5><?php echo e($faculty->designationDetails->name); ?></h5>

                <p class="designation_p_2"><?php echo e($faculty->dept_name ? $faculty->dept_name : 'Department Of Computer Science and Engineering'); ?></p>
                <p class="designation_p_2"><?php echo e($faculty->institute_name ? $faculty->institute_name : 'Shahjalal University of Science and Technology, Sylhet'); ?></p>

                <p class="designation_p_2"><span class="bold">Email:</span> <?php echo e($faculty->email); ?></p>
                <p class="designation_p_2"><span class="bold">Phone No:</span> <?php echo e($faculty->phone_no); ?></p>
            </div>
            <div class="clear-fix"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <hr>
                <p class="bio"><strong>BIO : </strong> <?php echo $faculty->bio!="" ? $faculty->bio : 'N/A'; ?></p>
            </div>
        </div>
    </div>
    </div>

    <div class="m-b-120"></div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>