
<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">View member</h2>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="faculty-member">
            <div class="row">
                <div class="col-md-3">
                    <?php if(isset($member->image)): ?>
                        <img src="<?php echo e(url('/assets/profile/' . $member->image)); ?>" alt="Member Profile Image">
                    <?php else: ?>
                        <img src="<?php echo e(url('/images/profile-placeholder.jpg')); ?>" alt="Member Profile Image">
                    <?php endif; ?>
                </div>
                <div class="col-md-5">
                    <h4 style="margin: 0;"><?php echo e($member->name); ?></h4>
                    <h5><?php echo e($member->designationDetails->name); ?></h5>
                    <p class="designation_p_2"><?php echo e($member->dept_name); ?></p>
                    <p class="designation_p_2"><?php echo e($member->institute_name); ?></p>
                    <p class="designation_p_2"><span class="bold">Email:</span> <?php echo e($member->email); ?></p>
                    <p class="designation_p_2"><span class="bold">Phone No:</span> <?php echo e($member->phone_no); ?></p>
                </div>
                <div class="col-md-4">
                    <p class="designation_p_2"><span class="bold">Research Fields:</span> <?php echo e($member->researchFields->implode('name', ', ')); ?></p>
                    <?php if(isset($member->supervisorDetails)): ?>
                        <p class="designation_p_2"><span class="bold">Supervisor:</span> <?php echo e($member->supervisorDetails->name); ?></p>
                    <?php endif; ?>
                    <p class="bio designation_p_2"><strong>BIO : </strong> <?php echo $member->bio!="" ? $member->bio : 'N/A'; ?></p>
                </div>
                <div class="clear-fix"></div>
            </div>

            <?php if(isset($member->publications) && count($member->publications)): ?>
            <br>
            <br>
            <div class="publication">
                <div class="col-md-12">
                    
                    <div class="row">
                        <h4>Publications :</h4>
                    </div>
                    <div class="row box-dsgn-thead">
                        <div class="col-md-4">
                            <p class="">Title</p>
                        </div>
                        <div class="col-md-3">
                            <p class="">Author</p>
                        </div>
                        <div class="col-md-2">
                            <p class="">File</p>
                        </div>
                        <div class="col-md-2">
                            <p class="">Publication date</p>
                        </div>
                        <div class="col-md-1">
                            <p class="">Action</p>
                        </div>
                    </div>
                    <hr class="row">
                    <?php $__currentLoopData = $member->publications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $publication): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="row box-dsgn">
                            <div class="col-md-4">
                                <h5>
                                    <a class="publication_title" href="<?php echo e(url('/'.$publication->type_of_publication.'/'.$publication->id)); ?>"><?php echo e($publication->title); ?></a>
                                </h5>
                            </div>
                            <div class="col-md-3">
                                <p class="publication_author"><?php echo e($publication->author); ?></p>
                            </div>
                            <div class="col-md-2">
                                <p>
                                    <?php if(isset($publication->file_url)): ?>
                                        <a  class="publication_image" href="<?php echo e(url('assets/publications/'.$publication->file_url)); ?>" target="_blank">Download</a>
                                    <?php else: ?>
                                        <p>N/A</p>
                                    <?php endif; ?>
                                </p>
                            </div>
                            <div class="col-md-2">
                                <p class="text-left"><?php echo e(\Carbon\Carbon::parse($publication->published_at)->format('d M, Y')); ?></p>
                            </div>
                            <div class="col-md-1">
                                <a class="publication_view" href="<?php echo e(url('/'.$publication->type_of_publication.'/'.$publication->id)); ?>">View</a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="m-b-120"></div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>