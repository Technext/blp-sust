<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <h3 class="head_title">Edit publication</h3>

                    <p>
                        <a href="<?php echo e(url('/dashboard/publication')); ?>" class="btn btn-new">Manage publications</a>
                        <a href="<?php echo e(url('/dashboard/publication/book/create')); ?>" class="btn btn-new <?php echo e((Request::segment(3)=='book' ? 'btn_new_active' : '')); ?>">Add Book</a>
                        <a href="<?php echo e(url('/dashboard/publication/conference/create')); ?>" class="btn btn-new <?php echo e((Request::segment(3)=='conference' ? 'btn_new_active' : '')); ?>">Add Conference Paper</a>
                        <a href="<?php echo e(url('/dashboard/publication/journal/create')); ?>" class="btn btn-new <?php echo e((Request::segment(3)=='journal' ? 'btn_new_active' : '')); ?>">Add Journal</a>
                    </p>


<?php
// echo '<pre>';
// print_r($datas);
// exit;

?>

<?php echo Form::open(['url' => action('Backend\PublicationController@update'), 'method' => 'POST', 'id' => 'updateForm','class' => 'row', 'role'=>'form', 'enctype'=>'multipart/form-data']); ?>


<?php echo e(csrf_field()); ?>



<!-- Common Stsrt -->

<input hidden="hidden" type="hidden" name="type_of_publication" value="<?php echo e($datas['type_of_publication']); ?>">
<input hidden="hidden" type="hidden" name="publication_id" value="<?php echo e($datas['id']); ?>">

<div class="form-group col-sm-12">
  <?php echo Form::label('title', 'Title *', array('class' => 'control-label')); ?>

  <?php echo Form::text('title',$datas['title'],['placeholder' => 'title','class' => 'form-control','required' => 'required']);; ?>

  <?php if($errors->has('title')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('title')); ?></strong>
      </p>
  <?php endif; ?>
</div>

<?php if($datas['type_of_publication']=="books"): ?>
<div class="form-group col-sm-12">
  <?php echo Form::label('booktitle', 'Book Title', array('class' => 'control-label')); ?>

  <?php echo Form::text('booktitle',$datas['book_title'],['placeholder' => 'Book Title','class' => 'form-control','required' => 'required']);; ?>

  <?php if($errors->has('booktitle')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('booktitle')); ?></strong>
      </p>
  <?php endif; ?>
</div>
<?php endif; ?>


<div class="form-group col-sm-12">
  <?php echo Form::label('notes', 'Notes', array('class' => 'control-label')); ?>

  <?php echo Form::textarea('notes',$datas['note'],[ 'cols'=>'30', 'rows'=>'10','class' => 'form-control textArea']);; ?>

  <!--<textarea name="notes" cols="30" rows="10" class="form-control textArea"><?php echo e($datas['note']); ?></textarea>-->
  <?php if($errors->has('notes')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('notes')); ?></strong>
      </p>
  <?php endif; ?>
</div>


<div class="form-group col-sm-12">
    <input type="checkbox" name="is_public" id="show-publicly" <?php echo e($datas['is_public']==1 ? 'checked' : ''); ?>>
    <label for="show-publicly">Show Publicly?</label>
</div>

<div class="form-group col-sm-12">
    <input type="checkbox" name="is_profile_only" id="show-profile-only" <?php echo e($datas['is_profile_only']==1 ? 'checked' : ''); ?>>
    <label for="show-profile-only">Show Profile Only?</label>
</div>


<div class="form-group col-sm-12">
  <?php echo Form::label('author', 'Author', array('class' => 'control-label')); ?>

  <?php echo Form::text('author',$datas['author'],['placeholder' => 'Author','class' => 'form-control','required' => 'required']);; ?>

  <?php if($errors->has('author')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('author')); ?></strong>
      </p>
  <?php endif; ?>
</div>


<div class="form-group col-sm-12">
  <?php echo Form::label('year', 'Year', array('class' => 'control-label')); ?>

  <?php echo Form::text('year',date('Y-m-d',strtotime($datas['published_at'])),['placeholder' => 'Year Published At','class' => 'form-control']);; ?>

  <?php if($errors->has('year')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('year')); ?></strong>
      </p>
  <?php endif; ?>
</div>


<div class="form-group col-sm-12">
  <?php echo Form::label('address', 'Address', array('class' => 'control-label')); ?>

  <?php echo Form::text('address',$datas['address'],['placeholder' => 'Address','class' => 'form-control']);; ?>

  <?php if($errors->has('address')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('address')); ?></strong>
      </p>
  <?php endif; ?>
</div>

<!-- Common End -->


<?php if(($datas['type_of_publication']=="books") OR ($datas['type_of_publication']=="journal")): ?>

<div class="form-group col-sm-12">
  <?php echo Form::label('editor', 'Editor', array('class' => 'control-label')); ?>

  <?php echo Form::text('editor',$datas['editor'],['placeholder' => 'Editor','class' => 'form-control']);; ?>

  <?php if($errors->has('editor')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('editor')); ?></strong>
      </p>
  <?php endif; ?>
</div>

<div class="form-group col-sm-12">
  <?php echo Form::label('publisher', 'Publisher', array('class' => 'control-label')); ?>

  <?php echo Form::text('publisher',$datas['publisher'],['placeholder' => 'Publisher','class' => 'form-control']);; ?>

  <?php if($errors->has('publisher')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('publisher')); ?></strong>
      </p>
  <?php endif; ?>
</div>


<div class="form-group col-sm-12">
  <?php echo Form::label('pages', 'Pages', array('class' => 'control-label')); ?>

  <?php echo Form::text('pages',$datas['pages'],['placeholder' => 'Pages','class' => 'form-control']);; ?>

  <?php if($errors->has('pages')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('pages')); ?></strong>
      </p>
  <?php endif; ?>
</div>

<?php endif; ?>


<?php if($datas['type_of_publication']=="books"): ?>

<div class="form-group col-sm-12">
  <?php echo Form::label('series', 'Series', array('class' => 'control-label')); ?>

  <?php echo Form::text('series',$datas['series'],['placeholder' => 'Series','class' => 'form-control']);; ?>

  <?php if($errors->has('series')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('series')); ?></strong>
      </p>
  <?php endif; ?>
</div>

<?php endif; ?>


<?php if($datas['type_of_publication']=="journal"): ?>


<div class="form-group col-sm-12">
  <?php echo Form::label('abstract', 'Abstract', array('class' => 'control-label')); ?>

  <?php echo Form::textarea('abstract',$datas['abstract'],[ 'cols'=>'30', 'rows'=>'10','class' => 'form-control textArea']);; ?>

  <?php if($errors->has('abstract')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('abstract')); ?></strong>
      </p>
  <?php endif; ?>
</div>



<div class="form-group col-sm-12">
  <?php echo Form::label('journal', 'Journal', array('class' => 'control-label')); ?>

  <?php echo Form::text('journal',$datas['journal'],['placeholder' => 'Journal','class' => 'form-control']);; ?>

  <?php if($errors->has('journal')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('journal')); ?></strong>
      </p>
  <?php endif; ?>
</div>

<div class="form-group col-sm-12">
  <?php echo Form::label('volume', 'Volume', array('class' => 'control-label')); ?>

  <?php echo Form::text('volume',$datas['volume'],['placeholder' => 'Volume','class' => 'form-control']);; ?>

  <?php if($errors->has('volume')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('volume')); ?></strong>
      </p>
  <?php endif; ?>
</div>

<?php endif; ?>



<?php if(($datas['type_of_publication']=="conference") OR ($datas['type_of_publication']=="journal")): ?>

<div class="form-group col-sm-12">
  <?php echo Form::label('institution', 'Institution', array('class' => 'control-label')); ?>

  <?php echo Form::text('institution',$datas['institution'],['placeholder' => 'Institution name','class' => 'form-control']);; ?>

  <?php if($errors->has('institution')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('institution')); ?></strong>
      </p>
  <?php endif; ?>
</div>

<?php endif; ?>


<div class="form-group col-sm-12">
  <?php echo Form::label('reference_url', 'Reference', array('class' => 'control-label')); ?>

  <?php echo Form::url('reference_url',$datas['reference_url'],['placeholder' => 'Reference Url','class' => 'form-control']);; ?>

  <?php if($errors->has('reference_url')): ?>
      <p class="help-block error_login">
          <strong><?php echo e($errors->first('reference_url')); ?></strong>
      </p>
  <?php endif; ?>
</div>


<div class="form-group col-sm-12">
    <label for="attachment">Attachment </label>
    <input type="file" class="form-control" name="attachment" placeholder="Attachment File" id="attachment">
    <?php if($datas['file_url']): ?>
    <p><strong><a href="<?php echo e(asset('assets/publications/'.$datas['file_url'])); ?>" target="_blank">Download</a></strong></p>
    <?php endif; ?>

    
</div>

<p class="text-right col-sm-12">
    <button type="submit" class="btn btn-new">Update</button>
</p>

<?php echo Form::close(); ?>

                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/ui/trumbowyg.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.21.0/trumbowyg.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('.textArea').trumbowyg();

    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>