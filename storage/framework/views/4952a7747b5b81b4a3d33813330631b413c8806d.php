                <div class="col-md-3 sidebar">
                    <ul>
                        <li>
                            <a href="<?php echo e(url('profile')); ?>" class="<?php echo e(Request::segment(1)=='profile' ? 'active' : ''); ?>">Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('dashboard/publication')); ?>" class="<?php echo e(Request::segment(2)=='publication' ? 'active' : ''); ?>">Publications</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('dashboard/resources')); ?>" class="<?php echo e(Request::segment(2)=='resources' ? 'active' : ''); ?>">Resources</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('dashboard/resource_types')); ?>" class="<?php echo e(Request::segment(2)=='resource_types' ? 'active' : ''); ?>">Resources Types</a>
                        </li>
                        <?php if(Auth::user()->is_admin): ?>
                        <li>
                            <a href="<?php echo e(url('dashboard/setting/homepage')); ?>" class="<?php echo e(Request::segment(3)=='homepage' ? 'active' : ''); ?>">Homepage Settings</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('dashboard/setting/footer')); ?>" class="<?php echo e(Request::segment(3)=='footer' ? 'active' : ''); ?>">Footer Settings</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('dashboard/lab')); ?>" class="<?php echo e(Request::segment(2)=='lab' ? 'active' : ''); ?>">Lab Facilities</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('dashboard/members')); ?>" class="<?php echo e(Request::segment(2)=='members' ? 'active' : ''); ?>">Members</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('dashboard/peoples')); ?>" class="<?php echo e(Request::segment(2)=='peoples' ? 'active' : ''); ?>">People</a>
                        </li>
                        <!-- <li>
                            <a href="<?php echo e(url('dashboard/designations')); ?>" class="<?php echo e(Request::segment(2)=='designations' ? 'active' : ''); ?>">Designations</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('dashboard/research_fields')); ?>" class="<?php echo e(Request::segment(2)=='research_fields' ? 'active' : ''); ?>">Research Fields</a>
                        </li> -->
                        <?php endif; ?>

                    </ul>
                </div>