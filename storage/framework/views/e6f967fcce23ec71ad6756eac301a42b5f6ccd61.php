<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title"><?php echo e($pubType); ?> Publications</h2>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-12">
            <div style="padding: 15px;" class="hidden-xs hidden-sm"></div>
            <div class="row box-dsgn-thead">
                <div class="col-md-4">
                    <p class="">Title</p>
                </div>
                <div class="col-md-3">
                    <p class="">Author</p>
                </div>
                <div class="col-md-2">
                    <p class="">File</p>
                </div>
                <div class="col-md-2">
                    <p class="">Publication date</p>
                </div>
                <div class="col-md-1">
                    <p class="">Action</p>
                </div>
            </div>
            <hr class="row">
            <?php if(isset($publication)): ?>
            <div class="row publication">
                <div class="col-md-12">
                    <?php $__currentLoopData = $publication; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $singlePublication): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="row box-dsgn">
                            <div class="col-md-4">
                                <h5>
                                    <a class="publication_title" href="<?php echo e(url('/'.$singlePublication->type_of_publication.'/'.$singlePublication->id)); ?>"><?php echo e($singlePublication->title); ?></a>
                                </h5>
                            </div>
                            <div class="col-md-3">
                                <p class="publication_author"><?php echo e($singlePublication->author); ?></p>
                            </div>
                            <div class="col-md-2">
                                <p>
                                    <?php if(isset($singlePublication->file_url)): ?>
                                        <a  class="publication_image" href="<?php echo e(url('assets/publications/'.$singlePublication->file_url)); ?>" target="_blank">Download</a>
                                    <?php else: ?>
                                        <p>N/A</p>
                                    <?php endif; ?>
                                </p>
                            </div>
                            <div class="col-md-2">
                                <p class="text-left"><?php echo e(\Carbon\Carbon::parse($singlePublication->published_at)->format('d M, Y')); ?></p>
                            </div>
                            <div class="col-md-1">
                                <a class="publication_view" href="<?php echo e(url('/'.$singlePublication->type_of_publication.'/'.$singlePublication->id)); ?>">View</a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <?php endif; ?>

            </div>
        </div>

    </div>
    <div class="m-b-120"></div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>