<?php $__env->startSection('css'); ?>

    <style type="text/css">

        /*Only for index page*/

        @media  only screen and (max-width: 760px) {
          header{min-height: 70vh;}
        }

        @media  only screen and (max-width: 480px) {
          header{min-height: 50vh !important;}
        }

        .ui-autocomplete-input, .ui-menu, .ui-menu-item {  z-index: 2006; }

    </style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>


        <div class="header-slider-wrapper">
            <div class="header-slider owl-carousel">
                <div class="item" style="background-image: url('assets/homepage_slider/9fa1bbd8ff88d1164f3577603533ca1c.jpg'); background-size: cover;">
                <div class="overlay"></div>
                </div>
                <div class="item" style="background-image: url('assets/homepage_slider/bca0cf5546b8c3649019f6abdde43026.jpg'); background-size: cover;">
                <div class="overlay"></div>
                </div>
                <div class="item" style="background-image: url('assets/homepage_slider/6a0bb3821d76a478639b6efcc2820c11.jpg'); background-size: cover;">
                <div class="overlay"></div>
                </div>
            </div>
        </div>

        <?php if(isset($themeDatas->homepage_header_title_1) || isset($themeDatas->custom_field1) || isset($themeDatas->custom_field12)): ?>
        <div class="inner-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <?php if($themeDatas->homepage_header_title_1): ?>
                        <h1 class="font-weight-regular header-title-1"><?php echo e($themeDatas->homepage_header_title_1); ?></h1>
                        <?php endif; ?>

                        <?php if($themeDatas->custom_field1): ?>
                        <h1><?php echo e($themeDatas->custom_field1); ?></h1>
                        <?php endif; ?>

                        <?php if($themeDatas->custom_field2): ?>
                        <p class="text-center"><?php echo $themeDatas->custom_field2; ?></p>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>


    </div>

    <div class="main-wrapper"> <!-- main wrapper -->
        <div class="content-wrapper">

            <div class="page-content" id="content">

                <?php if(isset($themeDatas->homepage_about_title_1) || isset($themeDatas->homepage_about_title_2) || isset($themeDatas->homepage_about_paragraph)): ?>
                    <div class="container">
                        <div class="row section">
                            <div class="col-md-12">
                                <h5 class="font-weight-regular title_read_p"><?php echo e($themeDatas->homepage_about_title_1); ?></h5>
                                <h4 class="title_read_p_main"><?php echo e($themeDatas->homepage_about_title_2); ?></h4>
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="h5 read_p">
                                            <?php echo $themeDatas->homepage_about_paragraph; ?>

                                        </p> <a class="read_more" href="<?php echo e(url('/research')); ?>">Read more</a>
                                    </div>
                                </div>
                                    
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="page-content" id="benifit-section">
                <div class="container section">
                    <div class="row section research-areas">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <img src="<?php echo e(asset('images/watson-text-to-speech-card.png')); ?>" alt="Image">
                                        <h4 class="title_read_p_main2">Research areas</h4>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                        <div class="research-field-list">
                                            <?php $__currentLoopData = $researchFields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $researchField): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                
                                                <h6 class="font-weight-regular"><?php echo e($researchField->name); ?></h6>
                                                
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <img src="<?php echo e(asset('images/objectives-icon.png')); ?>" alt="Image">
                                        <h4 class="title_read_p_main2">Objectives</h4>
                                    </div>
                                    <div class="col-md-7 col-xs-7 col-sm-7">
                                        <div class="objectives-list">
                                            <p class="font-weight-regular">Building multi-platform applications(mobile apps, desktop application and cloud services).</p>
                                            <p class="font-weight-regular">Sales and marketing of the products.</p>
                                            <p class="font-weight-regular">Arrange conference, workshop, training for dissemination of experience.</p>
                                            <p class="font-weight-regular">Developing Knowledge archive</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-content">

                <?php if(isset($labFacilities) && count($labFacilities) > 0): ?>
                <div class="container section">
                    <div class="row section">
                        <div class="col-md-12">
                            <h4 class="text-center title_read_p_main">Lab Facilities</h4>
                            <div class="row facilities-wrapper">
                                <?php $is_first = true; ?>
                                <?php $__currentLoopData = $labFacilities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $facility): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($is_first): ?>
                                        <div class="col-md-8">
                                            <img src="<?php echo e(url('/assets/lab_facilities/' . $facility->image)); ?>" alt="Image">
                                        </div>
                                        <?php $is_first = false; ?>
                                    <?php else: ?>
                                        <div class="col-md-4">
                                            <img src="<?php echo e(url('/assets/lab_facilities/' . $facility->image)); ?>" alt="Image">
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>

            <div class="page-content">

                <div class="container section">
                    <div class="row section partners">
                        <div class="col-md-12">
                            <div class="col-md-6 text-center">
                                <h4 class="title_read_p_main">Industry Partners</h4>
                                <a href="https://www.eatlbd.com/" target="_blank"><img src="images/eatl.png" alt="EATL Logo"></a>
                            </div>
                            <div class="col-md-6 text-center">
                                <h4 class="text-center title_read_p_main">Implementing Partner</h4>
                                <a href="http://www.sust.edu/departments/cse"><h5 class="font-weight-regular">Department of Computer Science and Engineering</h5></a>
                                <a href="http://www.sust.edu/"><h5 class="font-weight-regular">Shahjalal University of Science and Technology</h5></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div> <!-- end main wrapper -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>