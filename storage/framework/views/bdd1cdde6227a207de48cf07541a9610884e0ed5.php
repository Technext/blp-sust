
<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>

    <div class="m-b-120"></div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="front_title">Resource type details</h2>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-12">
            <div class="row publication">
                <div class="col-md-12">

                    <div class="journal">

                        <div class="row journal_details">

                            <div class="col-md-12">
                                <h4><?php echo e($resource->title); ?></h4>
                                
                                <p class="pub_p"><span class="pub_span"><?php echo e(\Carbon\Carbon::parse($resource->updated_at)->format('d M, Y')); ?></span></p>
                                
                                <br>

                                <?php if(isset($resource->version)): ?>
                                    <p class="pub_p"><span class="pub_span">Version : </span><?php echo e($resource->version); ?></p>
                                <?php endif; ?>

                                <?php if(isset($resource->keywords)): ?>
                                    <p class="pub_p"><span class="pub_span">Keywords : </span><?php echo e($resource->keywords); ?></p>
                                <?php endif; ?>
                                
                                <?php if(isset($resource->description)): ?>
                                    <p class="pub_p"><span class="pub_span">Description : </span><?php echo $resource->description; ?></p>
                                <?php endif; ?>

                                <?php if(isset($resource->reference_url)): ?>
                                    <p class="pub_p"><span class="pub_span">Reference : </span><?php echo e($resource->reference_url); ?></p>
                                <?php endif; ?>

                                <?php if(isset($resource->files) && count($resource->files) > 0): ?>
                                <div class="attachments">
                                <p class="pub_p pub_span">Attachments :</p>
                                <div class="attachment-list">
                                <div class="row">
                                <?php $__currentLoopData = $resource->files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-md-3 attachment">
                                        <h5><?php echo e($file->file_name); ?></h5>
                                        <a target="_blank" href="<?php echo e(url('/assets/resources/'.$file->file_name)); ?>" class="btn btn-download btn-sm">Download <i class="fa fa-download" aria-hidden="true"></i></a>
                                    </div>
                                            
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                </div>
                                </div>
                                <?php endif; ?>

                            </div>



                        </div>



                    </div>


                </div>
            </div>
            </div>
        </div>

    </div>
    <div class="m-b-120"></div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>