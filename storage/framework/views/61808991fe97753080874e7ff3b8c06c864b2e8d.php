<!DOCTYPE html>
<html>

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>


    <title>Bangla Language Processing - SUST, HEQEP, UGC</title>

    <meta name="csrf-token" id="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('images/sust-logo.png')); ?>">

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i|Open+Sans:300,300i,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,300i,400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,400&display=swap" rel="stylesheet">
    
    <!-- style -->
    <link rel="stylesheet" href="<?php echo e(asset('frontend/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('frontend/css/bootstrap-theme.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('frontend/css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('frontend/owl-carousel/assets/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('frontend/owl-carousel/assets/owl.theme.green.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('frontend/owl-carousel/assets/owl.theme.default.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('frontend/css/custom.css')); ?>">


    <link rel="stylesheet" href="<?php echo e(asset('common/toast/iziToast.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('common/select2/dist/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('common/jquery-ui/jquery-ui.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('frontend/jQuery-filer/css/jquery.filer.css')); ?>">

    <style>
    html {
      scroll-behavior: smooth;
    }
    </style>

    <link href="<?php echo e(asset('common/chosen/chosen.min.css')); ?>" rel="stylesheet">

    <!-- scripts -->
    <script src="<?php echo e(asset('frontend/js/jquery-3.1.1.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/owl-carousel/owl.carousel.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/isotope.min.js')); ?>"></script>

    <script src="<?php echo e(asset('common/jquery_validate/jquery.validate.min.js')); ?>"></script>
    <script src="<?php echo e(asset('common/chosen/chosen.jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('common/jquery-ui/jquery-ui.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/jQuery-filer/js/jquery.filer.min.js')); ?>"></script>

    <?php echo $__env->yieldContent('css'); ?>

</head>
<body  class="frontend page home" >

    
    <?php if((Request::segment(1)=='home') OR (Request::segment(1)=="")): ?>
    <div class="header">
    <?php endif; ?>

        <nav class="navbar navbar-default navbar-fixed-top main-navigation-home main-navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                        
                        <img src="<?php echo e(asset('images/sust-logo.png')); ?>" alt="SUST LOGO">
                        <img src="<?php echo e(asset('images/heqep-logo.png')); ?>" alt="UGC LOGO">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        
                        <li class="active"><a href="<?php echo e(url('/')); ?>">Home<span class="sr-only">(current)</span></a></li>
                        <li class="">
                            <a href="<?php echo e(url('/research')); ?>">Research</a>
                        </li>
                        <li class="dropdown ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">People<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo e(url('/faculty')); ?>">Faculty</a></li>
                                <li><a href="<?php echo e(url('/members')); ?>">Members</a></li>
                                <li><a href="<?php echo e(url('/administrator')); ?>">Administration</a></li>
                            </ul>
                        </li>
                        <li class="dropdown ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Publications<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo e(url('/journals')); ?>">Journals</a></li>
                                <li><a href="<?php echo e(url('/conference')); ?>">Conference</a></li>
                                <li><a href="<?php echo e(url('/books')); ?>">Books</a></li>
                            </ul>
                        </li>
                        <li class="dropdown ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Resources<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <?php
                                    $resource_types = \App\ResourceType::all();
                                ?>
                                <?php $__currentLoopData = $resource_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resource_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="<?php echo e(url('/resource/'.$resource_type->slug)); ?>"><?php echo e($resource_type->name); ?></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </li>

                        <?php if(auth()->guard()->guest()): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                            </li>
                        <?php else: ?>
                             <?php if(Auth::user()->image): ?>
                                <li class="dropdown profile-image">
                            <?php else: ?>
                                <li class="dropdown">
                            <?php endif; ?>
                                <a href="<?php echo e(url('/profile')); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <?php if(Auth::user()->image): ?>
                                    
                                    <div class="img-box" style="width: 60px;height: 60px;margin-bottom: 0px;margin-top: 0px;border: 5px solid #002147;background: #002147;}">
                                        <img class="" src="<?php echo e(url('assets/profile/'.Auth::user()->image)); ?>" alt="<?php echo e(Auth::user()->name); ?>">
                                    </div>
                                        
                                    <?php else: ?>
                                        <?php echo e(Auth::user()->name); ?>

                                    <?php endif; ?>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="<?php echo e(url('/dashboard')); ?>">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(url('/logout')); ?>"
                                           onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


