<?php $__env->startSection('mainContent'); ?>

    <div class="main-wrapper"> <!-- main wrapper -->

        <div class="container">
            <div class="dashboard col-md-12">

                <?php echo $__env->make('backend.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="dashboard-content col-md-9">

                    <div class="publications list">
                        <h3 class="head_title">Members</h3>
                        <div class="row">
                            <div class="col-md-12">

                                <p>
                                    <a href="<?php echo e(url('/dashboard/members')); ?>" class="btn btn-new btn_new_active">Manage Member</a>
                                    <a href="<?php echo e(url('/dashboard/members/create')); ?>" class="btn btn-new">Add New Member</a>
                                </p>

                                <div class="table-responsive">
                                    <table class="table table-hober table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Designation</th>
                                                <th>Phone Number</th>
                                                <th>User status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset($users)): ?>
                                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($user->name); ?></td>
                                                <td>
                                                <?php if($designations): ?>
                                                <?php $__currentLoopData = $designations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $designation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($designation->id == $user->designation): ?>
                                                        <?php echo e($designation->name); ?>

                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </td>
                                                <td><?php echo e(isset($user->phone_no) ? $user->phone_no : ''); ?></td>
                                                <td>
                                                    <?php if($user->is_active): ?>
                                                        <a onclick="return confirm('Are you sure. Want to de-active ?')" href="<?php echo e(url('/dashboard/members/'.$user->id.'/toggle-active')); ?>" class="btn">Deactivate</a>
                                                    <?php else: ?>
                                                        <a onclick="return confirm('Are you sure. Want to active ?')" href="<?php echo e(url('/dashboard/members/'.$user->id.'/toggle-active')); ?>" class="btn">Activate</a>
                                                    <?php endif; ?>
                                                </td>
                                                <td>

                                                    <a class="btn btn-xs btn-primary"  href="<?php echo e(url('/dashboard/members/show/'. $user->id)); ?>" class="btn" target="_blank"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo e(url('/dashboard/members/' . $user->id . '/edit')); ?>"class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                                                    <a onclick="return confirm('Are you sure ?')"  href="<?php echo e(url('/dashboard/members/' . $user->id . '/delete')); ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

                                                </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div> <!-- end main wrapper -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>