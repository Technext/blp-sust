<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsEventCategory extends Model
{
    protected $table = 'news_events_category';

    protected $guarded = [ 'id' ];

    public function news(){
        return $this->belongsToMany(News::class, 'news_category_relations', 'category_id', 'news_id');
    }

    public function events(){
        return $this->belongsToMany(Event::class, 'events_category_relations', 'category_id', 'event_id');
    }
}
