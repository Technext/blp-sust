<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function researchFields(){
        return $this->belongsToMany(ResearchField::class, 'user_research_field_relations', 'user_id', 'research_field_id');
    }

    public function designationDetails(){
        return $this->hasOne(Designation::class, 'id', 'designation');
    }

    public function supervisorDetails(){
        return $this->hasOne(People::class, 'id', 'supervisor');
    }

    public function publications(){
        return $this->hasMany(Publication::class, 'user_id', 'id');
    }

    
}
