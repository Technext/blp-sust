<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryRelation extends Model
{
    protected $table = 'blog_category_relations';

    protected $guarded = ['id'];
}
