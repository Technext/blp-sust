<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResearchField;
use App\LabFacilities;
use App\User;
use App\People;
use App\Publication;
use App\Resource;
use App\ResourceFile;
use App\ResourceType;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
    */
    public function index()
    {
        $data = array();
        $data['meta_title'] = 'Home meta title';
        $data['meta_description'] = 'Home meta description';
        $labFacilities = LabFacilities::all();
        $researchFields = ResearchField::all();
        return view('frontend.home.index',compact('data','labFacilities','researchFields'));
    }

    public function aboutTechnext()
    {
        $data = array();
        $data['meta_title'] = 'Technext meta title';
        $data['meta_description'] = 'Technext meta description';
        return view('frontend.home.technext',compact('data'));
    }

    public function research()
    {
        $data = array();
        $data['meta_title'] = 'Research meta title';
        $data['meta_description'] = 'Research meta description';
        $researchFields = ResearchField::all();
        return view('frontend.research.index',compact('data','researchFields'));
    }

    public function researchSingle($slug){
        
        $data = array();
        $data['meta_title'] = 'Research meta title';
        $data['meta_description'] = 'Research meta description';
        $researchField = ResearchField::where('slug', $slug)->first();
        $members = $researchField->members;
        $supervisors = array();
        foreach ($members as $member){
            if(!isset($member->supervisorDetails)){
                continue;
            }
            if(!in_array($member->supervisorDetails, $supervisors)){
                array_push($supervisors, $member->supervisorDetails);
            }
        }

        return view('frontend.research.single',compact('data','researchField','supervisors'));

    }


    public function allMembers()
    {
        $data = array();
        $data['meta_title'] = 'Member meta title';
        $data['meta_description'] = 'Member meta description';
        $members = User::all();
        return view('frontend.member.index',compact('data','members'));
    }

    public function singleMember($id){
        
        $data = array();
        $data['meta_title'] = 'Member meta title';
        $data['meta_description'] = 'Member meta description';
        $member = User::where('id', $id)->first();
        return view('frontend.member.single',compact('data','member'));

    }

    public function allFaculty()
    {
        $data = array();
        $data['meta_title'] = 'Faculty meta title';
        $data['meta_description'] = 'Faculty meta description';
        $faculties = People::where('type', 'academic')->orderBy('order_index', 'ASC')->get();
        return view('frontend.faculty.index',compact('data','faculties'));
    }

    public function singleFaculty($id){
        
        $data = array();
        $data['meta_title'] = 'Faculty meta title';
        $data['meta_description'] = 'Faculty meta description';
        $faculty = People::where('id', $id)->first();
        return view('frontend.faculty.single',compact('data','faculty'));

    }

    public function allAdministrator()
    {
        $data = array();
        $data['meta_title'] = 'Administrator meta title';
        $data['meta_description'] = 'Administrator meta description';
        $faculties = People::where('type', 'admin')->orderBy('order_index', 'ASC')->get();
        return view('frontend.administrator.index',compact('data','faculties'));
    }

    public function singleAdministrator($id){
        
        $data = array();
        $data['meta_title'] = 'Administrator meta title';
        $data['meta_description'] = 'Administrator meta description';
        $faculty = People::where('id', $id)->first();
        return view('frontend.administrator.single',compact('data','faculty'));

    }

    public function allJournals()
    {
        $data = array();
        $data['meta_title'] = 'Journal meta title';
        $data['meta_description'] = 'Journal meta description';
        $publication = Publication::where('type_of_publication', 'journal')->where('is_profile_only', false)->get();
        $pubType = "Journal";
        return view('frontend.publication.index',compact('data','publication','pubType'));
    }

    public function singleJournals($id){
        
        $data = array();
        $data['meta_title'] = 'Journal meta title';
        $data['meta_description'] = 'Journal meta description';
        $publication = Publication::where('id', $id)->where('type_of_publication', 'journal')->where('is_public', true)->first();
        $pubType = "Journal";
        return view('frontend.publication.single',compact('data','publication','pubType'));

    }

    public function allConference()
    {
        $data = array();
        $data['meta_title'] = 'Conference meta title';
        $data['meta_description'] = 'Conference meta description';
        $publication = Publication::where('type_of_publication', 'conference')->where('is_profile_only', false)->get();
        $pubType = "Conference";
        return view('frontend.publication.index',compact('data','publication','pubType'));
    }

    public function singleConference($id){
        
        $data = array();
        $data['meta_title'] = 'Conference meta title';
        $data['meta_description'] = 'Conference meta description';
        $publication = Publication::where('id', $id)->where('type_of_publication', 'conference')->where('is_public', true)->first();
        $pubType = "Conference";
        return view('frontend.publication.single',compact('data','publication','pubType'));

    }

    public function allBooks()
    {
        $data = array();
        $data['meta_title'] = 'Books meta title';
        $data['meta_description'] = 'Books meta description';
        $publication = Publication::where('type_of_publication', 'books')->where('is_profile_only', false)->get();
        $pubType = "Book";
        return view('frontend.publication.index',compact('data','publication','pubType'));
    }

    public function singleBooks($id){
        
        $data = array();
        $data['meta_title'] = 'Books meta title';
        $data['meta_description'] = 'Books meta description';
        $publication = Publication::where('id', $id)->where('type_of_publication', 'books')->where('is_public', true)->first();
        $pubType = "Book";
        return view('frontend.publication.single',compact('data','publication','pubType'));

    }


    public function singleReesourceTypes($type){
        
        $data = array();
        $data['meta_title'] = 'Resource meta title';
        $data['meta_description'] = 'Resource meta description';

        $type = ResourceType::where('slug', $type)->first();
        $typeName = $type->name;
        if(!auth()->guest()){
            $resources = Resource::where('resource_type', $type->id)->where('is_public', 1)->get();
        }
        else {
            $resources = Resource::where('resource_type', $type->id)->where('is_public', 1)->get();
        }

        return view('frontend.resources.single',compact('data','resources','type','typeName'));

    }


    public function singleReesourceTypeDetails($id){
        
        $data = array();
        $data['meta_title'] = 'Resource meta title';
        $data['meta_description'] = 'Resource meta description';

        $resource = null;
        if(auth()->guest()){
            $resource = Resource::where('id', $id)->where('is_public', true)->first();
        }
        else {
            $resource = Resource::where('id', $id)->first();
        }

        $files = ResourceFile::where('resource_id', $id)->get();

        return view('frontend.resources.details',compact('data','resource','files'));

    }

}
