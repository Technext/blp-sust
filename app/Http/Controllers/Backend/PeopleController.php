<?php

namespace App\Http\Controllers\Backend;

use App\Designation;
use App\People;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use DB;
use Validator;

class PeopleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkUser');
    }

    public function index()
    {
        $data = array();
        $data['meta_title'] = 'Member meta title';
        $data['meta_description'] = 'Member meta description';

        $designations = Designation::all();
        $people = People::orderBy('order_index', 'ASC')->get();

        return view('backend.pages.peoples', [
            'data' => $data,
            'people' => $people,
            'designations' => $designations
        ]);

    }

    public function create(){

        $designations = Designation::pluck('name', 'id');
        return view('backend.pages.people_create', [
            'designations' => $designations
        ]);

    }
    

    public function store(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'name'          => 'required',
                'email'         => 'required|email|unique:people,email',
                'phone_no'      => 'unique:people,phone_no',
                'designation'   => 'exists:designations,id'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
        
            $people = new People();
            $people->name = $request->input('name');
            $people->email = $request->input('email');
            $people->phone_no = $request->input('phone_no') ? $request->input('phone_no') : null;
            $people->designation = $request->input('designation') ? $request->input('designation') : null;
            $people->bio = $request->input('bio') ? $request->input('bio') : null;
            $people->type = $request->input('type');

            if($request->hasFile('image'))
            {
                $imgDetails = $request->file('image');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/profile/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $people->image = $imgName;
            }

            $people->save();

            DB::commit();
            $output = ['status' => 'success','message' => 'Inserted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => $e->getMessage() ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }


    public function show($id){

        $designations = Designation::all();
        $person = People::where('id', $id)->first();
        return view('backend.pages.people_view', [
            'person' => $person,
            'designations' => $designations
        ]);
    
    }
    

    public function edit($id){
        if($id != 0){

            $designations = Designation::pluck('name', 'id');
            $person = People::where('id', $id)->first();

            return view('backend.pages.people_edit', [
                'designations' => $designations,
                'person' => $person
            ]);

        }else{
            return redirect(route('dashboard.peoples'));
        }
    }

    public function update(Request $request){


        try {

            $id = $request->input('id');

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:people,email,' . $id,
                'phone_no' => 'unique:people,phone_no,' . $id,
                'designation' => 'exists:designations,id'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();


            $person = People::where('id', $id)->first();

            $person->name = $request->input('name');
            $person->email = $request->input('email');
            $person->phone_no = $request->input('phone_no') ? $request->input('phone_no') : null;
            $person->designation = $request->input('designation') ? $request->input('designation') : null;
            $person->bio = $request->input('bio') ? $request->input('bio') : null;
            $person->type = $request->input('type') ? $request->input('type') : null;
            $image = $request->file('image');
            
           if($request->hasFile('image'))
            {
                $imgDetails = $request->file('image');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/profile/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $person->image = $imgName;
            }


            $person->save();

            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => $e->getMessage() ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();


    }


    public function updateOrderIndex(Request $request){

        $peopleOrderIndex = $request->get('peopleOrderIndex');
        for($i = 0; $i < count($peopleOrderIndex); $i++){
            $person = People::where('id', intval($peopleOrderIndex[$i]))->first();
            if(isset($person)){
                $person->order_index = $i;
                $person->save();
            }
        }
        return json_encode(array('status' => 200, 'text' => 'got it', 'serverResponse' => $peopleOrderIndex));
    }

    public function delete($id){


        try {

            DB::beginTransaction();

            People::where('id', $id)->delete();

            DB::commit();
            $output = ['status' => 'success','message' => 'Deleted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }
}
