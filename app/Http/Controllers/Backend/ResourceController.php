<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Resource;
use App\ResourceFile;
use App\ResourceType;
use Illuminate\Http\File;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use DB;
use Validator;
use Carbon\Carbon;


class ResourceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = array();
        $data['meta_title'] = 'Resources meta title';
        $data['meta_description'] = 'Resources meta description';
        $resource_types = ResourceType::all();
        $resources = Resource::all()->where('user_id', auth()->user()->id);
        return view('backend.pages.resources',compact('data','resource_types','resources'));

    }

    public function create()
    {
        $data = array();
        $data['meta_title'] = 'Resources meta title';
        $data['meta_description'] = 'Resources meta description';
        $resource_types = ResourceType::all();
        return view('backend.pages.resources_create',compact('data','resource_types'));

    }

    public function store(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'title'             => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }


            DB::beginTransaction();


            $resource = new Resource();
            $resource->title = $request->input('title');
            $resource->user_id = auth()->user()->id;
            $keywords = $request->input('keywords');
            $resource->keywords = (isset($keywords) ? $keywords : null);
            $description = $request->input('description');
            $resource->description = isset($description) ? $description : null;
            $resource_type = $request->input('resource_type');
            $resource->resource_type = isset($resource_type) ? $resource_type : null;
            $reference_url = $request->input('reference_url');
            $resource->reference_url = isset($reference_url) ? $reference_url : null;
            $is_public = $request->input('is_public');
            $resource->is_public = (isset($is_public) && $is_public) ? true : false;
            $version = $request->input('version');
            $resource->version = isset($version) ? $version : null;
            $resource->save();

            if($request->hasFile('image'))
            {
                $files = $request->file('image');
                foreach ($files as $file) {
                    $rand = (rand(1,10000));
                    $galleryImgName = $file->getClientOriginalName();
                    $path           = 'assets/resources/';
                    $ImgName        = explode(".",$galleryImgName);
                    $fullName       = $rand.$request->productName.'.'.$ImgName['1'];
                    $file->move($path,$fullName);

                    $resourceFile = new ResourceFile();
                    $resourceFile->resource_id = $resource->id;
                    $resourceFile->file_name = $fullName;
                    $fileExtension = $file->getClientOriginalExtension();
                    $resourceFile->file_extension = $fileExtension;
                    $resourceFile->save();
                }
            }

            DB::commit();
            $output = ['status' => 'success','message' => 'Inserted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }

    public function show($id)
    {
        $data = array();
        $data['meta_title'] = 'Resources meta title';
        $data['meta_description'] = 'Resources meta description';
        $resources = Resource::where('id', $id)->first();
        $resourceFiles = ResourceFile::where('resource_id', $id)->get();
        $resource_types = ResourceType::all();
        return view('backend.pages.resources_views',compact('data','resources','resourceFiles','resource_types'));
    }

    public function edit($id)
    {
        $data = array();
        $data['meta_title'] = 'Resources meta title';
        $data['meta_description'] = 'Resources meta description';
        $resources = Resource::where('id', $id)->first();
        $resourceFiles = ResourceFile::where('resource_id', $id)->get();
        $resource_types = ResourceType::all();
        return view('backend.pages.resources_edit',compact('data','resources','resourceFiles','resource_types'));

    }

    public function update(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'title'             => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            $id = $request->input('id');

            $resource = Resource::where('id', $id)->first();
            $resource->title = $request->input('title');
            $resource->user_id = auth()->user()->id;
            $keywords = $request->input('keywords');
            $resource->keywords = (isset($keywords) ? $keywords : null);
            $description = $request->input('description');
            $resource->description = isset($description) ? $description : null;
            $resource_type = $request->input('resource_type');
            $resource->resource_type = isset($resource_type) ? $resource_type : null;
            $reference_url = $request->input('reference_url');
            $resource->reference_url = isset($reference_url) ? $reference_url : null;
            $is_public = $request->input('is_public');
            $resource->is_public = (isset($is_public) && $is_public) ? true : false;
            $version = $request->input('version');
            $resource->version = isset($version) ? $version : null;
            $resource->save();

            if($request->hasFile('image'))
            {
                $files = $request->file('image');
                foreach ($files as $file) {
                    $rand = (rand(1,10000));
                    $galleryImgName = $file->getClientOriginalName();
                    $path           = 'assets/resources/';
                    $ImgName        = explode(".",$galleryImgName);
                    $fullName       = $rand.$request->productName.'.'.$ImgName['1'];
                    $file->move($path,$fullName);

                    $resourceFile = new ResourceFile();
                    $resourceFile->resource_id = $id;
                    $resourceFile->file_name = $fullName;
                    $fileExtension = $file->getClientOriginalExtension();
                    $resourceFile->file_extension = $fileExtension;
                    $resourceFile->save();
                }
            }

            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }

    public function delete($id){
        $resource = Resource::where('id', $id)->first();
        if($resource){
            ResourceFile::where('resource_id', $id)->delete();
        }
        $resource->delete();
        $output = ['status' => 'success','message' => 'Deleted successfully'];
        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }

    public function deleteResFile($id){
        ResourceFile::where('id', $id)->delete();
        $output = ['status' => 'success','message' => 'Deleted successfully'];
        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }
}
