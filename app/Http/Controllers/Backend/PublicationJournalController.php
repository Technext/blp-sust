<?php

namespace App\Http\Controllers\Backend;

use App\Publication;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use Validator;


class PublicationJournalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = array();
        $data['meta_title'] = 'Publication meta title';
        $data['meta_description'] = 'Publication meta description';
        return view('backend.pages.publications_journal_create',compact('data'));
    }

    public function store(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'title'             => 'required',
                'author'             => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }


            DB::beginTransaction();

                
            $journalPaper = new Publication();
            $journalPaper->user_id = auth()->user()->id;
            $journalPaper->type_of_publication = 'journal';
            $journalPaper->title = $request->input('title');
            $showPublic = $request->input('is_public');
            $journalPaper->is_public = isset($showPublic) && $showPublic ? true : false;
            $showProfileOnly = $request->input('is_profile_only');
            $journalPaper->is_profile_only = isset($showProfileOnly) && $showProfileOnly ? true : false;
            $abstract = $request->input('abstract');
            $journalPaper->abstract = isset($abstract) ? $abstract : null;
            $author = $request->input('author');
            $journalPaper->author = isset($author) ? $author : null;
            $editor = $request->input('editor');
            $journalPaper->editor = isset($editor) ? $editor : null;
            $publisher = $request->input('publisher');
            $journalPaper->publisher = isset($publisher) ? $publisher : null;
            $journal = $request->input('journal');
            $journalPaper->journal = isset($journal) ? $journal : null;
            $volume = $request->input('volume');
            $journalPaper->volume = isset($volume) ? $volume : null;
            $publishedAt = $request->input('year');
            $journalPaper->published_at = isset($publishedAt) ? Carbon::parse($publishedAt) : null;
            $note = $request->input('notes');
            $journalPaper->note = isset($note) ? $note : null;
            $institution = $request->input('institution');
            $journalPaper->institution = isset($institution) ? $institution : null;
            $address = $request->input('address');
            $journalPaper->address = isset($address) ? $address : null;
            $pages = $request->input('pages');
            $journalPaper->pages = isset($pages) ? $pages : null;
            $referenceUrl = $request->input('reference_url');
            $journalPaper->reference_url = isset($referenceUrl) ? $referenceUrl : null;

            if($request->file('attachment'))
            {

                $imgDetails = $request->file('attachment');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/publications/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $journalPaper->file_url = $imgName;
            } 

            $journalPaper->save();

            DB::commit();
            $output = ['status' => 'success','message' => 'Inserted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }

}
