<?php

namespace App\Http\Controllers\Backend;

use App\Publication;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use DB;
use Validator;


class PublicationBookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = array();
        $data['meta_title'] = 'Publication meta title';
        $data['meta_description'] = 'Publication meta description';
        return view('backend.pages.publications_book_create',compact('data'));
    }

    public function store(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'title'             => 'required',
                'author'             => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }


            DB::beginTransaction();

            $publication = new Publication();
            $publication->user_id = auth()->user()->id;
            $publication->type_of_publication = 'books';
            $publication->title = $request->input('title');
            $bookTitle = $request->input('booktitle');
            $publication->book_title = isset($bookTitle) ? $bookTitle : null;
            $notes = $request->input('notes');
            $publication->note = isset($notes) ? $notes : null;
            $author = $request->input('author');
            $publication->author = isset($author) ? $author : null;
            $editor = $request->input('editor');
            $publication->editor = isset($editor) ? $editor : null;
            $publisher = $request->input('publisher');
            $publication->publisher = isset($publisher) ? $publisher : null;
            $year = $request->input('year');
            $publication->published_at = isset($year) ? Carbon::parse($year) : null;
            $address = $request->input('address');
            $publication->address = isset($address) ? $address : null;
            $series = $request->input('series');
            $publication->series = isset($series) ? $series : null;
            $pages = $request->input('pages');
            $publication->pages = isset($pages) ? $pages : null;
            $referenceUrl = $request->input('reference_url');
            $publication->reference_url = isset($referenceUrl) ? $referenceUrl : null;
            $is_public = $request->input('is_public');
            $publication->is_public = (isset($is_public) && $is_public) ? true : false;
            $is_profile_only = $request->input('is_profile_only');
            $publication->is_profile_only = (isset($is_profile_only) && $is_profile_only) ? true : false;

            if($request->file('attachment'))
            {

                $imgDetails = $request->file('attachment');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/publications/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $publication->file_url = $imgName;
            } 

            $publication->save();

            DB::commit();
            $output = ['status' => 'success','message' => 'Inserted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }

}
