<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Resource;
use App\ResourceType;
use Illuminate\Http\Request;
use DB;
use Validator;

class ResourceTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = array();
        $data['meta_title'] = 'Resources meta title';
        $data['meta_description'] = 'Resources meta description';
        $resource_types = ResourceType::all();
        return view('backend.pages.resource_types',compact('data','resource_types'));

    }

    public function create()
    {
        $data = array();
        $data['meta_title'] = 'Resources meta title';
        $data['meta_description'] = 'Resources meta description';
        return view('backend.pages.resource_type_create',compact('data'));

    }


    public function store(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'name'  => 'required',
                'slug'  => 'required|unique:resource_types,slug'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            $type = new ResourceType();
            $type->name = $request->input('name');
            $type->slug = $request->input('slug');
            $description = $request->input('description');
            $type->description = isset($description) ? $description : null;
            $type->save();

            DB::commit();
            $output = ['status' => 'success','message' => 'Inserted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }


    public function edit($id)
    {
        $data = array();
        $data['meta_title'] = 'Resources meta title';
        $data['meta_description'] = 'Resources meta description';
        $resource_type = ResourceType::where('id', $id)->first();
        return view('backend.pages.resource_type_edit',compact('data','resource_type'));
    }

    public function update(Request $request){

        try {

            $id = $request->input('id');
            $validator = Validator::make($request->all(), [
                'name'  => 'required',
                'slug'      => 'required|unique:resource_types,slug,'.$id
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            $type = ResourceType::where('id', $id)->first();
            if(isset($type)){
                $type->name = $request->input('name');
                $type->slug = $request->input('slug');
                $description = $request->input('description');
                $type->description = isset($description) ? $description : null;
                $type->save();
            }

            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }

    public function delete($id){
        $type = ResourceType::where('id', $id)->first();
        if(isset($type)){
            Resource::where('resource_type', $id)->update(['resource_type' => null]);
            $type->delete();
        }
        $output = ['status' => 'success','message' => 'Deleted successfully'];
        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }
}
