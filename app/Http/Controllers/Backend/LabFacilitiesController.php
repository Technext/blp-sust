<?php

namespace App\Http\Controllers\Backend;

use App\LabFacilities;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use Validator;

class LabFacilitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkUser');
    }

    public function index()
    {
        $data = array();
        $data['meta_title'] = 'Lab meta title';
        $data['meta_description'] = 'Lab meta description';
        $labFacilities = LabFacilities::all();
        return view('backend.pages.lab_facilities',compact('data','labFacilities'));
    }

    public function create()
    {
        $data = array();
        $data['meta_title'] = 'Lab meta title';
        $data['meta_description'] = 'Lab meta description';
        return view('backend.pages.lab_facilities_create',compact('data'));
    }


    public function store(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'image'  => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            $facility = new LabFacilities();
            $caption = $request->input('caption');
            $facility->caption = isset($caption) ? $caption : null;

            if($request->hasFile('image'))
            {
                $imgDetails = $request->file('image');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/lab_facilities/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $facility->image = $imgName;
            }

            $facility->save();

            DB::commit();
            $output = ['status' => 'success','message' => 'Inserted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }


    public function edit($id)
    {
        $data = array();
        $data['meta_title'] = 'Lab meta title';
        $data['meta_description'] = 'Lab meta description';
        $labFacility = LabFacilities::where('id', $id)->first();
        return view('backend.pages.lab_facilities_edit',compact('data','labFacility'));
    }

    public function update(Request $request){

        try {

            DB::beginTransaction();

            $id = $request->input('id');

            $facility = LabFacilities::where('id', $id)->first();
            $caption = $request->input('caption');
            $facility->caption = isset($caption) ? $caption : null;

            if($request->hasFile('image'))
            {
                $imgDetails = $request->file('image');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/lab_facilities/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $facility->image = $imgName;
            }

            $facility->save();

            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }

    public function delete($id){

        $facility = LabFacilities::where('id', $id)->delete();
        $output = ['status' => 'success','message' => 'Deleted successfully'];
        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }
}
