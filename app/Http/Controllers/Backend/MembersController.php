<?php

namespace App\Http\Controllers\Backend;

use App\Designation;
use App\People;
use App\ResearchField;
use App\User;
use App\UserResearchFieldRelation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests;
use DB;
use Validator;

class MembersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkUser');
    }

    public function index()
    {
        $data = array();
        $data['meta_title'] = 'Member meta title';
        $data['meta_description'] = 'Member meta description';
        $users = User::where('is_admin', false)->get();
        $designations = Designation::all();
        $research_fields = ResearchField::all();
        $faculties = People::where('type', 'academic')->get();
        return view('backend.pages.members', [
            'data' => $data,
            'users' => $users,
            'designations' => $designations,
            'research_fields' => $research_fields,
            'faculties' => $faculties
        ]);

    }

    public function create(){

        $designations = Designation::pluck('name', 'id');
        $researchFields = ResearchField::pluck('name', 'id');
        $faculties = People::where('type', 'academic')->get();
        return view('backend.pages.member_create', [
            'designations' => $designations,
            'research_fields' => $researchFields,
            'faculties'         => $faculties
        ]);

    }
    

    public function store(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'name'              => 'required',
                'email'             => 'required|email|unique:users,email',
                'password'          => 'required',
                'phone_no'          => 'unique:users,phone_no',
                'designation'       => 'exists:designations,id'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            $name = $request->input('name');
            $email = $request->input('email');
            $password = $request->input('password');
            $phone_no = $request->input('phone_no') ? $request->input('phone_no') : null;
            $bio = $request->input('bio') ? $request->input('bio') : null;
            $designation = $request->input('designation') ? $request->input('designation') : null;
            $dept = $request->input('dept_name') ? $request->input('dept_name') : 'Department of Computer Science and Engineering';
            $institute = $request->input('institute') ? $request->input('institute') : 'Shahjalal University of Science and Technology';
            $researches = $request->input('research_fields') ? $request->input('research_fields') : null;
            $supervisor = $request->input('supervisor') ? $request->input('supervisor') : null;

            $user = new User();
            $user->name = $name;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->phone_no = $phone_no;
            $user->bio = $bio;
            $user->designation = $designation;
            $user->dept_name = $dept;
            $user->institute_name = $institute;
            $user->supervisor = $supervisor;
            $user->save();

            if($researches){
                foreach ($researches as $research) {
                    $researchUserRelation = new UserResearchFieldRelation();
                    $researchUserRelation->user_id = $user->id;
                    $researchUserRelation->research_field_id = $research;
                    $researchUserRelation->save();
                }
            }


            if($request->hasFile('image'))
            {
                $imgDetails = $request->file('image');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/profile/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $user->image = $imgName;
                $user->save();
            }


            DB::commit();
            $output = ['status' => 'success','message' => 'Inserted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => $e->getMessage() ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }


    public function show($id){
        if($id != 0){
            $user = User::find($id);
            if($user){
                $designations = Designation::all();
                $designation = Designation::where('id', $user->designation)->first();
                $research_fields = ResearchField::all();
                $faculties = People::where('type', 'academic')->get();
                return view('backend.pages.member_view', [
                    'user' => $user,
                    'designation' => $designation,
                    'designations' => $designations,
                    'research_fields' => $research_fields,
                    'faculties'         => $faculties
                ]);
            }
            else{
                return redirect(route('dashboard.members'));
            }
        }
    }
    

    public function edit($id){
        if($id != 0){
            $user = User::find($id);
            if($user){
                $designations = Designation::pluck('name', 'id');
                $research_fields = ResearchField::pluck('name', 'id');
                $designation = Designation::where('id', $user->designation)->first();
                $faculties = People::where('type', 'academic')->get();
                return view('backend.pages.member_edit', [
                    'user' => $user,
                    'designation' => $designation,
                    'designations' => $designations,
                    'research_fields' => $research_fields,
                    'faculties'         => $faculties
                ]);
            }
            else{
                return redirect(route('dashboard.members'));
            }
        }
    }

    public function update(Request $request){


        try {

            $id = $request->input('id');
            
            $validator = Validator::make($request->all(), [
                'name'          => 'required',
                'email'         => 'required|email|unique:users,email,'.$id,
                'phone_no'      => 'unique:users,phone_no,'.$id,
                'designation'   => 'exists:designations,id'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();


            $user = User::where('id', $id)->first();

            $old_password = $request->input('password');
            $newPassword = $request->input('new_password');
            if($old_password){
                if(!(Hash::check($old_password, $user->password))){
                    $output = ['status' => 'error','message' => 'Old password does not match !'];
                    \Session::flash('sess_alert',$output);
                    return redirect()->back();
                }else{
                    $user->password = Hash::make($newPassword);
                }
            }

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone_no = !empty($request->input('phone_no')) ? $request->input('phone_no') : null;
            $user->designation = !empty($request->input('designation')) ? $request->input('designation') : null;
            $user->dept_name = !empty($request->input('dept_name')) ? $request->input('dept_name') : 'Department of Computer Science and Engineering';
            $user->institute_name = !empty($request->input('institute')) ? $request->input('institute') : 'Shahjalal University of Science and Technology';
            $user->bio = !empty($request->input('bio')) ? $request->input('bio') : null;
            $user->supervisor = !empty($request->input('supervisor')) ? $request->input('supervisor') : null;
            $researchFields = $request->input('research_fields');
            if($researchFields){
                $user->researchFields()->sync($researchFields);
            }

           if($request->hasFile('image'))
            {
                $imgDetails = $request->file('image');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/profile/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $user->image = $imgName;
                $user->save();
            }

            $user->save();


            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => $e->getMessage() ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();


    }

    public function changeActive($id){

        try {

            DB::beginTransaction();

            $user = User::where('id', $id)->first();
            if(isset($user)){
                if($user->is_active){
                    $user->is_active = false;
                }
                else {
                    $user->is_active = true;
                }
                $user->save();
            }

            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();


    }

    public function delete($id){


        try {

            DB::beginTransaction();
            
            // UserResearchFieldRelation::where('user_id', $id)->delete();
            // User::where('id', $id)->delete();
            
            DB::table('users')->where('id', $id)->delete();
            DB::table('resources')->where('user_id', $id)->delete();
            DB::table('publications')->where('user_id', $id)->delete();
            DB::table('user_research_field_relations')->where('user_id', $id)->delete();

            DB::commit();
            $output = ['status' => 'success','message' => 'Deleted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            // $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];
            $output = [ 'status' => 'error','message' => 'Something is went to wrong', 'show_error' => $e->getMessage() ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }
}
