<?php

namespace App\Http\Controllers\Backend;

use App\Publication;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;


class PublicationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $publications = Publication::all()->where('user_id', auth()->user()->id);
        return view('backend.pages.publications', ['publications' => $publications]);
    }

    public function show($id)
    {
        $datas = Publication::where('id', $id)->first();
        return view('backend.pages.single_publications', ['datas' => $datas]);
    }
    
    public function edit($id)
    {
        $datas = Publication::where('id', $id)->first();
        $data['meta_title'] = 'Publication meta title';
        $data['meta_description'] = 'Publication meta description';
        return view('backend.pages.publication_edit',compact('data','datas'));
    }

    public function delete($id){

        try {

            DB::beginTransaction();

            $publication = Publication::where('id', $id)->first();
            if(isset($publication)){
                $publication->delete();
            }
                
            DB::commit();
            $output = ['status' => 'success','message' => 'Deleted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }


    public function update(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'title'             => 'required',
                'author'             => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            $publication_type = $request->input('type_of_publication');
            $id = $request->input('publication_id');

            $publication = Publication::where('id', $id)->first();
            if(!isset($publication)){
                return redirect()->back();
            }

            //Common start

            $publication->title = $request->input('title');

            $notes = $request->input('notes');
            $publication->note = isset($notes) ? $notes : null;

            $is_public = $request->input('is_public');
            $publication->is_public = (isset($is_public) && $is_public) ? true : false;

            $is_profile_only = $request->input('is_profile_only');
            $publication->is_profile_only = (isset($is_profile_only) && $is_profile_only) ? true : false;


            $author = $request->input('author');
            $publication->author = isset($author) ? $author : null;

            $year = $request->input('year');
            $publication->published_at = isset($year) ? Carbon::parse($year) : null;

            $address = $request->input('address');
            $publication->address = isset($address) ? $address : null;

            $referenceUrl = $request->input('reference_url');
            $publication->reference_url = isset($referenceUrl) ? $referenceUrl : null;

            if($request->file('attachment'))
            {

                $imgDetails = $request->file('attachment');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/publications/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $publication->file_url = $imgName;
            } 

            //Common end

            if($publication_type=="books"){
                $bookTitle = $request->input('booktitle');
                $publication->book_title = isset($bookTitle) ? $bookTitle : null;

                $series = $request->input('series');
                $publication->series = isset($series) ? $series : null;
            }


            if($publication_type=="conference"){
                $institution = $request->input('institution');
                $publication->institution = isset($institution) ? $institution : null;
            }

            if($publication_type=="journal"){
                $institution = $request->input('institution');
                $publication->institution = isset($institution) ? $institution : null;

                $abstract = $request->input('abstract');
                $publication->abstract = isset($abstract) ? $abstract : null;
                
                $journal = $request->input('journal');
                $publication->journal = isset($journal) ? $journal : null;
                
                $volume = $request->input('volume');
                $publication->volume = isset($volume) ? $volume : null;
            }


            if($publication_type=="books"){
                $bookTitle = $request->input('booktitle');
                $publication->book_title = isset($bookTitle) ? $bookTitle : null;

                $series = $request->input('series');
                $publication->series = isset($series) ? $series : null;
            }


            $editor = $request->input('editor');
            $publication->editor = isset($editor) ? $editor : null;

            $publisher = $request->input('publisher');
            $publication->publisher = isset($publisher) ? $publisher : null;




            $pages = $request->input('pages');
            $publication->pages = isset($pages) ? $pages : null;

            $publication->save();

            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }


    
}
