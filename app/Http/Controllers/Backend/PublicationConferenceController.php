<?php

namespace App\Http\Controllers\Backend;

use App\Publication;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use DB;
use Validator;


class PublicationConferenceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = array();
        $data['meta_title'] = 'Publication meta title';
        $data['meta_description'] = 'Publication meta description';
        return view('backend.pages.publications_conference_create',compact('data'));
    }


    public function showView($id = 0)
    {
        if($id != 0){
            $conferencePaper = Publication::where('id', $id)->first();
            return view('pages/dashboard', ['conferencePaper' => $conferencePaper]);
        }
        return view('pages/dashboard');
    }


    public function store(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'title'             => 'required',
                'author'             => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }


            DB::beginTransaction();


            $conferencePaper = new Publication();
            $conferencePaper->user_id = auth()->user()->id;
            $conferencePaper->type_of_publication = 'conference';
            $conferencePaper->title = $request->input('title');
            $notes = $request->input('notes');
            $conferencePaper->note = isset($notes) ? $notes : null;
            $showPublic = $request->input('is_public');
            $conferencePaper->is_public = isset($showPublic) && $showPublic ? true : false;
            $showProfileOnly = $request->input('is_profile_only');
            $conferencePaper->is_profile_only = isset($showProfileOnly) && $showProfileOnly ? true : false;
            $author = $request->input('author');
            $conferencePaper->author = isset($author) ? $author : null;
            $publishedAt = $request->input('year');
            $conferencePaper->published_at = isset($publishedAt) ? Carbon::parse($publishedAt) : null;
            $address = $request->input('address');
            $conferencePaper->address = isset($address) ? $address : null;
            $referenceUrl = $request->input('reference_url');
            $conferencePaper->reference_url = isset($referenceUrl) ? $referenceUrl : null;
            $institution = $request->input('institution');
            $conferencePaper->institution = isset($institution) ? $institution : null;

            if($request->file('attachment'))
            {

                $imgDetails = $request->file('attachment');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/publications/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $conferencePaper->file_url = $imgName;
            } 

            $conferencePaper->save();

            DB::commit();
            $output = ['status' => 'success','message' => 'Inserted successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }

}
