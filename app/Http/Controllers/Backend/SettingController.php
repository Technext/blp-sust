<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\MetaData;
use Faker\Provider\File;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

use DB;
use Validator;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkUser');
    }

    public function index()
    {

        $data = array();
        $data['meta_title'] = 'Homepage meta title';
        $data['meta_description'] = 'Homepage meta description';

        $datas = DB::table('settings')->where('id',1)->first();
        return view('backend.pages.editor_homepage', ['datas' => $datas,'data'=> $data]);

    }


    public function update(Request $request){

        try {

            DB::beginTransaction();

            $data['homepage_header_title_1'] = $request->input('homepage_header_title_1');
            $data['homepage_about_title_1'] = $request->input('homepage_about_title_1');
            $data['homepage_about_title_2'] = $request->input('homepage_about_title_2');
            $data['homepage_about_paragraph'] = $request->input('homepage_about_paragraph');
            $data['custom_field1'] = $request->input('custom_field1');
            $data['custom_field2'] = $request->input('custom_field2');
            $data['custom_field3'] = $request->input('custom_field3');

            $affected = DB::table('settings')
                          ->where('id', 1)
                          ->update($data);
            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();
    }

    public function footer(){

        $data = array();
        $data['meta_title'] = 'Footer meta title';
        $data['meta_description'] = 'Footer meta description';
        $footerSocialLinks = array();
        $footerSocialLinks = DB::table('settings')->where('id',1)->first();
        return view('backend.pages.editor_footer', ['footerSocialLinks' => $footerSocialLinks,'data'=> $data]);

    }

    public function updateFooter(Request $request){

        try {

            DB::beginTransaction();

            $data['facebook'] = $request->input('facebook_url');
            $data['twitter'] = $request->input('twitter_url');
            $data['youtube'] = $request->input('youtube_url');
            $data['github'] = $request->input('github_url');
            $data['bitbucket'] = $request->input('bitbucket_url');

            $affected = DB::table('settings')
                          ->where('id', 1)
                          ->update($data);
            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();

    }
}
