<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Designation;
use App\People;
use App\ResearchField;

use App\Http\Requests;
use DB;
use Validator;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $designation = Designation::where('id', auth()->user()->designation)->first();
        $designations = Designation::all();
        $researchFields = ResearchField::all();
        $faculties = People::where('type', 'academic')->get();
        return view('backend.pages.dashboard', [
            'designation'       => $designation,
            'designations'      => $designations,
            'research_fields'   => $researchFields,
            'faculties'         => $faculties
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {

        $designation = Designation::where('id', auth()->user()->designation)->pluck('name', 'id');
        $designations = Designation::pluck('name', 'id');
        $researchFields = ResearchField::pluck('name', 'id');
        $faculties = People::where('type', 'academic')->get();
        return view('backend.pages.profile', [
            'designation'       => $designation,
            'designations'      => $designations,
            'research_fields'   => $researchFields,
            'faculties'         => $faculties
        ]);

    }

    public function update(Request $request){

        try {

            $validator = Validator::make($request->all(), [
                'name'          => 'required',
                'email'         => 'required|email|unique:users,email,'.auth()->user()->id,
                'phone_no'      => 'unique:users,phone_no,'.auth()->user()->id,
                'designation'   => 'exists:designations,id'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }


            DB::beginTransaction();

            $user = auth()->user();

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone_no = !empty($request->input('phone_no')) ? $request->input('phone_no') : null;
            $user->designation = !empty($request->input('designation')) ? $request->input('designation') : null;
            $user->dept_name = !empty($request->input('dept_name')) ? $request->input('dept_name') : 'Department of Computer Science and Engineering';
            $user->institute_name = !empty($request->input('institute')) ? $request->input('institute') : 'Shahjalal University of Science and Technology';
            $user->bio = !empty($request->input('bio')) ? $request->input('bio') : null;
            $researchFields = $request->input('research_fields');
            if($researchFields){
                $user->researchFields()->sync($researchFields);
            }
            $supervisor = $request->input('supervisor');
            if(isset($supervisor)){
                $user->supervisor = $supervisor;
            }


            if($request->hasFile('image'))
            {

                $imgDetails = $request->file('image');
                $getTime = $imgDetails->getATime();
                $orginalName = $imgDetails->getClientOriginalName();
                $imagePath      = 'assets/profile/';
                $getBGImageName = explode(".",$orginalName);
                $imgName = $getTime.mt_rand(0,5).'.'.$getBGImageName['1'];
                $imgDetails->move($imagePath,$imgName);

                $user->image = $imgName;
            }

            $user->save();
            
            DB::commit();
            $output = ['status' => 'success','message' => 'Updated successfully'];

        } catch (\Exception $e) {

            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [ 'status' => 'error','message' => 'Something is went to wrong' ];

        }

        \Session::flash('sess_alert',$output);
        return redirect()->back();


    }







}
