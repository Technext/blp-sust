<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCategoryRelation extends Model
{
    /**
     * table associated with this model
     * @var string
     */
    protected $table = 'news_category_relations';

    protected $guarded = ['id'];
}
