<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabFacilities extends Model
{
    protected $table = 'lab_facilities';

    protected $guarded = ['id'];
}
