<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCategoryRelation extends Model
{
    protected $table = 'events_category_relations';
}
