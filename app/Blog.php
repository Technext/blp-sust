<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * Table associated with this model
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'user_id'
    ];

    public function categories(){
        return $this->belongsToMany(BlogCategory::class, 'blog_category_relations', 'blog_id', 'category_id');
    }

    public function author(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
