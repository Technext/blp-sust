<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * table associated with this model
     * @var string
     */
    protected $table = 'events';

    /**
     * columns that cannot be mass assigned
     * @var array
     */
    protected $guarded = ['id', 'user_id'];

    public function categories(){
        return $this->belongsToMany(NewsEventCategory::class, 'events_category_relations', 'event_id', 'category_id');
    }

    public function author(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
