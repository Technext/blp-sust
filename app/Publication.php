<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    /**
     * table associated with this model
     * @var string
     */
    protected $table = 'publications';

    /**
     * columns that cannot be mass assigned
     * @var array
     */
    protected $guarded = ['id', 'user_id', 'type_of_publication'];

    public static $types = array(
        'journal'       => 'Journal',
        'conference'    => 'Conference',
        'books'          => 'Book'
    );
}
