<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ResearchField extends Model
{
    use Notifiable;

    /**
     * Table associated with this model
     *
     * @var string
     */
    protected $table = 'research_fields';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    public function members(){
        return $this->belongsToMany(User::class, 'user_research_field_relations', 'research_field_id', 'user_id');
    }
}
