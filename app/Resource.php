<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = 'resources';

    protected $guarded = ['id', 'user_id'];

    public function resourceType(){
        return $this->belongsTo(ResourceType::class, 'resource_type');
    }

    public function files(){
        return $this->hasMany(ResourceFile::class, 'resource_id', 'id' );
    }

    public function type(){
        return $this->belongsTo(ResourceType::class, 'resource_type', 'id');
    }
}
