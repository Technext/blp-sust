<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    //

    public function designationDetails(){
        return $this->hasOne(Designation::class, 'id', 'designation');
    }
}
