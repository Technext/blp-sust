<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $table = 'blog_category';

    protected $guarded = [ 'id' ];

    public function blogs(){
        return $this->belongsToMany(Blog::class, 'blog_category_relations', 'category_id', 'blog_id');
    }
}
