<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceFile extends Model
{
    protected $table = 'resource_files';

    protected $guarded = ['id'];

    public function fileLocation(){
        return 'resource/'.$this->resource_id;
    }
}
