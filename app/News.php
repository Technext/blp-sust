<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * table associated with this model
     * @var string
     */
    protected $table = 'news';

    /**
     * an array of columns that cannot be mass assigned
     * @var array
     */
    protected $guarded = [ 'id', 'user_id' ];

    public function categories(){
        return $this->belongsToMany(NewsEventCategory::class, 'news_category_relations', 'news_id', 'category_id');
    }

    public function author(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
