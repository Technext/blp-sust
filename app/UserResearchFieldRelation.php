<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserResearchFieldRelation extends Model
{
    use Notifiable;

    /**
     * Table associated with this model
     *
     * @var string
     */
    protected $table = 'user_research_field_relations';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];
}
