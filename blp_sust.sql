-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2020 at 07:10 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blp_sust`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_category_relations`
--

CREATE TABLE `blog_category_relations` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'academic',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `slug`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Lecturer', 'lecturer', 'academic', NULL, NULL),
(2, 'Asst. Professor', 'asst-professor', 'academic', NULL, NULL),
(3, 'Associate Professor', 'associate-professor', 'academic', NULL, NULL),
(4, 'Professor', 'professor', 'academic', NULL, NULL),
(5, 'Senior Lecturer', 'senior-lecturer', 'academic', NULL, NULL),
(6, 'Office Secretary', 'office-sec', 'admin', NULL, NULL),
(7, 'Computer Operator', 'computer-operator', 'admin', NULL, NULL),
(8, 'MLSS', 'mlss', 'admin', NULL, NULL),
(9, 'Accountant', 'accountant', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_of_the_event` datetime DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events_category_relations`
--

CREATE TABLE `events_category_relations` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lab_facilities`
--

CREATE TABLE `lab_facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lab_facilities`
--

INSERT INTO `lab_facilities` (`id`, `image`, `caption`, `created_at`, `updated_at`) VALUES
(7, '16067983853.jpg', 'First image', '2020-12-01 11:53:05', '2020-12-01 11:53:05'),
(8, '16067984015.jpg', 'Second image', '2020-12-01 11:53:21', '2020-12-01 11:53:21'),
(9, '16067984170.jpg', 'Third image', '2020-12-01 11:53:37', '2020-12-01 11:53:37'),
(10, '16067984382.jpg', 'Fourth image', '2020-12-01 11:53:58', '2020-12-01 11:53:58'),
(11, '16067984554.jpg', 'Fifth image', '2020-12-01 11:54:16', '2020-12-01 11:54:16'),
(12, '16067984722.jpg', 'Sixth image', '2020-12-01 11:54:32', '2020-12-01 11:54:32');

-- --------------------------------------------------------

--
-- Table structure for table `metadata`
--

CREATE TABLE `metadata` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_10_27_070525_create_blogs_table', 1),
(4, '2016_11_15_080554_create_research_field_table', 1),
(5, '2016_11_16_053727_create_user_research_field_relation_table', 1),
(6, '2016_11_16_092157_create_blog_category_table', 1),
(7, '2016_11_16_092248_create_blog_category_relation_table', 1),
(8, '2016_11_16_092323_create_designation_table', 1),
(9, '2016_11_16_092411_create_people_table', 1),
(10, '2016_11_16_094310_create_metadata_table', 1),
(11, '2016_11_16_094352_add_users_table_foreign_keys', 1),
(12, '2016_11_16_094440_add_user_research_field_relations_table_foreign_keys', 1),
(13, '2016_11_16_094526_add_people_table_foreign_keys', 1),
(14, '2016_11_16_094548_add_blogs_table_foreign_keys', 1),
(15, '2016_11_16_094617_add_blog_category_relations_table_foreign_keys', 1),
(16, '2016_11_17_115512_add_image_field_to_people_table', 1),
(17, '2016_11_29_050343_add_featured_image_field_to_blogs_table', 1),
(18, '2016_12_02_111216_create_news_table', 1),
(19, '2016_12_02_112913_create_news_event_category_table', 1),
(20, '2016_12_03_060039_create_events_table', 1),
(21, '2016_12_03_061043_create_news_category_relation_table', 1),
(22, '2016_12_03_062815_create_event_category_relations_table', 1),
(23, '2016_12_03_063349_add_foreign_key_to_news_table', 1),
(24, '2016_12_03_063750_add_foreign_key_to_events_table', 1),
(25, '2016_12_03_064030_add_foreign_key_to_news_category_relations_table', 1),
(26, '2016_12_03_064319_add_foreign_key_to_events_category_relations_table', 1),
(27, '2016_12_05_065526_add_featured_image_field_to_news_table', 1),
(28, '2016_12_05_065800_add_featured_image_field_to_events_table', 1),
(29, '2016_12_05_065940_add_published_at_field_to_events_table', 1),
(30, '2016_12_07_063405_create_publications_table', 1),
(31, '2016_12_07_081420_add_foreign_keys_to_publications_table', 1),
(32, '2016_12_09_123023_create_resources_table', 1),
(33, '2016_12_09_124514_create_resource_types_table', 1),
(34, '2016_12_09_124523_add_foreign_keys_to_resources_table', 1),
(35, '2016_12_15_110958_add_author_field_to_publication_table', 1),
(36, '2016_12_27_063137_add_is_active_field_to_user_table', 1),
(37, '2016_12_28_063921_add_type_field_to_designation_table', 1),
(38, '2016_12_31_074037_create_resource_files_table', 1),
(39, '2016_12_31_084718_add_resource_files_table_foreign_key', 1),
(40, '2017_01_09_094614_add_order_index_field_in_people_table', 1),
(41, '2017_03_08_111020_add_preview_image_field_to_research_field', 1),
(42, '2017_03_22_102247_add_dept_and_institute_name_field_to_users_table', 1),
(43, '2017_03_27_130021_add_supervisor_field_to_users_table', 1),
(44, '2017_03_27_140606_add_supervisor_foreign_key_to_users_table', 1),
(45, '2017_03_28_101716_create_lab_facilities_table', 1),
(46, '2017_05_24_090245_add_show_profile_only_field_to_publication_table', 1),
(47, '2020_10_29_072340_create_sessions_table', 1),
(48, '2020_11_07_103232_create_settings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_category_relations`
--

CREATE TABLE `news_category_relations` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_events_category`
--

CREATE TABLE `news_events_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` longtext COLLATE utf8mb4_unicode_ci,
  `designation` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_index` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `name`, `email`, `phone_no`, `bio`, `designation`, `type`, `image`, `order_index`, `created_at`, `updated_at`) VALUES
(2, 'Dr. Muhammed Zafar Iqbal', 'drzafar@gmail.com', '01714725836', '<p>Comming soon..</p>', 4, 'academic', '16067979481.jpg', 1, '2020-12-01 11:45:48', '2020-12-01 11:45:48'),
(3, 'Dr Mohammad Shahidur Rahman', 'mdshahidur@gmail.com', '01712345678', '<p>Comming soon&nbsp;</p>', 4, 'academic', '16067980364.jpeg', 1, '2020-12-01 11:47:16', '2020-12-01 11:47:16'),
(4, 'Dr Mohammad Reza Selim', 'mdreza@gmail.com', '01741414141', '<p>Comming soon</p>', 1, 'academic', '16067980802.jpeg', 1, '2020-12-01 11:48:00', '2020-12-01 11:48:00'),
(5, 'Arif Alon', 'arif@gmail.com', '01654789587', '<p>Comming soon</p>', 9, 'admin', '16067993011.jpeg', 1, '2020-12-01 12:08:21', '2020-12-01 12:08:21');

-- --------------------------------------------------------

--
-- Table structure for table `publications`
--

CREATE TABLE `publications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_of_publication` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `abstract` longtext COLLATE utf8mb4_unicode_ci,
  `published_at` datetime DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publisher` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pages` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `journal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `volume` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institution` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  `is_profile_only` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `publications`
--

INSERT INTO `publications` (`id`, `user_id`, `title`, `type_of_publication`, `book_title`, `abstract`, `published_at`, `address`, `editor`, `publisher`, `author`, `pages`, `series`, `note`, `journal`, `volume`, `institution`, `file_url`, `reference_url`, `is_public`, `is_profile_only`, `created_at`, `updated_at`) VALUES
(2, 2, 'Centromere protein U expression promotes non-small-cell lung cancer cell proliferation through FOXM1 and predicts poor survival', 'journal', NULL, '<p><strong style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13.125px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\">Purpose:&nbsp;</strong><span style=\"color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.125px;\">Centromere protein U (CENPU) abnormally exhibits high expression in various types of human tumor tissues and participates in tumor progression; however, its expression pattern and biological function in lung cancer have not yet been elucidated. In the present study, we explored the clinical significance and biological function of CENPU in lung cancer.</span><br style=\"color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.125px;\"><strong style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13.125px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\">Materials and methods:</strong><span style=\"color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.125px;\">&nbsp;The Cancer Genome Atlas (TCGA) data analyses, quantitative real-time PCR (RT-PCR), and Western blotting were performed to quantify CENPU and FOXM1 expression in non-small-cell lung cancer (NSCLC) samples. Survival data were obtained from Kaplan–Meier plotter or PROGgene V2 prognostic database. The function of CENPU in lung cancer cell proliferation was determined using 5-ethynyl-2′-deoxyuridine (EdU), Cell Counting Kit-8 (CCK-8), and cell cycle assays, and the underlying mechanism was determined through bioinformatic analyses and validated by in vitro siRNA or plasmid transfection experiments.</span><br style=\"color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.125px;\"><strong style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13.125px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\">Results:&nbsp;</strong><span style=\"color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.125px;\">CENPU was abnormally overexpressed in NSCLC samples compared with matched paired normal tissues. Higher expression of CENPU predicted worse overall survival (OS) and relapse-free survival (RFS) in NSCLC patients. Knockdown of CENPU expression by siRNA significantly inhibited proliferation and delayed cell cycle progression of lung cancer cells. To figure out the mechanism, bioinformatic analyses were performed and the results showed that the transcription factor, FOXM1, positively correlated with CENPU. Further in vitro experiments indicated that FOXM1 was the possible downstream transcription factor of CENPU as the knockdown of CENPU led to lower expression of FOXM1 and the overexpression of FOXM1 significantly reversed the inhibition of proliferation caused by CENPU knockdown. Furthermore, FOXM1 was highly expressed in NSCLC. The knockdown of FOXM1 also attenuated proliferation and induced G1 arrest in lung cancer cells.</span><br style=\"color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.125px;\"><p><strong style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13.125px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\">Conclusion:</strong><span style=\"color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.125px;\">&nbsp;CENPU was highly expressed in NSCLC tissues, wherein it promoted lung cancer cell proliferation via the transcription factor, FOXM1, which could be a potential target for therapeutic strategies.</span></p></p>', '2020-12-01 00:00:00', 'City, Akhaliya', 'Emon', 'Akhter', 'Emon', '10', NULL, '<p><span style=\"color: rgb(85, 85, 85); font-family: Verdana, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.125px;\">CENPU, NSCLC, proliferation, FOXM1</span><br></p>', 'A1-00', 'V-1', 'Sust', '16068014132.pdf', 'https://www.dovepress.com/centromere-protein-u-expression-promotes-non-small-cell-lung-cancer-ce-peer-reviewed-article-CMAR', 1, 0, '2020-12-01 12:43:33', '2020-12-01 12:43:33');

-- --------------------------------------------------------

--
-- Table structure for table `research_fields`
--

CREATE TABLE `research_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `preview_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `research_fields`
--

INSERT INTO `research_fields` (`id`, `name`, `slug`, `description`, `preview_image`, `created_at`, `updated_at`) VALUES
(1, 'Bangla Text to Speech', 'bangla-tts', 'Improving an existing Bangla TTS system ‘Subachan’ (developed in SUST). Diaphone-based output in Subachan was not smooth. New system is defined with less concatenation point. Identified most frequent Bangla words that are directly pronounced to obtain a more natural sounding output. Implementing a durational model using PSOLA algorithm.', 'f55794931a0413062cb3e4c768450a26.png', '2020-12-01 17:36:09', '2020-12-01 17:36:09'),
(2, 'Bangla Optical Character Recognition', 'bangla-ocr', 'Preprocessing is done,Character Segmentation is done,Working with recognition phase using ANN (Artificial Neural Network) Preparing training data', '194d2638a10af1f6d0733ad3f445b7b5.jpeg', '2020-12-01 17:36:09', '2020-12-01 17:36:09'),
(3, 'English to Bangla Translation', 'e2b-translation', 'Resource collection: English-Bengali parallel corpus(Supara, Indic-Parallel, Indian Statistical Institutes\' Corpus), Bengali Monolingual Coupus (SUMono, Indian Statistical Institutes\' Corpus, EMILLE Corpus), Open Subtitle 2016 Corpus\r\nImplementation of Baseline System: Baseline Statistical Machine Translation System(using Moses, Giza++, KenLM)', '4e420c8045b4e9290753001b48bbc900.png', '2020-12-01 17:40:34', '2020-12-01 17:40:34'),
(4, 'Bangla Speech to Text', 'bangla-stt', 'A demo for ANN-based Speaker Independent Isolated Speech Recognizer for Bangla Word is already developed. Now, exploring the different Speech Feature extraction methods to identify the speech in robust way We are also exploring the techniques to develop a Standard Bangla Speech Corpora for Speaker Independent Continuous SR as well as Isolated SR Now we are working with Bangla Phonemes recognition techniques, which will be a part of a Standard Speech Corpora', '2913b3a1549c6fddfeb8a9e849d98674.png', '2020-12-01 17:40:34', '2020-12-01 17:40:34');

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `resource_type` int(10) UNSIGNED DEFAULT NULL,
  `reference_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `user_id`, `title`, `keywords`, `description`, `resource_type`, `reference_url`, `is_public`, `version`, `created_at`, `updated_at`) VALUES
(1, 2, 'Utilization of Common Reed (Phragmites australis) as Bedding for Housed Suckler Cows: Practical and Economic Aspects for Farmers', 'Phragmites ,Cows, Utilization', '<p><span style=\"color: rgb(34, 34, 34); font-family: Arial; font-size: 12px;\">The common reed (</span><span class=\"html-italic\" style=\"max-height: 1e+06px; font-style: italic; display: inline; color: rgb(34, 34, 34); font-family: Arial; font-size: 12px;\">Phragmites australis</span><span style=\"color: rgb(34, 34, 34); font-family: Arial; font-size: 12px;\">) has long been used in wetlands of the French Atlantic coast as fodder and bedding or roof thatching, among other uses. This article explores the practical and economic aspects of utilizing common reed for housing suckler cows compared to straw. Based on a study conducted over two years on a research farm of the French National Research Institute for Agriculture, Food and the Environment (INRAE), located in the marshes of Rochefort-sur-Mer, we show that reed is a good alternative to cereal straw and its cost is quite competitive compared to straw; the closer the reed bed is to the farm, the more competitive it is. By mobilizing the concept of restoration of natural capital, we lay the foundations for a debate on a possible revival of this ancient practice, with the idea that ecological restoration of reed beds can benefit biodiversity and the economy of wetlands farms.&nbsp;</span><a href=\"https://www.mdpi.com/2079-9276/9/12/140/htm\" style=\"color: rgba(5, 83, 125, 0.75); line-height: inherit; max-height: 1e+06px; font-weight: 700; font-family: Arial; font-size: 12px; background-color: rgb(255, 255, 255);\">View Full-Text</a><br></p>', 1, 'https://www.mdpi.com/2079-9276/9/12/140', 1, '1', '2020-12-01 12:01:28', '2020-12-01 12:01:28');

-- --------------------------------------------------------

--
-- Table structure for table `resource_files`
--

CREATE TABLE `resource_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `resource_id` int(10) UNSIGNED NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_extension` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_storage_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resource_files`
--

INSERT INTO `resource_files` (`id`, `resource_id`, `file_name`, `file_extension`, `file_storage_name`, `created_at`, `updated_at`) VALUES
(1, 1, '9994.odp', 'odp', NULL, '2020-12-01 12:01:28', '2020-12-01 12:01:28');

-- --------------------------------------------------------

--
-- Table structure for table `resource_types`
--

CREATE TABLE `resource_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resource_types`
--

INSERT INTO `resource_types` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Publication Archive', 'publication-archive', NULL, '2020-11-30 05:30:32', '2020-11-30 05:30:32'),
(2, 'Products', 'products', NULL, '2020-11-30 05:30:32', '2020-11-30 05:30:32'),
(3, 'User Guide', 'user-guide', NULL, '2020-11-30 05:30:32', '2020-11-30 05:30:32'),
(4, 'Presentations', 'presentations', NULL, '2020-11-30 05:30:32', '2020-11-30 05:30:32'),
(5, 'Books', 'books', NULL, '2020-11-30 05:30:33', '2020-11-30 05:30:33'),
(6, 'Corpus', 'corpus', NULL, '2020-11-30 05:30:33', '2020-11-30 05:30:33');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `homepage_header_title_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Research & Development of',
  `homepage_about_title_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'University Grants Commission of Bangladesh',
  `homepage_about_title_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Window 4 Innovation Fund',
  `homepage_about_paragraph` longtext COLLATE utf8mb4_unicode_ci,
  `custom_field1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `github` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bitbucket` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `homepage_header_title_1`, `homepage_about_title_1`, `homepage_about_title_2`, `homepage_about_paragraph`, `custom_field1`, `custom_field2`, `custom_field3`, `facebook`, `twitter`, `youtube`, `github`, `bitbucket`, `created_at`, `updated_at`) VALUES
(1, 'Research & Development of', 'University Grants Commission of Bangladesh', 'Window 4 Innovation Fund', '<p>University-Industry Research Collaboration. Development of Multi-Platform Speech and Language Processing Software for Bangla ( CP-3888), HEQEP, UGC. Implementation Period: 3 Years (July 01, 2015- June 30, 2018)<br></p>', 'Language Processing Software for Bangla', NULL, NULL, 'https://www.facebook.com/', 'https://www.twitter.com/', 'https://www.twitter.com/', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `designation` int(10) UNSIGNED DEFAULT NULL,
  `dept_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Department of Computer Science and Engineering',
  `institute_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Shahjalal University of Science and Technology',
  `supervisor` int(10) UNSIGNED DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone_no`, `bio`, `designation`, `dept_name`, `institute_name`, `supervisor`, `image`, `is_admin`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Mahjabeen Akter Lubna', 'mahjabeen.sust@gmail.com', '$2y$10$TUfePGXn0NmRjk/UAR./.OqEEpWR0cqzzTg6fhmsA4JDeKAo1td1C', '01700000000', '<p>Comming soon</p>', NULL, 'Department of Computer Science and Engineering', 'Shahjalal University of Science & Technology', NULL, '16067387452.jpg', 1, 1, 'KAqXdyM5QpOaA6L98ibBrynbEBjQfLbyESY1d5IC60HX91Wn4aG4rpr5TDJG', '2020-11-30 19:19:05', '2020-11-30 19:19:05'),
(3, 'Rubel Reza', 'rubelreza101@gmail.com', '$2y$10$kd/4WL5S5x6fSAW8DYiGr.E3/H04u52JJQjZJ2r8Ny2ifOGSJgRcS', '01711111111', '<p>Comming soon...</p>', 1, 'Department of Computer Science and Engineering', 'Shahjalal University of Science & Technology', NULL, NULL, 0, 1, NULL, '2020-12-01 11:24:37', '2020-12-01 11:24:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_research_field_relations`
--

CREATE TABLE `user_research_field_relations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `research_field_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_user_id_foreign` (`user_id`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_category_slug_unique` (`slug`);

--
-- Indexes for table `blog_category_relations`
--
ALTER TABLE `blog_category_relations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_category_relations_blog_id_foreign` (`blog_id`),
  ADD KEY `blog_category_relations_category_id_foreign` (`category_id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `designations_slug_unique` (`slug`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `events_user_id_foreign` (`user_id`);

--
-- Indexes for table `events_category_relations`
--
ALTER TABLE `events_category_relations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `events_category_relations_event_id_foreign` (`event_id`),
  ADD KEY `events_category_relations_category_id_foreign` (`category_id`);

--
-- Indexes for table `lab_facilities`
--
ALTER TABLE `lab_facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metadata`
--
ALTER TABLE `metadata`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `metadata_key_unique` (`key`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_user_id_foreign` (`user_id`);

--
-- Indexes for table `news_category_relations`
--
ALTER TABLE `news_category_relations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_category_relations_news_id_foreign` (`news_id`),
  ADD KEY `news_category_relations_category_id_foreign` (`category_id`);

--
-- Indexes for table `news_events_category`
--
ALTER TABLE `news_events_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_events_category_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `people_email_unique` (`email`),
  ADD UNIQUE KEY `people_phone_no_unique` (`phone_no`),
  ADD KEY `people_designation_foreign` (`designation`);

--
-- Indexes for table `publications`
--
ALTER TABLE `publications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `publications_user_id_foreign` (`user_id`);

--
-- Indexes for table `research_fields`
--
ALTER TABLE `research_fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `research_fields_slug_unique` (`slug`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resources_user_id_foreign` (`user_id`),
  ADD KEY `resources_resource_type_foreign` (`resource_type`);

--
-- Indexes for table `resource_files`
--
ALTER TABLE `resource_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resource_files_resource_id_foreign` (`resource_id`);

--
-- Indexes for table `resource_types`
--
ALTER TABLE `resource_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `resource_types_slug_unique` (`slug`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_no_unique` (`phone_no`),
  ADD KEY `users_designation_foreign` (`designation`),
  ADD KEY `users_supervisor_foreign` (`supervisor`);

--
-- Indexes for table `user_research_field_relations`
--
ALTER TABLE `user_research_field_relations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_research_field_relations_user_id_foreign` (`user_id`),
  ADD KEY `user_research_field_relations_research_field_id_foreign` (`research_field_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_category_relations`
--
ALTER TABLE `blog_category_relations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events_category_relations`
--
ALTER TABLE `events_category_relations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lab_facilities`
--
ALTER TABLE `lab_facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `metadata`
--
ALTER TABLE `metadata`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_category_relations`
--
ALTER TABLE `news_category_relations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_events_category`
--
ALTER TABLE `news_events_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `publications`
--
ALTER TABLE `publications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `research_fields`
--
ALTER TABLE `research_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `resource_files`
--
ALTER TABLE `resource_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `resource_types`
--
ALTER TABLE `resource_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_research_field_relations`
--
ALTER TABLE `user_research_field_relations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_category_relations`
--
ALTER TABLE `blog_category_relations`
  ADD CONSTRAINT `blog_category_relations_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_category_relations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `blog_category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `events_category_relations`
--
ALTER TABLE `events_category_relations`
  ADD CONSTRAINT `events_category_relations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `news_events_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `events_category_relations_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `news_category_relations`
--
ALTER TABLE `news_category_relations`
  ADD CONSTRAINT `news_category_relations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `news_events_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `news_category_relations_news_id_foreign` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `people`
--
ALTER TABLE `people`
  ADD CONSTRAINT `people_designation_foreign` FOREIGN KEY (`designation`) REFERENCES `designations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `publications`
--
ALTER TABLE `publications`
  ADD CONSTRAINT `publications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `resources`
--
ALTER TABLE `resources`
  ADD CONSTRAINT `resources_resource_type_foreign` FOREIGN KEY (`resource_type`) REFERENCES `resource_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `resources_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `resource_files`
--
ALTER TABLE `resource_files`
  ADD CONSTRAINT `resource_files_resource_id_foreign` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_designation_foreign` FOREIGN KEY (`designation`) REFERENCES `designations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_supervisor_foreign` FOREIGN KEY (`supervisor`) REFERENCES `people` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_research_field_relations`
--
ALTER TABLE `user_research_field_relations`
  ADD CONSTRAINT `user_research_field_relations_research_field_id_foreign` FOREIGN KEY (`research_field_id`) REFERENCES `research_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_research_field_relations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
